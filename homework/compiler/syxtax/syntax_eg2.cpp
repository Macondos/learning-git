/**使用时，先在源文件目录下建立一个input.txt文档，存放需要执行词法分析的程序，执行后，词法分析的结果以及常量表，变量表的结果将输出至当前目录。*/

#include <iostream>
#include <fstream>
#include <math.h>
#include <ctype.h>
#include <cstdlib>
#include <cstring>

using namespace std;

#define Max 655  //最大代码长度
#define WordMaxNum 256   //变量最大个数
#define DigitNum 256    //常量最大个数
#define MaxKeyWord  32  //关键字数量
#define MaxOptANum 8    //运算符最大个数
#define MaxOptBNum 4    //运算符最大个数
#define MaxEndNum 11    //界符最大个数

enum errorType {VarExceed = 1,PointError = 2,ConExceed = 3};

typedef struct DisplayTable
{
    int Index;  //标识符所在表的下标
    int type;   //标识符的类型
    int line;   //标识符所在表的行数
    char symbol[20];    //标识符所在表的名称
}Table;

 int TableNum = 0;  //display表的下标
 char Word[WordMaxNum][20]; //标识符表
 char Digit[WordMaxNum][20]; //数字表
 int WordNum = 0;   //变量表的下标
 int DigNum = 0;     //常量表的下标
 bool errorFlag = 0; //错误标志

 const char* const KeyWord[MaxKeyWord] =
 {
     "and", "array", "begin", "case","char" "constant",
      "do",  "else", "end", "false","for",  "if", "input",
      "integer", "not", "of", "or", "output","packed",
      "procedure", "program", "read", "real","repeat",
      "set", "then", "to", "type", "until", "var","while",
      "with","prn"
};     //关键字
 const char OptA[] = {'+','-','*','/','=','#','<','>'}; // 单目运算
 const char *OptB[] = {"<=",">=",":=","<>"};        //双目运算符
 const char End[] = {
     '(', ')' , ',' , ';' , '.' , '[' ,
        ']' , ':' , '{' , '}' , '"'
 }; // 界符

 void error(char str[20],int nLine, int errorType)
 {
     cout <<" \nError :    ";
     switch(errorType)
     {
     case VarExceed:
        cout << "第" << nLine-1 <<"行" << str << " 变量的长度超过限制！\n";
        errorFlag = 1;
        break;
     case PointError:
        cout << "第" << nLine-1 <<"行" << str << " 小数点错误！\n";
        errorFlag = 1;
        break;
     case ConExceed:
        cout << "第" << nLine-1 <<"行" << str << " 常量的长度超过限制！\n";
        errorFlag = 1;
        break;
     }

 }//error

 void Scanner(char ch[],int chLen,Table table[Max],int nLine)
 {
    int chIndex = 0;

     while(chIndex < chLen) //对输入的字符扫描
     {
/*处理空格和tab*/
        while(ch[chIndex] == ' ' || ch[chIndex] == 9 ) //忽略空格和tab
        { chIndex ++; }
/*处理换行符*/
        while(ch[chIndex] == 10) //遇到换行符，行数加1
        {   nLine++;chIndex ++;}

/*标识符*/
        if( isalpha(ch[chIndex])) //以字母、下划线开头
        {
            char str[256];
            int strLen = 0;
            while(isalpha(ch[chIndex]) || ch[chIndex] == '_' ) //是字母、下划线
            {
                str[strLen ++] = ch[chIndex];
            chIndex ++;
                while(isdigit(ch[chIndex]))//不是第一位，可以为数字
                {
                    str[strLen ++] = ch[chIndex];
                    chIndex ++;
                }
            }
            str[strLen] = 0; //字符串结束符
            if(strlen(str) > 20) //标识符超过规定长度，报错处理
            {
                error(str,nLine,1);
            }
            else{   int i;
            for(i = 0;i < MaxKeyWord; i++) //与关键字匹配
                if(strcmp(str, KeyWord[i]) == 0) //是关键字，写入table表中
                {
                    strcpy(table[TableNum].symbol,str);
                    table[TableNum].type = 1;  //关键字
                    table[TableNum].line = nLine;
                    table[TableNum].Index = i;
                    TableNum ++;
                    break;
                }
                if(i >= MaxKeyWord) //不是关键字
                {

                            table[TableNum].Index = WordNum;
                            strcpy(Word[WordNum++],str);
                      table[TableNum].type = 2; //变量标识符
                     strcpy(table[TableNum].symbol,str);
                      table[TableNum].line = nLine;
                      TableNum ++;
                }
            }
        }

/*常数*/
        //else if(isdigit(ch[chIndex])&&ch[chIndex]!='0') //遇到数字
        else if(isdigit(ch[chIndex])) //遇到数字
        {
            int flag = 0;
            char str[256];
            int strLen = 0;
            while(isdigit(ch[chIndex]) || ch[chIndex] == '.') //数字和小数点
            {
                if(ch[chIndex] == '.')    //flag表记小数点的个数，0时为整数，1时为小数，2时出错
                    flag ++;
                str[strLen ++] = ch[chIndex];
                chIndex ++;
            }
            str[strLen] = 0;
                if(strlen(str) > 20) //常量标识符超过规定长度20，报错处理
            {
                error(str,nLine,3);
            }
            if(flag == 0)
            {
                table[TableNum].type = 3; //整数

            }
            if(flag == 1)
            {
                    table[TableNum].type = 4; //小数

            }
            if(flag > 1)
            {
                error(str,nLine,2);
            }
            table[TableNum].Index = DigNum;
            strcpy(Digit[DigNum ++],str);

            strcpy(table[TableNum].symbol,str);
            table[TableNum].line = nLine;
            TableNum ++;
        }

/*运算符*/

        else
        {
            int errorFlag; //用来区分是不是无法识别的标识符，0为运算符，1为界符

            char str[3];
            str[0] = ch[chIndex];
            str[1] = ch[chIndex + 1];
            str[3] = 0;
            int i ;
            for(i = 0;i < MaxOptBNum;i++)//MaxOptBNum)
                if(strcmp(str,OptB[i]) == 0)
                {
                    errorFlag = 0;
                    table[TableNum].type = 6;
                    strcpy(table[TableNum].symbol,str);
                    table[TableNum].line = nLine;
                    table[TableNum].Index = i;
                    TableNum ++;
                    chIndex  = chIndex + 2;
                    break;
                }
            if(i >= MaxOptBNum)
                {
                    for( int k = 0;k < MaxOptANum; k++)
                        if(OptA[k] == ch[chIndex])
                        {
                            errorFlag = 0;
                            table[TableNum].type = 5;
                            table[TableNum].symbol[0] = ch[chIndex];
                            table[TableNum].symbol[1] = 0;
                            table[TableNum].line = nLine;
                            table[TableNum].Index = k;
                            TableNum ++;
                            chIndex ++;
                            break;
                        }

/*界符*/
            for(int j = 0;j < MaxEndNum;j ++)
                if(End[j] ==ch[chIndex])
                {
                    errorFlag = 1;
                    table[TableNum].line = nLine;
                    table[TableNum].symbol[0] = ch[chIndex];
                    table[TableNum].symbol[1] = 0;
                    table[TableNum].Index = j;
                    table[TableNum].type = 7;
                    TableNum ++;
                    chIndex ++;
                }
/*其他无法识别字符*/
                if(errorFlag != 0 && errorFlag != 1) //开头的不是字母、数字、运算符、界符
                {
                    char str[256];
                    int strLen = -1;
                    str[strLen ++] = ch[chIndex];
                    chIndex ++;

                    while(*ch != ' ' || *ch != 9 || ch[chIndex] != 10)//
                    {
                        str[strLen ++] = ch[chIndex];
                        chIndex ++;
                    }
                    str[strLen] = 0;
                    table[TableNum].type = 8;
                    strcpy(table[TableNum].symbol,str);
                    table[TableNum].line = nLine;
                    table[TableNum].Index = -2;
                    TableNum ++;
                }
        }
     }

 }

}

void Trans(double x,int p)  //把十进制小数转为16进制
{
    int i=0;                  //控制保留的有效位数
    while(i<p)
    {
        if(x==0)              //如果小数部分是0
            break;            //则退出循环
        else
        {
            int k=int(x*16);  //取整数部分
            x=x*16-int(k);    //得到小数部分
            if(k<=9)
                cout<<k;
            else
                cout<<char(k+55);
        };
        i++;
    };

};


 int main()
 {
    ifstream in;
    ofstream out,outVar,outCon;
    char in_file_name[26],out_file_name[26]; //读入文件和写入文件的名称
    char ch[Max];   //存放输入代码的缓冲区
    int nLine = 1;  //初始化行数
    Table *table = new Table[Max];
    int choice;

    cout << "请输入读入方式：1：从文件中读，2：从键盘读（输入结束标志位#）：\n";
    cin >> choice;

    switch(choice)
    {
      int i;
/*从文件读取*/
      case 1:

        cout<<"Enter the input file name:\n";
        cin>>in_file_name;
        in.open(in_file_name);
        if(in.fail())  //打开display表读文件失败
        {
            cout<<"Inputput file opening failed.\n";
            exit(1);
        }

        cout<<"Enter the output file name:\n";
        cin>>out_file_name;
        out.open(out_file_name);
        outVar.open("变量表.txt");
        outCon.open("常量表.txt");
        if(out.fail())  //打开display表写文件失败
        {
            cout<<"Output file opening failed.\n";
            exit(1);
        }
        if(outVar.fail())  //打开变量表写文件失败
        {
            cout<<"VarOutput file opening failed.\n";
            exit(1);
        }

        if(outCon.fail())  //打开常量表写文件失败
        {
            cout<<"ConstOutput file opening failed.\n";
            exit(1);
        }


     in.getline(ch,Max,'#');
     Scanner(ch, strlen(ch),table,nLine); //调用扫描函数

     if(errorFlag == 1) //出错处理
            return 0;
 /*把结果打印到各个txt文档中*/  out <<"类型"<<"      "<<"下标"  <<endl;
     for( i = 0; i < TableNum;i ++)//打印display
        out<< "(" <<hex << table[i].type<< "    ,   "<< "" << hex << table[i].Index<< ")" <<endl; //在文件testout.txt中输出
     outCon << "下标" << "     " << "常量值" << endl;
     for(i = 0;i < TableNum;i++) //打印常量表
     {
         if(table[i].type == 3)
         {
            long num1;
            num1 = atoi(table[i].symbol);
            outCon<< "(" <<hex << table[i].Index << "   ,   "<< "" << hex << num1 << ")" <<endl;
         }
         if(table[i].type == 4)
         {
            double num2;
            num2 = atof(table[i].symbol);
            outCon<< "(" <<hex << table[i].Index << "   ,   "<< "" << hex << num2<< ")" <<endl;
         }
     }
     outVar <<"类型"<<"       "<< "变量名称"  <<endl;
     for( i = 0; i < WordNum;i ++)//打印变量表
        outVar<< "(" <<hex << i<< "     " << Word[i] << ")" <<endl; //在文件testout.txt中输出

        in.close();//关闭文件
        out.close();
        outVar.close();
        outCon.close();
        break;
/*从键盘输入的方式，输出到屏幕*/
    case 2:
        cin.getline(ch,Max,'#');
        Scanner(ch, strlen(ch),table,nLine); //调用扫描函数

    if(errorFlag == 1)
            return 0;

        cout << "\nDisplay表： \n";
        cout <<"类型"<<"      "<<"下标"  <<endl; //dos界面下
        for( i = 0; i < TableNum;i ++)
            cout<< "(" <<hex << table[i].type<< "   ,   "<< "" << hex << table[i].Index<< ")" <<endl;

     cout << "\n常量表：\n" << "下标" << "     " << "常量值" << endl;
     for(i = 0;i < TableNum;i++) //打印常量表
     {
         if(table[i].type == 3)
         {
            long num1;
            num1 = atoi(table[i].symbol);
            cout<< "(" <<hex << table[i].Index << " ,   "<< "" << hex << num1 << ")" <<endl;
         }
         if(table[i].type == 4)
         {
            char *num2;
            float num,num3;
            num = atof(table[i].symbol);
            num2 = gcvt(16,strlen(table[i].symbol),table[i].symbol);
            num3 = num - floor(num);
            cout << "(" << hex << table[i].Index << "   ,   " << num2;
            Trans(num3,5) ;
            cout << ")" <<endl;
         }
     }
     cout <<"\n变量表：\n类型"<<"     "<< "变量名称"  <<endl;
     for( i = 0; i < WordNum;i ++)//打印变量表
        cout<< "(" <<hex << i<< "       " << Word[i] << ")" <<endl; //在文件testout.txt中输出
        break;
    }
    return 0;
 }
