#include<iostream>
using namespace std;

class one 
{
		int hi;
	public:
		one(int o=0):hi(o){}
		void read() const{cout<<hi<<"call one"<<endl;};
};
class two:public one
{
		int hi;
	public:
		two(int o=0,int h=1):one(o),hi(h){}
		void read(){cout<<hi<<"call two"<<endl;}
};
class three:public two
{
		int hi;
	public:
		three(int t=0,int o=0,int h=1):two(o,h),hi(t){};
		void read(int i=0) const{cout<<hi<<"call three"<<endl;}
};
int main()
{
		one* pt;
		two apple(1,2);
		apple.read();
}
