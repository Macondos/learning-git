#include<iostream>
using namespace std;
double arr[10];
void quicksort(int low,int high);

int main()
{
    for (int i=0;i<10;i++)
        cin>>arr[i];
    quicksort(0,9);
    for (int i=0;i<9;i++)
        cout<<arr[i]<<" ";
    cout<<arr[9];
}

void quicksort(int low,int high)
{
    if (low<high)
    {
        int fix_low=low;
        int fix_high=high;
        double set=arr[low];
        while (low<high)
        {
            while(arr[high]>=set&&high>low)
                high--;
            double temp=arr[high];
            arr[high]=arr[low];
            arr[low]=temp;
            while(arr[low]<=set&&low<high)
                low++;
            temp=arr[high];
            arr[high]=arr[low];
            arr[low]=temp;
        }
        quicksort(fix_low,low-1);
        quicksort(low+1,fix_high);
    }
}
