#include <iostream>
#include <queue>
#include <algorithm>
using namespace std;
struct node
{
    int key;
    node *next;
};
void merge(node *left, node *right, node *&back)
{
    node *i = left, *j = right, *cur;
    if (left->key <= right->key)
    {
        back = left;
        i = i->next;
    }
    else
    {
        back = right;
        j = j->next;
    }
    cur = back;
    while (i != nullptr && j != nullptr)
    {
        if (i->key <= j->key)
        {
            cur->next = i;
            cur = i;
            i = i->next;
        }
        else
        {
            cur->next = j;
            cur = j;
            j = j->next;
        }
    }
    if (i != nullptr)
        cur->next = i;
    else
        cur->next = j;
}

void merge_sort1(node *&head)
{
    node *temp = head;
    int count = 0;
    while (temp != nullptr)
    {
        count++;
        temp = temp->next;
    }
    node *link_arr[count];
    link_arr[0] = head;
    node *pre = head,
         *cur = head->next;
    int i = 1;
    while (cur != nullptr)
    {
        link_arr[i++] = cur;
        pre->next = nullptr;
        pre = cur;
        cur = cur->next;
    }

    int len = 1;
    while (len < count)
    {
        for (int i = 0; i < count - len; i += 2 * len)
            merge(link_arr[i], link_arr[i + len], link_arr[i]);
        len *= 2;
    }
    head=link_arr[0];
}

int main()
{
    int num;
    cin >> num;
    node *head = new node;
    cin >> head->key;
    node *cur = head;
    for (int i = 1; i < num; i++)
    {
        cur->next = new node;
        cin >> cur->next->key;
        cur = cur->next;
    }
    cur->next = nullptr;
    merge_sort1(head);
}