#include <iostream>
#include <algorithm>
#include <stack>
#include <queue>
using namespace std;
//partition算法1，该算法pivot不参与交换，一直跑到ij相互交错
//要求将选出的pivot放在两端的一个位置上
int partition1(int *arr, int left, int right)
{
    int pivot = arr[left];
    int i = left + 1, j = right;
    while (i <= right && j >= left)
    {
        while (arr[j] > pivot && j >= left)
            j--;
        while (arr[i] < pivot && i <= right)
            i++;
        if (i < j)
        {
            swap(arr[i], arr[j]);
            i++;
            j--;
        }
        else break;
    }
    swap(arr[left], arr[j]);
    return j;
}
//partition算法2，该算法pivot参与交换，是传统的快速排序算法
//要求将pivot放在后走的指针上
int partition2(int *arr, int left, int right)
{
    int pivot = arr[left];
    int i = left, j = right;
    while (i < j)
    {
        while (i < j && arr[j] > pivot)
            j--;
        arr[i] = arr[j];
        while (i < j && arr[i] <= pivot)
            i++;
        arr[j] = arr[i];
    }
    arr[i] = pivot;
    return i;
}
//快速排序非递归算法
//使用栈实现
void qs_stack(int *arr, int num)
{
    /*int temp[num];
    for (int i = 0; i < num; i++)
        temp[i] = arr[i];*/

    stack<int> s;
    s.push(0);
    s.push(num - 1); //先压左边界，再压右边界
    while (!s.empty())
    {
        int right = s.top();
        s.pop();
        int left = s.top();
        s.pop();
        if (right > left)
        {
            int pivot = partition1(arr, left, right);
            s.push(left);
            s.push(pivot - 1);
            s.push(pivot + 1);
            s.push(right);
        }
    }
}

void qs(int *arr, int left, int right)
{
    if (left < right)
    {
        int pivot = partition1(arr, left, right);
        qs(arr, left, pivot - 1);
        qs(arr, pivot + 1, right);
    }
}

int main()
{
    int num = 12;
    /* cin >> num;*/
    int arr[num] = {38, 7, 72, 12, 43, 65, 62, 88, 31, 27, 15, 54};
    /*for (int i = 0; i < num; i++)
    {
        cin >> arr[i];
    }*/
    //   qs(arr, 0, num - 1);
    qs_stack(arr, num);
    return 0;
}