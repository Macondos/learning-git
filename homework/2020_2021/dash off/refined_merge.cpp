#include <iostream>
#include <algorithm>
using namespace std;

int gcd(int a, int b)
{
    return b == 0 ? a : gcd(b, a % b);
}

void sift_right(int *a, int left, int right, int k)
{
    int num = right - left + 1;
    k = k % num;
    if (k == 0 && k == num)
        return;
    int m = gcd(num, k);
    for (int i = 0; i < m; i++)
    {
        int temp = a[i + left];
        int cur = i;
        do
        {
            a[cur + left] = a[(cur - k + num) % num + left];
            cur = (cur - k + num) % num;
        } while ((cur - k + num) % num != i);
        a[cur + left] = temp;
    }
}
void merge(int *a, int left, int right, int rightend)
{
    int i = left, j = right;
    while (i < j && j <= rightend)
    {
        int d = 0;
        while (a[i] <= a[j] && i < j)
            i++;
        while (a[i] > a[j] && i < j && j <= rightend)
        {
            j++;
            d++;
        }
        if (i < j)
            sift_right(a, i, j - 1, d);
    }
}
void refined_merge(int *a, int num)
{
    int len = 1;
    while (len < num)
    {
        int i = 0;
        for (; i < num - 2 * len; i += 2 * len)
            merge(a, i, i + len, i + 2 * len - 1);
        if (i + len < num)
            merge(a, i, i + len, num - 1);
        len *= 2;
    }
}
int main()
{
    int num;
    cin >> num;
    int a[num];
    for (int i = 0; i < num; i++)
        cin >> a[i];
    refined_merge(a, num);
}
