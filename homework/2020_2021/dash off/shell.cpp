#include <iostream>
using namespace std;

void gap_insert(int *a, int num, int start, int gap)
{
    for (int i = start + gap; i < num; i += gap)
    {
        int temp = a[i];
        int j = i - gap;
        for (; j >= 0 && a[j] > temp; j -= gap)
            a[j + gap] = a[j];
        a[j + gap] = temp;
    }
}
void shell_sort(int *a, int num)
{
    int gap = num / 2;
    while (gap != 0)
    {
        for (int i = 0; i < gap; i++)
            gap_insert(a, num, i, gap);
        gap /= 2;
    }
}

int main()
{
    int num = 10;
    int a[10];
    for (int &x : a)
        cin >> x;
    shell_sort(a, num);
    cout << " ";
}