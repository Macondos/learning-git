#include <iostream>
#include <algorithm>
#include <stack>
#include <queue>
using namespace std;
//partition算法1，该算法pivot不参与交换，一直跑到ij相互交错
//要求将选出的pivot放在两端的一个位置上
int partition1(int *arr, int left, int right)
{
    int pivot = arr[left];
    int i = left + 1, j = right;
    while (1)
    {
        while (j >= left + 1 && arr[j] > pivot)
            j--;
        while (i <= right && arr[i] < pivot)
            i++;
        if (i < j && j >= left + 1 && i <= right)
        {
            swap(arr[i], arr[j]);d
            i++;
            j--;
        }
        else
            break;
    }
    swap(arr[left], arr[j]);
    return j;
}

//快速排序非递归算法
//使用栈实现
void qs(int *arr, int left, int right)
{
    if (left < right)
    {
        int pivot = partition1(arr, left, right);
        qs(arr, left, pivot - 1);
        qs(arr, pivot + 1, right);
    }
}

int main()
{
    int num;
    cin >> num;
    int arr[num];
    for (int i = 0; i < num; i++)
        cin >> arr[i];
    qs(arr, 0, num - 1);
    for (int i = 0; i < num; i++)
        cout << arr[i];
    return 0;
}