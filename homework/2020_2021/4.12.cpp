#include<iostream>
using namespace std;
typedef unsigned long Item;
class Stack
{
	private:
		enum {MAX = 10}; // constant specific to class
		Item * pitems; // holds stack items
		int size; // number of elements in stack
		int top; // index for top stack item
	public:
		Stack(int n = MAX); // creates stack with n elements
		Stack(const Stack & st);
		~Stack(){delete [] pitems;};
		bool isempty() const;
		bool isfull() const;
		// push() returns false if stack already is full, true otherwise
		bool push(const Item & item); // add item to stack
		// pop() returns false if stack already is empty, true otherwise
		bool pop(Item & item); // pop top into item
		Stack & operator=(const Stack & st);
};
Stack::Stack(int n):size(n),top(0)
{
	pitems=new Item[size];
}
Stack::Stack(const Stack & st)
{
	size=st.size;
	top=st.top;
	pitems=new Item[size];
	for (int i=0;i<top;i++)
		pitems[i]=st.pitems[top];
}
bool Stack::isempty() const
{
	return top==0;
}
bool Stack::isfull() const
{
	return top==size;
}
bool Stack::push(const Item & item)
{
	if (top<size)
	{
		pitems[top++]=item;
		return true;
	}
	else return false;
}
bool Stack::pop(Item & item)
{
	if (top>0)
	{
		item=pitems[--top];
		return true;
	}
	else return false;
}
Stack & Stack::operator=(const Stack & st)
{
	size=st.size;
	top=st.top;
	delete [] pitems;
	pitems=new Item[size];
	for (int i=0;i<top;i++)
		pitems[i]=st.pitems[i];
	return *this;
}

int main()
{
	Item temp;
	int max;
	cout<<"build two stack! input the max numbers"<<endl;
	cin>>max;
	Stack one(max);
	Stack two;
	cout<<"push items into stack one until full"<<endl;
	while(cin>>temp)
	{
		if (!one.isfull())
		{
			cout<<"push "<<temp<<" into the stack"<<endl;
			one.push(temp);
		}
		else break;
	}
	cout<<"assign one to two!"<<endl;
	two=one;
	while(true)
	{
		if (!two.isempty())
		{
			two.pop(temp);
			cout<<"two pops "<<temp<<endl;
		}
		else break;
	}
}

