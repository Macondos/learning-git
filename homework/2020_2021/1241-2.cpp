#include <iostream>
#include <stack>
using namespace std;

int main()
{
    stack<int> left_yuan;
    stack<int> left_fang;
    // record the whole array
    char arr[101] = {0};
    // record the unmatched right operand
    bool unmatched[101] = {0};
    char temp;
    // initialize a counter
    int i = 0;

    while ((temp = cin.get()) != '\n')
    {
        //store the original data
        arr[i] = temp;
        //check
        switch (temp)
        {
        case '[':
            left_fang.push(i);
            break;
        case '(':
            left_yuan.push(i);
            break;
        case ']':
            if (!left_fang.empty())
                left_fang.pop();
            else
                unmatched[i] = true;
            break;
        case ')':
            if (!left_yuan.empty())
                left_yuan.pop();
            else
                unmatched[i] = true;
            break;
        }
         i++;
    }
    //find what's left in the stack
    int position;
    while (!left_yuan.empty())
    {
        position = left_yuan.top();
        unmatched[position] = true;
        left_yuan.pop();
    }
    while (!left_fang.empty())
    {
        position = left_fang.top();
        unmatched[position] = true;
        left_fang.pop();
    }
    //print
    for (int k = 0; k < i; k++)
    {
        if (!unmatched[k])
            cout << arr[k];
        else if (arr[k] == '[' || arr[k] == ']')
            cout << "[]";
        else
            cout << "()";
    }
}