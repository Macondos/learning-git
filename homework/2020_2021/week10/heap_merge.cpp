#include <iostream>
using namespace std;

void merge(int *a, int *temp, int left, int right, int rightend)
{
    int leftend = right - 1;
    int i = left, j = right, k = left;
    while (i <= leftend && j <= rightend)
        if (a[i] <= a[j])
            temp[k++] = a[i++];
        else
            temp[k++] = a[j++];
    while (i <= leftend)
        temp[k++] = a[i++];
    while (j <= rightend)
        temp[k++] = a[j++];
}
void mergepass(int *a, int *temp, int len, int num)
{
    int i = 0;
    for (; i < num - 2 * len; i += 2 * len)
        merge(a, temp, i, i + len, i + 2 * len - 1);
    if (i + len >= num)
        while (i < num)
            temp[i] = a[i++];
    else
        merge(a, temp, i, i + len, num - 1);
}
void mergesort(int *a, int num)
{
    int *temp = new int[100];
    int len = 1;
    while (len < num)
    {
        mergepass(a, temp, len, num);
        len *= 2;
        mergepass(temp, a, len, num);
        len *= 2;
    }
    delete[] temp;
}
//前一个元素是(n-1)/2
void filterdown(int *a, int i, int num)
{
    int temp = a[i];
    int parent = i;
    int child = 2 * i + 1;
    if (2 * i + 2 < num && a[child + 1] < a[child])
        child++;
    while (child < num && temp > a[child])
    {
        a[parent] = a[child];
        parent = child;
        child = 2 * parent + 1;
        if (2 * parent + 2 < num && a[child + 1] < a[child])
            child++;
    }
    a[parent] = temp;
}
void minheap_sort(int *a, int num)
{
    for (int i = num / 2 - 1; i >= 0; i--)
        filterdown(a, i, num);
    for (int i = num - 1; i > 0; i--)
    {
        swap(a[0], a[i]);
        filterdown(a, 0, i);
    }
}

int main()
{
    int num;
    cin >> num;
    int a[100];
    for (int i = 0; i < num; i++)
        cin >> a[i];

    mergesort(a, num);
    cout << a[0];
    for (int i = 1; i < num; i++)
        cout << " " << a[i];
    cout << endl;
    minheap_sort(a, num);
    cout << a[0];
    for (int i = 1; i < num; i++)
        cout << " " << a[i];

    return 0;
}