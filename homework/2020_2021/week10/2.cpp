#include <iostream>
using namespace std;

void bubblesort(int *a, int num = 10)
{
    for (int i = 0; i < num - 1; i++)
    {
        int exchange = 0;
        for (int j = num - 1; j > i; j--)
            if (a[j - 1] > a[j])
            {
                swap(a[j - 1], a[j]);
                exchange = 1;
            }
        if (exchange == 0)
            break;
    }
}

void selectionsort(int *a, int num = 10)
{
    for (int i = 0; i < num - 1; i++)
    {
        int pos = i;
        for (int j = i; j < num; j++)
            if (a[j] > a[pos])
                pos = j;
        if (i != pos)
            swap(a[i], a[pos]);
    }
}

int main()
{
    int a[10];
    for (int &x : a)
        cin >> x;
    bubblesort(a);
    cout << a[0];
    for (int i = 1; i < 10; i++)
        cout << " " << a[i];
    cout << endl;
    selectionsort(a);
    cout << a[0];
    for (int i = 1; i < 10; i++)
        cout << " " << a[i];
}