#include <iostream>
using namespace std;

struct stu
{
    int num;
    string name;
    int score;
};
void direct_insert_sort(stu *a, int num)
{
    for (int i = 1; i < num; i++)
    {
        stu temp = a[i];
        int k = i - 1;
        for (; k >= 0 && a[k].score < temp.score; k--)
            a[k + 1] = a[k];
        a[k + 1] = temp;
    }
}
int main()
{
    int num;
    cin >> num;
    stu a[20];
    for (int i = 0; i < num; i++)
        cin >> a[i].num >> a[i].name >> a[i].score;
    direct_insert_sort(a, num);
    int i = 0;
    for (; i < num - 1; i++)
        cout << a[i].num << " " << a[i].name << " " << a[i].score << endl;
    cout << a[i].num << " " << a[i].name << " " << a[i].score;
}