#include <iostream>
#include <cstring>
using namespace std;

void transfer(int *temp, int decimal, int base)
{
    int count = 0;
    for (; decimal != 0; count++)
    {
        temp[count] = decimal % base + 1;
        decimal /= base;
    }
}

int main()
{
    long long int decimal, base;
    cin >> decimal >> base;
    int temp[10000] = {0};

    if (decimal == 0)
    {
        cout << "Yes" << endl
             << "0";
        return 0;
    }

    transfer(temp, decimal, base);

    int len = -1;
    while (temp[++len] != 0)
        ;

    int i = 0;
    for (; i <= (len - 1) / 2; i++)
    {
        if (temp[i] != temp[len - 1 - i])
            break;
    }

    if (i == (len - 1) / 2 + 1)
        cout << "Yes" << endl;
    else
        cout << "No" << endl;

    cout << temp[len - 1] - 1;
    for (int i = len - 2; i >= 0; i--)
        cout << " " << temp[i] - 1;
}