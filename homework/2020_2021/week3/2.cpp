#include <iostream>
#include <cstring>
#include <math.h>
using namespace std;

int numget()
{
    char temp[20];
    cin >> temp;
    int len = strlen(temp);

    int t = 0;
    int count = len - 1;

    for (; count >= 0; count--)
        t += pow(10, count) * (temp[len - 1 - count]-48);
    return t;
}
int main()
{
    int num1, num2;
    num1 = numget();
    num2 = numget();
    cout << num1 + num2;
}