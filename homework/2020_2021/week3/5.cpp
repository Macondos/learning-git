#include <iostream>
using namespace std;

int main()
{
    int len;
    cin >> len;
    if (len <= 1)
        return 0;
    int arr[len + 1] = {0};
    int next_prime = 2;
    int j;
    do
    {
        for (int i = next_prime + 1; i < len; i++)
        {
            if (!arr[i] && i % next_prime == 0)
                arr[i] = 1;
        }

        for (j = next_prime + 1; j <= len; j++)
            if (!arr[j])
            {
                next_prime = j;
                break;
            }
    } while (j <= len);
    cout << "2";
    for (int i = 3; i < len; i++)
        if (!arr[i])
            cout << " " << i;
}