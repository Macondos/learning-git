#include <iostream>
#include <math.h>
#include <algorithm>

using namespace std;
bool is_prime(int n)
{
    if (n == 1)
        return false;
    int i;
    for (i = 2; i <= sqrt(n); i++)
    {
        if (n % i == 0)
            break;
    }
    if (i == int(sqrt(n)) + 1)
        return true;
    else
        return false;
}

int main()
{
    int num1, num2, tmax, tmin, result;
    cin >> num1 >> num2;
    tmax = result = max(num1, num2);
    tmin = min(num1, num2);
    for (int i = 2; i <= tmin; i++)
    {
        if (is_prime(i) && tmin % i == 0)
        {
            while (tmin % i == 0)
            {
                if (tmax % i != 0)
                    result *= i;
                if (tmax % i == 0)
                    tmax /= i;
                tmin /= i;
            }
        }
    }
    cout << result;
}