//用C++测试词典分词的速度
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <set>
#include <algorithm>

using namespace std;
int main()
{
    ifstream dict_file("mini_dict.txt");
    set<string> dict;
    vector<string> segs;
    if (dict_file.good())
    {
        string word;
        while (dict_file.peek() != EOF)
        {
            dict_file >> word;
            dict.insert(word);
        }
    }
    string text = "中文编码是一个复杂而繁琐的问题，尤其是在使用C++的时候，不像python这种直接就可以迭代出单个中文字符，C++中是以字节为单位的，那么我们要读取一个中文字符就要读取三次字节流，读取英文字符就只需要读取一次，是不是超级麻烦。那么C++怎么样在中英文混合的字符串中分离中英文或者计算字符串长度（不是字节数）呢，那就需要彻底搞清楚编码是个怎么回事";
    clock_t start = clock();
    string seg;
    for (int i = 0; i < text.size();)
    {
        string longest_word = text.substr(i, 3);
        for (int j = i + 3; j < text.size() + 1; j += 3)
        {
            seg = text.substr(i, j - i);
            if (dict.find(seg) != dict.end())
            {
                if (seg.size() > longest_word.size())
                {
                    longest_word = seg;
                }
            }
        }
        i = i + longest_word.size();
        segs.push_back(longest_word);
    }
    clock_t end = clock();
    cout << "分词完成\n";
    double consumeTime = (double)(end - start) / CLOCKS_PER_SEC;
    cout << "一共有" << segs.size() << "个分词,分词速度:" << segs.size() * 1000.0 / consumeTime << endl;
}
