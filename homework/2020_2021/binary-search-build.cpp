#include <iostream>
using namespace std;

struct knot
{
    int key;
    knot *left;
    knot *right;
};

int main()
{
    int temp;
    cin >> temp;
    knot *root = new knot();
    root->key = temp;
    root->left = nullptr;
    root->right = nullptr;
    while (cin.peek() != '\n')
    {
        knot *t, *troot = root;
        cin >> temp;
        while (troot)
        {
            t = troot;
            if (temp == troot->key)
                break;
            else if (temp > troot->key)
                troot = troot->right;
            else
                troot = troot->left;
        }
        if (troot)
            continue;
        else
        {
            knot *tt = new knot;
            tt->key = temp;
            tt->left = nullptr;
            tt->right = nullptr;

            if (temp > t->key)
                t->right = tt;
            else
                t->left = tt;
        }
    }
    return 0;
}