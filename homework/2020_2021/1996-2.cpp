#include<iostream>
#include<deque>
using namespace std;

int main()
{
    int n,m;
    cin>>n>>m;
    deque<int> dque;
    for(int i=1;i<=n;i++)
        dque.push_back(i);
    int count=1;

    while(!dque.empty())
    {
        int temp=dque.front();
        if(count!=m)
        {
            dque.push_back(temp);
            dque.erase(dque.begin());
            count++;
        }
        else
        {
            dque.erase(dque.begin());
            cout<<temp<<" ";
            count=1;
        }       
    }
}
