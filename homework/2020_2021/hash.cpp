#include <iostream>
using namespace std;
int Hash(int key)
{
    return key % 13;
}
//使用线性探测法插入元素
bool liner_probing(int *a, int key)
{
    int temp = Hash(key);
    int d = temp;
    while (a[d])
    {
        d = (d + 1) % 13;
        if (d == temp)
            return false;
    }
    a[d] = key;
    return true;
}
//从哈希表中查找元素，查找成功则返回需要查找次数，不成功则返回-1
int find_liner(int *a, int key)
{
    int temp = Hash(key);
    int d = temp;
    int count = 1;
    while (a[d] != key)
    {
        count++;
        d = (d + 1) % 13;
        if (d == temp)
            return -1;
    }
    return count;
}
double asl_liner(int *a)
{
    int sum = 0;
    for (int i = 0; i < 13; i++)
        if (a[i])
            sum += find_liner(a, a[i]);
    return sum / 10.0;
}
/////////////////////////////////////////////////////////
//使用平方探测法插入元素
bool quadratic_probing(int *a, int key)
{
    int temp = Hash(key);
    int d = temp;
    int count = 1;
    while (a[d])
    {
        count++;
        if (count % 2 == 1)
            d = (temp - (count / 2) * (count / 2)) % 13;
        else
            d = (temp + (count / 2) * (count / 2)) % 13;
        if (d == temp)
            return false;
    }
    a[d] = key;
    return true;
}
//从哈希表中查找元素，查找成功则返回需要查找次数，不成功则返回-1
int find_quadratic(int *a, int key)
{
    int temp = Hash(key);
    int d = temp;
    int count = 1;
    while (a[d] != key)
    {
        count++;
        if (count % 2 == 1)
            d = (temp - (count / 2) * (count / 2)) % 13;
        else
            d = (temp + (count / 2) * (count / 2)) % 13;
        if (d == temp)
            return -1;
    }
    return count;
}
double asl_quadratic(int *a)
{
    int sum = 0;
    for (int i = 0; i < 13; i++)
        if (a[i])
            sum += find_quadratic(a, a[i]);
    return sum / 10.0;
}
////////////////////////////////////////////////////////////
//使用链地址法
struct node
{
    int key;
    node *next;
};
void link_probing(node *a, int key)
{
    int temp = Hash(key);
    node *t = new node;
    t->key = key;
    t->next = nullptr;
    node *p1 = &a[temp];
    while (p1->next != nullptr)
        p1 = p1->next;
    p1->next = t;
}
double asl_link(node *a)
{
    int sum = 0, count = 0;
    node *p1;
    for (int i = 0; i < 13; i++)
    {
        p1 = &a[i];
        count = 0;
        while (p1->next != nullptr)
        {
            p1 = p1->next;
            count++;
        }
        sum += (1 + count) * count / 2;
    }
    return sum / 10.0;
}
void print(int *a)
{
    if (a[0] == 0)
        cout << " ";
    else
        cout << a[0];
    for (int i = 1; i < 13; i++)
        if (a[i] == 0)
            cout << ", ";
        else
            cout << "," << a[i];
}
void print_link(node *a)
{
    for (int i = 0; i < 13; i++)
    {
        cout << "余数为" << i << " : ";
        if (a[i].next != nullptr)
        {
            node *p = a[i].next;
            cout << p->key;
            while (p->next != nullptr)
            {
                p = p->next;
                cout << "," << p->key;
            }
        }
        cout << endl;
    }
}

int main()
{
    int a[13] = {0};
    int temp[] = {5, 88, 12, 56, 71, 28, 33, 43, 93, 17};
    //线性探测法
    for (int x : temp)
        liner_probing(a, x);
    cout << "线性探测法后的哈希表为：" << endl;
    print(a);
    cout << endl
         << "查找成功时平均查找长度" << asl_liner(a)
         << endl
         << endl;
    //平方探测法
    for (int &x : a)
        x = 0;
    for (int x : temp)
        quadratic_probing(a, x);
    cout << "平方探测法后的哈希表为：" << endl;
    print(a);
    cout << endl
         << "查找成功时平均查找长度" << asl_quadratic(a)
         << endl
         << endl;
    //链地址法
    node b[13];
    for (node &x : b)
    {
        x.key = 0;
        x.next = nullptr;
    }
    for (int x : temp)
        link_probing(b, x);
    cout << "链地址探测法后的哈希表为：" << endl;
    print_link(b);
    cout << "链地址法查找成功时平均查找长度" << asl_link(b);
}