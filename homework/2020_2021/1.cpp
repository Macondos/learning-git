#include <iostream>
using namespace std;

int main()
{
    int num;
    cin >> num;
    int arr[100];
    for (int i = 0; i < num; i++)
        cin >> arr[i];
    int low = 0, high = num - 1, mid;
    int temp;
    cin >> temp;
    while (low <= high)
    {
        mid = (low + high) / 2;
        if (temp == arr[mid])
        {
            cout << mid;
            break;
        }
        else if (temp > arr[mid])
            low = mid + 1;
        else
            high = mid - 1;
    }
    if (low > high)
        cout << -1;
}