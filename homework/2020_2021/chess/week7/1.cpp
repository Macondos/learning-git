#include <iostream>
#include <queue>
#include <string>
using namespace std;
struct knot
{
    int key;
    knot *left, *right;
    friend bool operator<(knot k1, knot k2)
    {
        return k2.key < k1.key;
    }
};

int wpl(knot *root, int depth, int&);
void print(knot *root);
void code(knot *root, string c);
int main()
{
    priority_queue<knot> q;
    int num;
    cin >> num;
    for (int i = 0; i < num; i++)
    {
        knot temp;
        cin >> temp.key;
        temp.left = temp.right = nullptr;
        q.push(temp);
    }
    for (int i = 0; i < num-1; i++)
    {
        knot temp;
        knot *top1 = new knot(q.top());
        q.pop();
        knot *top2 = new knot(q.top());
        q.pop();

        temp.key = top1->key + top2->key;
        temp.left = top1;
        temp.right = top2;
        q.push(temp);
    }
    knot root = q.top();
    //计算wpl
    int count=0;
    wpl(&root, 0, count);
    cout << count << endl;
    print(&root);
    cout << endl;
    code(&root, "");
}

int wpl(knot *root, int depth, int& count)
{
    if (root->left)
        count += wpl(root->left, 1 + depth,count);
    if (root->right)
        count += wpl(root->right, 1 + depth,count);
    if (!root->left && !root->right)
        return root->key * depth;
    else
        return 0;
}
void print(knot *root)
{
    if (root->right && root->left)
    {
        cout << root->key << "(";
        print(root->left);
        cout << ",";
        print(root->right);
        cout << ")";
    }
    else
        cout << root->key;
}

void code(knot *root, string c)
{
    if (root->left)
    {
        c.push_back('0');
        code(root->left, c);
        c.pop_back();
        c.push_back('1');
        code(root->right, c);
        c.pop_back();
    }
    else
        cout << c << " ";
}