#include <iostream>
#include <cmath>
using namespace std;
struct knot
{
    int c;
    int e;
    knot *next;
};
class poly
{
private:
    knot head_p;

public:
    poly();
    poly(const poly &);
    void print();
    int calculate(int x);
    friend poly operator+(const poly &p1, const poly &p2);
    friend poly operator-(const poly &p1, const poly &p2);
    ~poly();
};

int main()
{
    poly one;
    poly two;
    cout << "打印第一个对象one" << endl;
    one.print();
    cout << endl;

    cout << "打印第二个对象two" << endl;
    two.print();
    cout << endl;

    poly three = one + two;
    poly four = one - two;
    cout << "打印第三个对象：three=one+two" << endl;
    three.print();
    cout << endl;

    cout << "打印第四个对象：four=one-two" << endl;
    four.print();
    cout << endl;

    cout << "请输入x，带入多项式one并计算" << endl;
    int x;
    cin >> x;
    cout << one.calculate(x);
}

poly::poly()
{
    cout << "请输入多项式，输入格式为 n c1 e1 c2 e2 … cn en" << endl
         << ",其中n是多项式的项数，ci,ei分别是第i项的系数和指数，序列按指数降序排列；";
    cout << endl;
    cin >> head_p.c; //用c来存储节点个数

    knot *temp = new knot;
    cin >> temp->c >> temp->e;
    head_p.next = temp;

    knot *p = temp;
    for (int i = 0; i < head_p.c - 1; i++)
    {
        knot *temp = new knot;
        cin >> temp->c >> temp->e;
        p->next = temp;
        p = temp;
    }
    p->next = nullptr;
}
poly::poly(const poly &copy)
{
    head_p.c = copy.head_p.c;
    knot *temp = new knot(*copy.head_p.next);
    head_p.next = temp;

    knot *p1, *p2;
    p1 = temp;
    p2 = copy.head_p.next->next;
    for (int i = 0; i < head_p.c - 1; i++)
    {
        knot *temp = new knot(*p2);
        p2 = p2->next;
        p1->next = temp;
        p1 = temp;
    }
    p1->next = nullptr;
}
void poly::print()
{
    cout << head_p.c;
    knot *p = head_p.next;
    while (p)
    {
        cout << "," << p->c << "," << p->e;
        p = p->next;
    }
}
int poly::calculate(int x)
{
    int sum = 0;
    knot *p = head_p.next;
    while (p)
    {
        sum += p->c * pow(x, p->e);
        p = p->next;
    }
    return sum;
}
poly operator+(const poly &p1, const poly &p2)
{
    poly sum = p1;
    int count = 0;
    knot *pt, *pp1;
    pt = &(sum.head_p);
    pp1 = sum.head_p.next; //pt是跟随指针，pp1是前指针，插入在pt的后面，pp1的前面
    for (knot *pp2 = p2.head_p.next; pp2; pp2 = pp2->next)
    {
        while (pp1 && pp1->e > pp2->e)
        {
            pt = pp1;
            pp1 = pp1->next;
        }
        if (pp1 && pp1->e == pp2->e)
            pp1->c += pp2->c;
        else
        {
            knot *temp = new knot(*pp2);
            pt->next = temp;
            temp->next = pp1;
            pt = temp;
            count++;
        }
    }
    sum.head_p.c += count;
    return sum;
}
poly operator-(const poly &p1, const poly &p2)
{
    poly sum = p1;
    int count = 0;
    knot *pt, *pp1;
    pt = &(sum.head_p);
    pp1 = sum.head_p.next; //pt是跟随指针，pp1是前指针，插入在pt的后面，pp1的前面
    for (knot *pp2 = p2.head_p.next; pp2; pp2 = pp2->next)
    {
        while (pp1 && pp1->e > pp2->e)
        {
            pt = pp1;
            pp1 = pp1->next;
        }
        if (pp1 && pp1->e == pp2->e)
        {
            pp1->c -= pp2->c;
            if (pp1->c == 0)
            {
                knot *temp = pp1->next;
                delete pp1;
                pt->next = temp;
                count--;
            }
        }
        else
        {
            knot *temp = new knot(*pp2);
            temp->c *= -1;
            pt->next = temp;
            temp->next = pp1;
            pt = temp;
            count++;
        }
    }
    sum.head_p.c += count;
    return sum;
}
poly::~poly()
{
    knot *p1, *p2;
    p1 = p2 = head_p.next;
    while (p1->next)
    {
        p2 = p1;
        p1 = p1->next;
        delete p2;
    }
    delete p1;
}
