#include<iostream>
#include<iomanip>
using namespace std;
//black:1;white:-1;empty:0
//chessboard has 19 lines;

int board[21][21];
bool flag[21][21];
int row,line,color;

void inputboard();
void outputboard(int color=0);
void setcolor();
bool check(int,int);

int main()
{
    cout<<"set the board"<<endl;
    inputboard();
    cout<<"now show the current chessboard "<<endl;
    outputboard();
    cout<<"put your piece,remember 1 represents the black and -1 represents the white"<<endl;
    cin>>row>>line>>color;
    board[row][line]=color;
    
    setcolor();          //set the peripheral piece's color
    if (check(row,line))
    {
        cout<<"alive,and here comes the chessboard"<<endl;
        outputboard(color);
    }
    else 
    {
        cout<<"the piece is dead,and the chessboard changed into"<<endl;
        outputboard();
    }
}

bool check(int row,int line)
{
    if (board[row][line]==-color)
        return false;
    else if(!board[row][line-1]||!board[row-1][line]||!board[row+1][line]||!board[row][line+1])
        return true;
    else
    {
        board[row][line]=-color;
        flag[row][line]=true;
        if (check(row-1,line)||check(row+1,line)||check(row,line+1)||check(row,line-1))
            return true;
        else return false;
    }
}

void setcolor()
{
    for (int i=0;i<21;i++)
        board[0][i]=board[20][i]=board[i][0]=board[i][20]=-color;
}

void inputboard()
{
    for (int i=1;i<20;i++)
        for (int j=1;j<20;j++)
            cin>>board[i][j];
}

void outputboard(int color)
{
    for (int i=1;i<20;i++)
        for (int j=1;j<20;j++)
            if (flag[i][j])
                board[i][j]=color;
    int i=1;
    cout<<"   ";
    while (i<20)
        cout<<setw(2)<<i++<<" ";
    cout<<endl<<"----------------------------------------------------------------------"<<endl;
    
    for (int i=1;i<20;i++)
    {
        cout<<setw(2)<<i;
        cout<<'|';
        for (int j=1;j<20;j++)
            cout<<setw(2)<<board[i][j]<<" ";
        cout<<endl;
    }
}
