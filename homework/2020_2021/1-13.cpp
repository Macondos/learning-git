#include<iostream>
#include<cstring>
#include<string>
using std::cin;
using std::cout;
using std::endl;
using std::string;
class Cd
{
	private:
		char performers[50];
		char label[20];
		int selections;
		double playtime;
	public:
		Cd(const char * s1, const char * s2, int n, double x):selections(n),playtime(x)
			{strcpy(performers,s1);strcpy(label,s2);};
		Cd(const Cd & d);
		Cd():selections(0),playtime(0){strcpy(performers,"null");strcpy(label,"null");};
		virtual ~Cd(){};
		virtual void Report() const;
		Cd & operator=(const Cd & d);
};

class Classic:public Cd
{
	private:
		string work;
	public:
		Classic(const string& s,const char * s1,const char * s2, int n, double x):Cd(s1,s2,n,x),work(s){};
		Classic():Cd(),work("null"){};
		~Classic(){};
		void Report() const{cout<<work<<",";Cd::Report();};
		Classic& operator=(const Classic& s);
};

Cd::Cd(const Cd & d):selections(d.selections),playtime(d.playtime)
{
	strcpy(performers,d.performers);
	strcpy(label,d.label);
}
Cd& Cd::operator=(const Cd & d)
{
	selections=d.selections;
	playtime=d.playtime;
	strcpy(performers,d.performers);
	strcpy(label,d.label);
	return *this;
}
void Cd::Report() const
{
	cout<<performers<<","<<label<<","<<selections<<","<<playtime<<endl;
}
Classic& Classic::operator=(const Classic& s)
{
	Cd::operator=(s);
	work=s.work;
	return *this;
}



void Bravo(const Cd & disk);
int main()
{
	Cd c1("Beatles", "Capitol", 14, 35.5);
	Classic c2 = Classic("Piano Sonata in B flat, Fantasia in C", "Alfred Brendel", "Philips", 2, 57.17);
	Cd *pcd = &c1;
	cout << "Using object directly:\n";
	c1.Report();
	c2.Report();
	cout << "Using type cd * pointer to objects:\n";
	pcd->Report();
	pcd = &c2;
	pcd->Report();
	cout << "Calling a function with a Cd reference argument:\n";
	Bravo(c1);
	Bravo(c2);
	cout << "Testing assignment: "; //注意此处冒号后有一个空格
	Classic copy;
	copy = c2;
	copy.Report();
	return 0;
}
void Bravo(const Cd & disk) 
{
disk.Report();
}

