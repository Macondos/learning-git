#include<iostream>
#include<iomanip>
using namespace std;

class students
{
	private:
		friend class operate;
		int num;
		int classs;
		string name;
		double grades[3];
		double sum;
		void aver(){sum=0;for (int i=0;i<3;i++)sum+=grades[i]; };
	public:
		students();

		const students& operator=(const students& s);

		bool isdelete(bool hi,int tempnum,const string& tempname)const
		{return ((hi&&tempnum==num)||(!hi&&tempname==name));};
		
		bool isselect(int temp) const
		{return (temp==num||temp==classs);};
		
		friend ostream& operator<<(ostream& o,const students& s);
		
		friend bool operator<(const students& s1,const students& s2)
        {return ((s1.classs<s2.classs)||(s1.classs==s2.classs&&s1.sum>s2.sum));}
		
		friend bool operator>(const students&s1,const students& s2){return s2<s1;}
};
void output_column();

int main()
{
    int choice;
	int k=0; //k is used to mark which object is deleted!;count the numbers of the object;
	students* p[3];
	string flag;
    while (1)
    {
        output_column();
        cin>>choice;
        if (choice==6)
            break;
        switch (choice)
        {
            case 1:
				do
				{
					static int i=0;
					p[i++]=new students(); 
					cout<<"continue?"<<endl;
					cin>>flag;
			 	 }while (!flag.compare("yes"));
                break;
            case 2:
				do
				{
					int tempnum;
					string tempname;
					bool hi=true;
					if (!(cin>>tempnum))
					{
						cin.clear();
						cin>>tempname;
						hi=false;
					}
					for (int i=0;i<3-k;i++)
						if (p[i]->isdelete(hi,tempnum,tempname))
						{
							for (int j=i;j<2-k;j++)
								*(p[j])=*(p[j+1]);
							delete p[2-k];
							k++;
						}
					for (int i=0;i<3-k;i++)
						cout<<*(p[i]);
					cout<<"continue?"<<endl;
					cin>>flag;
				 }while (!flag.compare("yes"));		
                break;
            case 3:
				do 
				{
					bool hi=false;
					int temp;
					cin>>temp;
					for (int i=0;i<3-k;i++)
						if (p[i]->isselect(temp))
						{
							cout<<*(p[i]);
							hi=true;
						}
					if (!hi)
					{
						cout<<"there is no eligible student";
						break;
					}
					cout<<"continue?"<<endl;
					cin>>flag;
				}while (!flag.compare("yes"));
                break;
            case 4:
                for (int i=0;i<3-k;i++)
					for (int j=0;j<2-k-i;j++)
						if (p[j]>p[j+1])
						{
							students* temp=p[j];
							p[j]=p[j+1];
							p[j+1]=temp;
						}
            case 5:
                for (int i=0;i<3-k;i++)
					cout<<*(p[i]);
                break;
        }
    }
}



void output_column()
{
    cout<<"1.input"<<endl<<"2.delete"<<endl<<"3.select"
        <<endl<<"4.order"<<endl<<"5.output"<<endl<<"6.quit"
        <<endl<<"please input your option"<<endl;
}
students::students()
{
	cout<<"Id ";
	cin>>num;
	cout<<"class ";
	cin>>classs;
	cout<<"name ";
	cin>>name;
	for (int i=0;i<3;i++)
	{
		cout<<"score"<<i+1<<" ";
		cin>>grades[i];
	}
	aver();
}   
ostream& operator<<(ostream& o,const students& s)
{
	o<<s.num<<","<<s.classs<<","<<s.name<<",";
	for (int j=0;j<3;j++)
		o<<fixed<<setprecision(1)<<s.grades[j]<<",";
	o<<fixed<<setprecision(1)<<s.sum<<endl;
	return o;
}
const students& students::operator=(const students& s)
{
	num=s.num;
	classs=s.classs;
	name=s.name;
	for(int i=0;i<3;i++)
		grades[i]=s.grades[i];
	sum=s.sum;
	return *this;
}

