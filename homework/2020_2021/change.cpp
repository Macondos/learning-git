#include<iostream>
#include<iomanip>
using namespace std;
typedef struct students* hi;

void print(hi,int flag);
int mfind(hi);
void swap(struct students& one,struct students& two);
void arrange(hi);
struct students
{
    int clas;
    int num;
    string name;
    double grades[3];
    double sum;
};

int main()
{
    struct students stu[3]={11, 10001, "Zhang",{99.5, 88.5,89.5}, 0,
                            12, 10002, "Yang", {77.9, 56.5, 87.5},0,
                            11, 10003, "Liang",{92.5, 99.0, 60.5},0,};
    for (int i=0;i<3;i++)
        for (int j=0;j<3;j++)
            stu[i].sum+=stu[i].grades[j]; 
    int flag=mfind(stu);
    arrange(stu);
    print(stu,flag);
}

void arrange(hi stu)
{
    int front,back;
    for (int i=0;i<3-i;i++)
    {
        front=back=i;
        for (int j=i+1;j<3-i;j++)
        {
            if (stu[j].clas<stu[front].clas||(stu[j].clas==stu[front].clas&&stu[j].sum>stu[front].sum))
                front=j;
            else if (stu[j].clas>stu[back].clas||(stu[j].clas==stu[back].clas&&stu[j].sum<stu[back].sum))
                back=j;
        }
        swap(stu[i],stu[front]);
        swap(stu[2-i],stu[back]);
    }
}

void swap(struct students& one,struct students& two)
{
    struct students temp;
    temp=one;
    one=two;
    two=temp;
}

int mfind(hi stu)
{
    struct students temp={0};
    cin>>temp.num>>temp.clas>>temp.name;
    for (int i=0;i<3;i++)
        cin>>temp.grades[i];
        for (int j=0;j<3;j++)
            temp.sum+=temp.grades[j]; 
    for (int i=0;i<3;i++)
        if (stu[i].num==temp.num)
        {
            swap(temp,stu[i]);
            return temp.num;
        }
    return -1;
}

void print(hi stu,int flag)
{
    for (int j=0;j<3;j++)
    {
        if (stu[j].clas==stu[j-1].clas&&j)
            cout<<"  ";
        else  cout<<stu[j].clas;
        cout<<" "<<stu[j].num<<" "<<stu[j].name;
        for (int i=0;i<3;i++)
            cout<<" "<<fixed<<setprecision(1)<<stu[j].grades[i];
        if (flag==stu[j].num)
            cout<<" modified";
        cout<<endl;
    }
}
