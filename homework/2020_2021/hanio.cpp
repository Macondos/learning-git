#include<iostream>
using std::cin;
using std::cout;
void hanio(long m,int* num);

int main()
{
    long m;
    cin>>m;
    int num[7]={0,0,0,0,0,0,1};
    hanio(m,num);
    int i;
    for (i=1;i<7;i++)
    {    
        if (num[i])
            break;
    }
    for (int j=i;j<7;j++)
        cout<<num[j];
}

void hanio(long m,int* num)
{
    if (m==1)
        return;
    else 
    {
        for (int i=1;i<7;i++)
            num[i]*=2;
        num[6]++;
        for (int i=6;i>=1;i--)
        {
            if (num[i]/10)
            {
                num[i]%=10;
                num[i-1]++;
            }
        }
        num[0]=0;
        return hanio(--m,num);
    }
}


