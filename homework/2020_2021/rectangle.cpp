#include<iostream>
using namespace std;

class rec
{
	private:
		int length;
		int width;
	public:
		int area(){return length*width;};
		rec(int len=0,int wid=0):length(len),width(wid){};
};

class cuboid:public rec
{
	private:
		int height;
	public:
		int vol(){return height*area();};
		cuboid(const rec& o,int h):rec(o),height(h){};
		cuboid(int len=0,int wid=0,int h=0):rec(len,wid),height(h){};
};

int main()
{
	int templen1,tempwid1,templen,tempwid,temph;
	cin>>templen1>>tempwid1>>templen>>tempwid>>temph;
	rec one(templen1,tempwid1);
	cuboid two(templen,tempwid,temph);
	cout<<one.area()<<" "<<two.vol();
}

