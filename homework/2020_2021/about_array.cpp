#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int int_ar[5]={1,2,3,4,5}; //创建一个整数数组
    int *pint=int_ar; //创建一个指向数组的指针，这个指针加1，步长就是一个int长度
    int (*pint_array)[5]=&int_ar; //创建一个指向五个元素的数组的指针，这个指针加1.步长就是一个int数组的长度
    cout<<"int_ar is:"<<int_ar<<endl;
    cout<<"pint is:"<<pint<<endl;
    cout<<"&int_ar is:"<<&int_ar<<endl;
    cout<<"(*pint_array)[5],pint_arrar is:"<<pint_array<<endl;

    int i1=1,i2=2,i3=3,i4=4,i5=5;
    int *ppa[5]={&i1,&i2,&i3,&i4,&i5}; //创建一个长度是5，每一个元素是int指针的数组
    int **ppb=ppa;  //创建指针的指针，ppb指向的地址为数组首地址，数组每一个元素是指针
    **ppb=5; //*(*ppb)=5;
    **(ppb+1)=4; // *(*(ppb+1))=4;

    for(int i=0;i<5;i++)
        cout<<*ppa[i]<<"\t";

    char *pchar[5]={  //创建一个有5个元素的数组，每隔元素是一个字符串指针
        "平顶山",
        "洛阳",
        "焦作",
        "南阳",
        "商丘"
    };
    cout<<pchar[3]<<endl;
    //下面创建的是二维数组的指针
    int intArray[4][3]={
        {0,1,2},
        {3,4,5},
        {6,7,8},
        {9,10,11}
    };
    cout<<endl;
    int (*p)[3]=intArray;
    for(int i=0;i<4;i++){
        for (int j=0;j<3;j++)
            cout<<setw(5)<<*(*(p+i)+j); //等效于cout<<setw(5)<<p[i][j];
        cout<<endl;
    }
    cout<<endl;
    return 0;
}
