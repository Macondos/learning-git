#include<iostream>
#include<set>
#include<iterator>
#include<algorithm>
using namespace std;
struct data
{
    int time;
    set<int> nation;
};
int main()
{
    int count;
    cin>>count;
    data arr[count];
    for (int i=0;i<count;i++)
    {
        int temp_number;
        cin>>arr[i].time>>temp_number;

        int temp_nation[temp_number];
        for (int i=0;i<temp_number;i++)
            cin>>temp_nation[i];
        set<int> t_nation(temp_nation,temp_nation+temp_number);
        //input the data
        arr[i].nation=t_nation;
    }
    // output the data
    for (int i=0;i<count;i++)
    {
        int start=i-86400>=0?i-86400:0;
        while(arr[start].time<=i-86400)
            start++;
        //combine the set
        set<int>tpc;
        insert_iterator< set<int> >pr(tpc,tpc.begin());
        for (int k=start;k<=i;k++)
            set_union(tpc.begin(),tpc.end(),arr[k].nation.begin(),arr[k].nation.end(),pr);
        cout<<tpc.size()<<endl;
    }

}