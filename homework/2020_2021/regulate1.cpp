#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

struct students
{
public:
	int num;
	string name;
	double grades[4];
	//grades[0]代表数学成绩，grades[1]代表英语成绩，grades[2]代表计算机成绩
	//grades[3]代表平均成绩
	void set_average(); //计算平均分
	students();			//构造函数
	//下面使用非类型参数模板函数，优势在于可以通过参数n的控制实现不同的需求
	//该友元函数实现不同的打印需求
	template <int n>
	friend void show(const students &);
	template <int n>
	void calculate(const students *a, int num);
	//注：此处由于浅复制即可满足要求，故可使用默认重载=运算符而无需重新重载
};
template <int n>							   //该函数计算各门课程的平均成绩、最高分、最低分、不及格人数、
void calculate(const students *, int num = 7); //60-69分人数、70-79分人数、 80-89分人数、90分以上人数。
template <int n>
void show(const students &s);
void search_name(students *a, string);
void search_num(students *a, int temp);
//下面函数对各科成绩以及平均分进行排序
//subject代表是按grades数组里哪一关键字项排序
//0代表数学成绩，1代表英语成绩，2代表计算机成绩,3代表平均成绩
template <int subject>
void qsort(students *, int low, int high);
template <int subject>
void quicksort(students *a, int num = 7) { qsort<subject>(a, 0, num - 1); }
template <int subject>
int partition(students *, int low, int high);

int main()
{
	cout << "请依次输入学生的学号，姓名，数学，英语，计算机成绩" << endl;
	students a[7];
	cout << "你所输入的信息如下：" << endl;
	for (int i = 0; i < 7; i++)
		show<4>(a[i]);
	cout << endl;
	/////////////////////////////////////////////////////////////////
	cout << "按数学成绩排名的名单顺序：" << endl;
	quicksort<0>(a);
	for (int i = 0; i < 7; i++)
		show<0>(a[i]);
	calculate<0>(a);
	cout << endl;
	/////////////////////////////////////////////////////////////////
	cout << "按英语成绩排名的名单顺序：" << endl;
	quicksort<1>(a);
	for (int i = 0; i < 7; i++)
		show<1>(a[i]);
	calculate<1>(a);
	cout << endl;
	/////////////////////////////////////////////////////////////////
	cout << "按计算机成绩排名的名单顺序：" << endl;
	quicksort<2>(a);
	for (int i = 0; i < 7; i++)
		show<2>(a[i]);
	calculate<2>(a);
	cout << endl;
	/////////////////////////////////////////////////////////////////
	cout << "按平均成绩排名的名单顺序：" << endl;
	quicksort<3>(a);
	for (int i = 0; i < 7; i++)
		show<3>(a[i]);
	calculate<3>(a);
	cout << endl;
	////////////////////////////////////////////////////////////////
	//按学号查找
	cout<<"查找学号为001的同学"<<endl;
	search_num(a, 001);
	//按名字查找
	cout<<"查找名字为lh的同学（）"<<endl;
	string temp = "lh";
	search_name(a, temp);
}
template <int n>
void calculate(const students *a, int num)
{
	cout << "最高分：" << a[num - 1].grades[n] << "	最低分：" << a[0].grades[n] << endl;
	;
	int failed = 0, bt60_69 = 0, bt70_79 = 0, bt80_89 = 0, above90 = 0;
	for (int i = 0; i < num; i++)
		if (a[i].grades[n] < 60)
			failed++;
		else if (a[i].grades[n] <= 69)
			bt60_69++;
		else if (a[i].grades[n] <= 79)
			bt70_79++;
		else if (a[i].grades[n] <= 89)
			bt80_89++;
		else
			above90++;
	cout << "不及格人数:" << failed << ",60-69分人数:" << bt60_69
		 << ",70-79分人数:" << bt70_79 << ",80-89分人数:" << bt80_89
		 << ",90分以上人数:" << above90 << endl;
}
students::students()
{
	cout << "学号： ";
	cin >> num;
	cout << "姓名： ";
	cin >> name;
	cout << "数学分数： ";
	cin >> grades[0];
	cout << "英语分数： ";
	cin >> grades[1];
	cout << "计算机分数： ";
	cin >> grades[2];
	set_average();
}
//n==0表示只打印数学成绩，n==1只打印英语成绩，n==2只打印计算机成绩，n==3只打印平均成绩
//n==4打印所有信息
template <int n>
void show(const students &s)
{
	cout << "学号： ";
	cout << s.num;
	cout << ",姓名： ";
	cout << s.name;
	if (n == 0 || n == 4)
		cout << ",数学分数： " << fixed << setprecision(1) << s.grades[0];
	if (n == 1 || n == 4)
		cout << ",英语分数： " << fixed << setprecision(1) << s.grades[1];
	if (n == 2 || n == 4)
		cout << ",计算机分数： " << fixed << setprecision(1) << s.grades[2];
	if (n == 3)
		cout << ",平均分数：" << fixed << setprecision(1) << s.grades[3];
	cout << endl;
}
void students::set_average()
{
	double sum = 0;
	for (int i = 0; i < 3; i++)
		sum += grades[i];
	grades[3] = sum / 3.0;
}

template <int subject>
void qsort(students *a, int low, int high)
{
	if (low < high)
	{
		int pivot = partition<subject>(a, low, high);
		qsort<subject>(a, low, pivot - 1);
		qsort<subject>(a, pivot + 1, high);
	}
}
template <int subject>
int partition(students *a, int low, int high)
{
	students temp = a[low];
	int pivot = a[low].grades[subject];
	while (low < high)
	{
		while (low < high && a[high].grades[subject] > pivot)
			high--;
		a[low] = a[high];
		while (low < high && a[low].grades[subject] <= pivot)
			low++;
		a[high] = a[low];
	}
	a[low] = temp;
	return low;
}
void search_num(students *a, int temp)
{
	for (int i = 0; i < 7; i++)
		if (a[i].num == temp)
		{
			show<4>(a[i]);
			break;
		}
}
void search_name(students *a, string temp)
{
	int position;
	int count = 0;
	for (int i = 0; i < 7; i++)
		if (a[i].name == temp)
		{
			position = i;
			count++;
		}
	if (count == 1)
		show<4>(a[position]);
	else
	{
		cout << "存在重名，请输入学号" << endl;
		int num;
		cin >> num;
		search_num(a, num);
	}
}