#include <iostream>
#include <cstring>
#include <algorithm>
#include <cmath>
using namespace std;

int main()
{
    string fsearch;
    string lsearch;
    cin >> fsearch;
    cin >> lsearch;
    int size = fsearch.size();
    int count = 0;
    reverse(lsearch.begin(), lsearch.end());
    for (int i = 0; i < size-1; i++)
    {
        char temp = fsearch[i];
        char templatter_f = fsearch[i + 1];
        char templatter_l;
        int j = 0;
        for (; j < size-1; j++)
        {
            if (lsearch[j] == temp)
            {
                templatter_l = lsearch[j + 1];
                break;
            }
        }
        if (j!=(size-1)&&templatter_f == templatter_l)
            count++;
    }
    cout << pow(2, count);
}