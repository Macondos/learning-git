#include<iostream>
using namespace std;

template<typename t>
class one
{
	private:
		t hi;
	public:
		one(t i=0):hi(i){};
		virtual void read(){cout<<"call one"<<endl;}
};
template<typename t>
class two:public one<t>
{
	public:
		two(t i=0):one<t>(i){};
		void read(){cout<<"call two"<<endl;}
};

int main()
{
	one<int>* pt;
	two<int> apple;
	pt=&apple;
	(*pt).read();
}


