#include <iostream>
#include <bitset>
using namespace std;

int main()
{
    cout << "输入一个int类型的数，显示其在内存中的存储" << endl;
    int num1;
    cin >> num1;
    bitset<32> bitset1(num1);
    cout << "其在内存中的存储形式为：";
    cout << bitset1 << endl;
    cout << "输入一个float类型的数，显示其在内存中的存储" << endl;
    float num2;
    cin >> num2;
    bitset<32> bitset2((int &)num2);
    //这里将float在内存中的存储强制作为int来读取，
    //这样就可以读取float在内存中的存储
    cout << "其在内存中的存储形式为：";
    cout << bitset2;
}