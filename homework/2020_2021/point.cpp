#include<iostream>
#include<cmath>
using namespace std;

class Point
{
	private:
		int x,y;
	public:
	friend int calcH(const Point&, const Point&);
	friend int calcV(const Point&, const Point&);
	friend ostream& operator<<(ostream& o,const Point& h){o<<h.x<<" "<<h.y;return o;};
	Point(int i = 0, int j = 0):x(i),y(j){};
};

int calcH(const Point&a, const Point&b)
{
	return abs(a.x-b.x);
}
int calcV(const Point&a, const Point&b)
{
	return abs(a.y-b.y);
}

int main()
{
	int x1,x2,y1,y2;
	cin>>x1>>y1>>x2>>y2;
	Point one(x1,y1);
	Point two(x2,y2);
	calcH(one,two);
	cout<<" ";
	calcV(one,two);
	cout<<one<<two;
}


