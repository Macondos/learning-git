#include<valarray>
#include<iostream>
using namespace std;

template<typename t1,typename t2>
class Pair
{
	private:
		t1 array1;
		t2 array2;
	public:
		Pair(){};
		void change1(int i,int a){array1[i]=a;};
		void change2(int i,int a){array2[i]=a;};
		int print1(int i){return array1[i];};
		int print2(int i){return array2[i];};
		int get(){return array2.sum();};
		void resize(int i){array1.resize(i);array2.resize(i);}
};

typedef Pair<valarray<int>,valarray<int>> pairint;

class Wine
{
	private:
		string name;
		int year;
		pairint p;
	public:
		Wine():name("null"),year(0),p(){};
		Wine(const char* l,int y,const int yr[],const int bot[]):name(l),year(y)
		{
			p.resize(year);
			for (int i=0;i<year;i++)
			{
				p.Pair::change1(i,yr[i]);
				p.Pair::change2(i,bot[i]);
			}
		};
		Wine(const char*l,int y):name(l),year(y),p(){p.resize(year);};
		void Getbottles();
		string & Label(){return name;};
		int sum(){return p.pairint::get();};
		void show();
};

void Wine::Getbottles()
{
	int temp1,temp2;
	cout<<"Enter "<<name<<" data for "<<year<<" year(s): "<<endl;
	for (int i=0;i<year;i++)
	{
		cout<<"Enter year: "<<endl;	
		cin>>temp1;
		cout<<"Enter bottles for that year: "<<endl;
		cin>>temp2;
		p.pairint::change1(i,temp1);
		p.pairint::change2(i,temp2);
	}
}
void Wine::show()
{
	cout<<"Wine: "<<name<<endl;
	cout<<"        Year    Bottles"<<endl;
	for (int i=0;i<year;i++)
		cout<<"        "<<p.pairint::print1(i)<<"    "<<p.pairint::print2(i)<<endl;
}

int main()
{
	cout<<"Enter name of wine: "<<endl;
	char lab[50];
	cin.getline(lab,50);
	cout<<"Enter number of years: "<<endl;
	int yrs;
	cin>>yrs;

	Wine holding(lab,yrs);
	holding.Getbottles();
	holding.show();
	const int yr=3;
	int y[yr]={1993,1995,1998};
	int b[yr]={48,60,72};
	Wine more("Gushing Grape Red",yr,y,b);
	more.show();
	cout<<"Total bottles for "<<more.Label()<<": "<<more.sum()<<endl;
	cout<<"Bye\n";
}
