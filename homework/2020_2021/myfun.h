/*代码采用先对数据排序，后查找的方法，以避免如果数据庞大，每次检索后都要排序，导致算法性能降低
采用结构体数组存储每一个学生的数据，对一维结构体数组，采用快速排序
排序准则：班级代码从小到大，相同班级，成绩从高到地。
由于没有根据单科成绩和姓名、学号排序，所以这些列数据的查找只能按照数组从头到尾查找的办法。
即使是商业数据库，在没有排序的字段上检索也很慢
只有在班级代码上支持二分法快速查找,如果需要在多个列上实现二分法快速查找，需要创建多个根据该列排序的数组。
感觉已经超出了作业的要求范围。除非咱们都不用快速查找算法，那样就简单多了。
中英文夹杂，hhh
*/
//struct Table define student's information
struct Table
{
    int student_id;
    int class_id;
    string name;
    float achievement1;        //achievement of course 1
    float achievement2;        //achievement of course 1
    float achievement3;        //achievement of course 1
    float achievement_sum = 0; //the sum of all achievements
    //define the = > < operator between struct Table;
    bool operator>=(const Table &t2)
    {
        if (class_id > t2.class_id) //如果student班级号大于t2的班级号，则student大于t2,排序的时候在后面
        {
            return class_id > t2.class_id;
        }
        else if (class_id == t2.class_id) ////如果同一个班级，则比较总分，总分大的，认为是小的student，排序可以排在前面
        {
            return achievement_sum <= t2.achievement_sum;
        }
        else
        {
            return false;
        }
    }
    bool operator<=(const Table &t2)
    {
        if (class_id < t2.class_id) //参考上面的注释
        {
            return class_id < t2.class_id;
        }
        else if (class_id == t2.class_id) //参考上面的注释
        {
            return achievement_sum >= t2.achievement_sum;
        }
        else
        {
            return false;
        }
    }
    bool operator==(const Table &t2)
    {
        return class_id == t2.class_id && achievement_sum == t2.achievement_sum;
    }
};

int lines = 7; //define table rows
Table students[] = {
    {10001, 11, "Zhang", 99.5, 88.5, 89.5},
    {10002, 12, "Yang", 77.9, 56.5, 87.5},
    {10003, 11, "Liang", 92.5, 99.0, 60.5},
    {10004, 11, "Cai", 89.6, 56.9, 90.5},
    {10005, 14, "Fu", 55.6, 67.9, 98.9},
    {10006, 12, "Mao", 22.1, 45.9, 99.2},
    {10007, 13, "Zhan", 35.6, 67.9, 88.0}};
int tags[7] = {0}; //the value of the tags[i] is 1 if students[i] is find and selected//
struct former_latter
{ //save formmer digit and latter digit
    float former;
    float latter;
};
union Arg
{                  //save search arg,if search by name,U should not input former and latter
    char name[20]; //union类型内部变量不支持string，所以用C字符串
    former_latter fm;
} arg;

//some functions here
void indexAllData(int left, int right); //index data on class(ascent) and achievement_sum(desc)
void inputSearchKeyword();              //
void findStudent(int index, Arg arg, int begin, int end);
void printResull(); //show search result
//二分法查找根据班级排序的数组,由于二分法查找的元素位置，未必是元素第一次出现的位置
//所以设置direct变量标记是查找最大值还是最小是。
//direct为true，找到最小值后，还应该检索这个值前面还有没和它一样的
//direct为false，找到最大值后，还应该检索这个值后面还有没和它一样的
int binarysearch(int key, int &begin, int &end, bool direct);