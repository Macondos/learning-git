#include<iostream>
#include<iostream>
#include<cstring>
using namespace std;

class dma
{
	private:
		char * label;
		int rating;    
	public:
		dma(const char * l = "null", int r = 0);
		dma(const dma & rs);
		virtual ~dma();
		dma & operator=(const dma & rs);
		virtual void view() const=0;
};

class baseDMA:public dma
{
	public:
		baseDMA(const char * l = "null", int r = 0):dma(l,r){};
		baseDMA(const baseDMA & rs):dma(rs){};
		virtual ~baseDMA(){};
		baseDMA & operator=(const baseDMA & rs){dma::operator=(rs); return *this;};
		void view() const{dma::view();};		
};

class lacksDMA :public dma
{
private:
    enum { COL_LEN = 40};
    char color[COL_LEN];
public:
    lacksDMA(const char * c = "blank", const char * l = "null",int r = 0);
    lacksDMA(const lacksDMA &);
	lacksDMA & operator=(const lacksDMA &);
	void view() const{dma::view();cout<<"color: "<<color<<endl;};
};

class hasDMA :public dma
{
private:
    char * style;
public:
    hasDMA(const char * s = "none", const char * l = "null",int r = 0);
    hasDMA(const hasDMA & hs);
    ~hasDMA();
    hasDMA & operator=(const hasDMA & rs);  
    void view() const{dma::view();cout<<"style: "<<style<<endl;};
};

int main()
{
	int num;
	cout<<"input how many pointers you want"<<endl;
	cin>>num;
	cin.get();
	dma* p[num];
	string label;
	int rating;
	int kind;
	for (int i = 0; i <num; i++)
	{
	   cout << "Enter label: ";
	   getline(cin,label);
	   cout << "Enter rating: ";
	   cin >> rating;
	   cout << "Enter 1 for baseDMA;enter 2 for lacksDMA;enter 3 for hasDMA;"<<endl;
	   try
	   {
		   if (!(cin>>kind)||(kind !=1&& kind !=2&&kind!=3))
				throw "bad input i don't want to play with you!!!";
	   }
	   catch (const char* l)
	   {
		   cout<<l<<endl;
		   return 0;
	   }
	   cin.get();
	   if (kind ==1)
		   p[i]=new baseDMA(label.c_str(),rating);
	   else if (kind==2)
	   {
		   cout<<"input color"<<endl;
		   string color;
		   getline(cin,color);
		   p[i]=new lacksDMA(color.c_str(),label.c_str(),rating);
	   }
	   else 
	   {
		   cout<<"input style"<<endl;
		   string style;
		   getline(cin,style);
		   p[i]=new hasDMA(style.c_str(),label.c_str(),rating);
	   }
	}
	cout << endl;
	for (int i = 0; i <num; i++)
	{
	   p[i]->view();
	   cout << endl;
	}
			  
	for (int i = 0; i < num; i++)
	{
	   delete p[i];  // free memory
	}
	return 0; 
}

dma::dma(const char * l, int r)
{
    label = new char[std::strlen(l) + 1];
    std::strcpy(label, l);
    rating = r;
}

dma::dma(const dma& rs)
{
    label = new char[std::strlen(rs.label) + 1];
    std::strcpy(label, rs.label);
    rating = rs.rating;
}

dma::~dma()
{
    delete [] label;
}

dma& dma::operator=(const dma& rs)
{
    if (this == &rs)
        return *this;
    delete [] label;
    label = new char[std::strlen(rs.label) + 1];
    std::strcpy(label, rs.label);
    rating = rs.rating;
    return *this;
}
    
void dma::view() const 
{
    cout<< "Label: " << label << std::endl;
    cout<< "Rating: " << rating << std::endl;
}

// lacksDMA methods
lacksDMA::lacksDMA(const char * c, const char * l, int r):dma(l, r)
{
    std::strncpy(color, c, 39);
    color[39] = '\0';
}

lacksDMA::lacksDMA(const lacksDMA& rs):dma(rs)
{
    std::strncpy(color,rs.color, COL_LEN - 1);
    color[COL_LEN - 1] = '\0';
}

lacksDMA& lacksDMA::operator=(const lacksDMA& rs)
{
	if (this==&rs)
		return *this;
	dma::operator=(rs);
    std::strncpy(color,rs.color, COL_LEN - 1);
    color[COL_LEN - 1] = '\0';
	return *this;
}

hasDMA::hasDMA(const char * s, const char * l, int r):dma(l,r)
{
    style = new char[std::strlen(s) + 1];
    std::strcpy(style, s);
}

hasDMA::hasDMA(const hasDMA& rs):dma(rs)
{
    style = new char[std::strlen(rs.style) + 1];
    std::strcpy(style,rs.style);
}
hasDMA::~hasDMA()
{
    delete [] style;
}

hasDMA & hasDMA::operator=(const hasDMA & hs)
{
    if (this == &hs)
        return *this;
    dma::operator=(hs);  // copy base portion
    delete [] style;         // prepare for new style
    style = new char[std::strlen(hs.style) + 1];
    std::strcpy(style, hs.style);
    return *this;
}
    
