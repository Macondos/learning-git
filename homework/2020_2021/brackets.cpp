#include <iostream>
#include <stack>
using namespace std;
struct character
{
    char ch;
    int position;
};

int main()
{
    char a[100] = {0};
    cin >> a;
    stack<character> st;
    int count = 0;

    for (int i = 0; i < 100 && a[i]; i++)
    {
        if (!st.empty() && ((st.top().ch == '(' && a[i] == ')') || (st.top().ch == '[' && a[i] == ']')))
        {
            st.pop();
            count--;
        }
        else
        {
            character temp = {a[i], i};
            st.push(temp);
            count++;
        }
    }

    int record[100] = {0};
    int k=0;

    while (!st.empty()&&k<count)
    {
        record[k] = st.top().position;
        st.pop();
        k++;
    }

    int flag = count - 1;

    for (int i = 0;a[i]; i++)
    {
        for (; record[flag] <=i&&flag>=0; flag--)
        {
            if (record[flag] == i)
            {
                if (a[i] == '[' || a[i] == ']')
                    cout << "[]";
                else
                    cout << "()";
                break;
            }
        }
        if (record[flag] > i)
            cout << a[i];
    }
}
