#include<iostream>
using std::cin;
using std::cout;

class stack
{
    private:
        char arr[20];
        int position;
        void install(char ch){if (position==0) arr[position]=ch; else arr[++position]=ch;};
        void pop(char ch){if (position!=0) --position;};
    public:
        stack(int p=0){cin>>position;arr[0]='\0';};
        void test(char ch);
        bool isempty ();
};

int main()
{
    char ch;
    stack brackets;
    while (cin.get(ch))
        brackets.test(ch);
    if (brackets.isempty())
        cout<<"Yes";
    else cout<<"No";
    return 0;
}


void stack::test(char ch)
{
    if ((ch==']'&&arr[position]=='[')||(ch==')'&&arr[position]=='('))
        pop(ch);
    else install(ch);
}

bool stack::isempty()
{
    if (position==0)
        return true;
    else return false;
}
 
        
