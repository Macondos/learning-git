//可用4位二进制数顺序0000分别表示农夫、狼、白菜和羊的位置。
//用0表在南岸，1表示在北岸。
//节点之间的关系 ++a[0]mod2 再最多有一个其他位变化；节点的关系是双向的；

#include <iostream>
#include <stack>
#include <algorithm>
#define start_flag 100
using namespace std;
void to_array(int *temp, int t);
void print(int *);
class node
{
private:
    int tag;                //tag为该节点所对应的编号
    int array[4];           //4位二进制串，表示此刻的状态
    bool state;             //state为该节点的安全状态
    bool adjacent[16];      //0-15号节点标记和该节点邻接的点
    void array_adjust();    //转化为4位二进制array
    void state_adjust();    //调整该节点的安全状态
    void adjacent_adjust(); //标注该节点的邻接点
public:
    node() : tag(0), state(true){array_adjust();adjacent_adjust();}
    node(int t) : tag(t){array_adjust();state_adjust();adjacent_adjust();}
    bool is_safe() { return state; }
    void into_stack(stack<int> &, const int *);
};

int main()
{
    int visited[16]; //用于记录前驱路径
    for (int &x : visited)
        x = -1;
    stack<int> s;

    int front = 0;
    visited[0] = start_flag;
    s.push(0);
    node temp = node(0);
    node(0).into_stack(s, visited);

    while (true)
    {
        int temp = s.top();

        if (temp == 15) //已经走到了终点
        {
            visited[temp] = front;
            print(visited);
            //从终点退回，回到路径的上一个分岔口去访问其他到终结点的路径
            visited[temp] = -1;
            s.pop();
            temp = s.top();
        }

        //当一个对于某一个节点，上一次循环没有压入节点时执行
        while (visited[temp] != -1) //此时退回上一个有分支的节点
        {
            front = visited[temp]; //正确更新前项
            visited[temp] = -1;    //回退的点更改访问状态，因为其不在正确路径上
            s.pop();
            if (!s.empty())
                temp = s.top();
        }
        if (s.empty())
            break;//此时栈为空只有可能是：while循环退栈，栈被退空，说明所有的分叉都被访问了
        //标记已经访问过,并且记录前驱节点
        visited[temp] = front;
        front = temp;
        //将这个点的所有安全的，未访问过的邻接点都压入栈
        node(temp).into_stack(s, visited);
    }
}

void print(int *visited)
{
    static int count=1;
    int temp = 15;
    stack<int> reverse_visited;
    while(temp!=start_flag)
    {
        reverse_visited.push(temp);
        temp = visited[temp];
    }
    cout<<"第"<<count<<"种方法："<<endl;
    int tarr[4];
    while(!reverse_visited.empty())
    {
        temp=reverse_visited.top();
        reverse_visited.pop();
        to_array(tarr, temp);
        cout << "河的北岸：";
        if (tarr[0]) cout << "人";if (tarr[1])cout << "狼";
        if (tarr[2]) cout << "菜";if (tarr[3])cout << "羊";
        cout << "         河的南岸：";
        if (!tarr[0])cout << "人";if (!tarr[1]) cout << "狼";
        if (!tarr[2]) cout << "菜";if (!tarr[3])cout << "羊";
        cout << endl;
    }
    count++;
    cout << endl;
}
void node::into_stack(stack<int> &s, const int *visited)
{
    for (int x = 1; x < 16; x++)
        if (visited[x] == -1 && adjacent[x] && node(x).is_safe()) //未访问安全的邻接点的都压入栈
            s.push(x);
}
void node::array_adjust()
{
    to_array(array, tag);
}
void node::state_adjust()
{
    //不安全状态的判定
    if (array[3] != array[0]) //羊不在农夫身边
        if (array[1] != array[0] || array[2] != array[0])
            state = false;
        else
            state = true;
    else
        state = true;
}
void node::adjacent_adjust()
{
    for (int i = 0; i < 16; i++)
    {
        int temp[4];
        to_array(temp, i);
        if (array[0] == ++temp[0] % 2)
            if (equal(array + 1, array + 4, temp + 1, temp + 4)) //只有农夫移动
                adjacent[i] = true;
            else if (equal(array + 2, array + 4, temp + 2, temp + 4))
                adjacent[i] = true;
            else if (equal(array + 1, array + 3, temp + 1, temp + 3))
                adjacent[i] = true;
            else if (array[1] == temp[1] && array[3] == temp[3])
                adjacent[i] = true;
            else
                adjacent[i] = false;
        else
            adjacent[i] = false;
    }
}
void to_array(int *temp, int t)
{
    for (int i = 3; i >= 0; i--)
    {
        temp[i] = t % 2;
        t /= 2;
    }
}
