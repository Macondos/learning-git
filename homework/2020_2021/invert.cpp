#include <iostream>
#include <cmath>
#include <iomanip>
//add one line comment
using namespace std;

int Matrix_Size=2;
double **Matrix=new double *[Matrix_Size];
double **InvertMatrix=new double *[Matrix_Size];
double **childMatrix=new double *[Matrix_Size-1];

void initMatrix(); //初始化矩阵
void printMatrix(int matrix_size,double **matrix);
double calculateRank(int matrix_size,double **matrix); //计算行列式的值
void getChildMatrix(int i,int j);
void invertMatrix(); //求默认矩阵Matrix的逆矩阵
void deleteNew();

int main()
{
    initMatrix();
    printMatrix(Matrix_Size,Matrix);
    
    if(calculateRank(Matrix_Size,Matrix)==0)
    {
        cout<<"行列式为0，不具有逆矩阵\n";
    }
    else
    {
        invertMatrix();
        printMatrix(Matrix_Size,InvertMatrix);
    }
    
    deleteNew();
    return 0;
}

void initMatrix()
{
    cout<<"Please size of matrix you wanna create divided vy space\nFor example 2x2,input 2:  ";
    cin>>Matrix_Size;
    cout<<"Please input "<<Matrix_Size*Matrix_Size<<" digital\n";
    for(int i=0;i<Matrix_Size;i++)
    {
        Matrix[i]=new double[Matrix_Size];
        for(int j=0;j<Matrix_Size;j++)
            cin>>Matrix[i][j];
    }
    //init child metrix
    for(int i=0;i<Matrix_Size-1;i++)
    {
        childMatrix[i]=new double[Matrix_Size-1];
        for(int j=0;j<Matrix_Size-1;j++)
            childMatrix[i][j]=0;
    }
    //init invert matrix
    for(int i=0;i<Matrix_Size;i++)
    {
        InvertMatrix[i]=new double[Matrix_Size];
        for(int j=0;j<Matrix_Size;j++)
            InvertMatrix[i][j]=0;
    }
}

void printMatrix(int matrix_size,double **matrix)
{
    for(int i=0;i<matrix_size;i++)
    {
        for(int j=0;j<matrix_size;j++)
        {
            cout<<setw(5);
            cout<<matrix[i][j];
        }
        cout<<endl;
    }
    cout<<endl;
}
double calculateRank(int matrix_size,double **tmp_matrix)
{
    int swaptimes=0; //行交换次数
    double **matrix=new double *[matrix_size];
    for(int i=0;i<matrix_size;i++)
    {
        matrix[i]=new double[matrix_size];
        for(int j=0;j<matrix_size;j++)
        {
            matrix[i][j]=tmp_matrix[i][j];
        }
    }
    //如果[0][0]元素是0，则先将它和首元素不是0的行互换
    if(matrix[0][0]==0)
    {
        for(int i=1;i<matrix_size;i++)
        {
            if(matrix[i][0]!=0)
            {
                for(int j=0;j<matrix_size;j++)
                {
                    int tmp=matrix[0][j];
                    matrix[0][j]=matrix[i][j];
                }
            }
            swaptimes++;
            break;
        }
    }
    //先进行初等行变换
    for (int i=0;i<matrix_size;i++)
    {
        for(int j=i+1;j<matrix_size;j++)
        {
            double base=matrix[j][i];
            for(int k=i;k<matrix_size;k++)
            {
                double t=matrix[j][k]-matrix[i][k]*base/matrix[i][i];
                matrix[j][k]=t;
            }
        }
    };
    //计算上三角行列式的对角线乘积
    double rank=1;
    for(int i=0;i<matrix_size;i++)
        rank*=matrix[i][i];

    //delete data copy
    for(int i=0;i<matrix_size;i++)
    {
        delete [] matrix[i];
    }
    delete [] matrix;
    return pow(-1,swaptimes)*rank; 
}

void getChildMatrix(int row,int col)
{
    for(int i1=0,i2=0;i1<Matrix_Size;i1++)
    {
        if(i1==row)
        {
            continue;
        }
        for(int j1=0,j2=0;j1<Matrix_Size;j1++)
        {
            if(j1==col)
            {
                continue;
            }          
            childMatrix[i2][j2]=Matrix[i1][j1];
            j2++;
        }
        i2++;
    }
}
//caculate invert matrix
void invertMatrix()
{   double MatrixValue=calculateRank(Matrix_Size,Matrix);
    for(int i=0;i<Matrix_Size;i++)
    {
        for(int j=0;j<Matrix_Size;j++)
        {
            getChildMatrix(i,j);
            double yuzishi=calculateRank(Matrix_Size-1,childMatrix);
            InvertMatrix[j][i]=pow(-1,i+j)*yuzishi/MatrixValue;
        }
    }
}

void deleteNew()
{
    for(int i=0;i<Matrix_Size;i++)
    {
        delete [] Matrix[i];
        delete [] InvertMatrix[i];
    }
    delete Matrix;
    delete InvertMatrix;

    for(int i=0;i<Matrix_Size-1;i++)
    {
        delete [] childMatrix[i];
    }
    delete [] childMatrix;
}
