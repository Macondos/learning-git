#include<iostream>
using namespace std;

class cnum
{
	private:
		double real;
		double imag;
	public:
		cnum(double r=0,double i=0):real(r),imag(i){};
		cnum operator+(const cnum& o)const{return cnum(real+o.real,imag+o.imag);};
		cnum operator-(const cnum& o)const{return cnum(real-o.real,imag-o.imag);};
		cnum operator*(const cnum& o)const;
		cnum operator~()const{return cnum(real,-imag);};
		friend cnum operator*(double k,cnum& o){return cnum(o.real*k,o.imag*k);};
		friend ostream& operator<<(ostream& o,cnum c);
		friend istream& operator>>(istream& o,cnum& c);
};

cnum cnum::operator*(const cnum& o)const
{
	double one=real*o.real-imag*o.imag;
	double two=real*o.imag+imag*o.real;
	return cnum(one,two);
}

ostream& operator<<(ostream& o,cnum c)
{
	o<<"("<<c.real<<","<<c.imag<<"i)";
	return o;
}

istream& operator>>(istream& o,cnum& c)
{
	cout<<"real:";
	o>>c.real;
	if (o)
		cout<<"imaginary:";
	o>>c.imag;
	return o;
}

int main()
{
	cnum a(3.0, 4.0); // initialize to (3,4i)
	cnum c;
	cout << "Enter a complex number (q to quit):\n";
	while (cin >> c)
	{
		cout << "c is " << c << '\n';
		cout << "complex conjugate is " << ~c << '\n';
		cout << "a is " << a << '\n';
		cout << "a + c is " << a + c << '\n';
		cout << "a - c is " << a - c << '\n';
		cout << "a * c is " << a * c << '\n';
		cout << "2 * c is " << 2 * c << '\n';
		cout << "Enter a complex number (q to quit):\n";
	}
	cout << "Done!\n";
	return 0;
}



