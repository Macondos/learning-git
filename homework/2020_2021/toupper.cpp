#include<iostream>
#include<string>
using namespace std;

void toupper(string&);

int main()
{
    string input;
    getline(cin,input);
    toupper(input);
}

void toupper(string& str)
{
    //A:65;a:97;
    for (int i=0;i<str.size();i++)
        if (str[i]>=97&&str[i]<=122)
            cout<<char(str[i]-32);
        else cout<<str[i];
}
