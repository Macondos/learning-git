#include <iostream>
using namespace std;

int main()
{
    int count = 0;
    int remember = 0;
    int time = 0;
    int cur;
    cin >> cur;
    while (cur)
    {
        if (cur > remember)
            time += (cur - remember) * 6 + 5;
        else
            time += -(cur - remember) * 4 + 5;
        remember = cur;
        cin>>cur;
    }
    cout << time;
}