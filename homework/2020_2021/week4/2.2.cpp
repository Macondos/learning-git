#include <iostream>
#include <stack>
using namespace std;
int main()
{
    stack<char> q,qt;
    char ch;
    bool flag = false;

    while (cin.peek() != '\n')
    {
        ch = cin.get();
        if (ch != 32||flag)
        {
            q.push(ch);
            flag = true;
        }
        else
            continue;
    }
    flag=false;
    while(!q.empty())
    {
        ch = q.top();
        q.pop(); 
        if (ch != 32||flag)
        {
            qt.push(ch);
            flag = true;
        }
        else
            continue;
    }
    while (!qt.empty())
    {
        cout << qt.top();
        qt.pop();
    }
}