#include <iostream>
#include <deque>
#include <iterator>
using namespace std;

int main()
{
    deque<char> s;
    char ch;
    int count = 0;

    while (cin.peek() != '\n')
    {
        ch = cin.get();
        s.push_back(ch);
    }
    deque<char>::iterator it = s.end();

    while (it != s.begin())
    {
        it--;
        (++count) %= 3;
        if (count == 0&&it != s.begin())
            it = s.insert(it, ',');
    }

    for (auto &x : s)
        cout << x;
}