#include <iostream>
#include <stack>
#include <cmath>
using namespace std;

int main()
{
    int s, t;
    cin >> s >> t;
    int a, b;
    a = (s + t) / 2;
    b = (s - t) / 2;
    stack<int> s1, s2;
    int temp;
    int count1 = 0;
    int count2 = 0;
    // invert a
    while (a)
    {
        temp = a % 10;
        s1.push(temp);
        count1++;
        a = a / 10;
    }
    //invert b
    while (b)
    {
        temp = b % 10;
        s2.push(temp);
        count2++;
        b = b / 10;
    }
    int A = 0;
    int B = 0;
    int count11=0;
    int count22=0;
    while (count11!=count1)
    {
        A += s1.top() * pow(10, count11++);
        s1.pop();
    }
    while (count22!=count2)
    {
        B += s2.top() * pow(10, count22++);
        s2.pop();
    }
    cout << A + B << " " << A - B;
}