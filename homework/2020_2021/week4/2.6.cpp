#include <iostream>
#include <list>
using namespace std;
struct hi
{
    int exp;
    double coeff;
};

int main()
{
    list<hi> ls;
    int num;
    cin >> num;
    hi temp;
    for (int i = 0; i < num; i++)
    {
        cin >> temp.exp >> temp.coeff;
        ls.push_back(temp);
    }
    // another list
    int tnum;
    cin >> tnum;
    list<hi>::iterator it = ls.begin();
    for (int i = 0; i < tnum; i++)
    {
        cin >> temp.exp >> temp.coeff;
        while ((*it).exp > temp.exp && it != ls.end())
            it++;
        if ((*it).exp == temp.exp)
            (*it).coeff += temp.coeff;
        else
        {
            ls.insert(it, temp);
            num++;
        }
    }
    cout << num;
    for (auto &x : ls)
        cout << " " << x.exp << " " << x.coeff;
}
