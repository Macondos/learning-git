#include <iostream>
using namespace std;

int main()
{
    int num;
    cin >> num;
    int former = num - 1;
    int latter;
    int max = 0;
    int sum = 0;
    int arr[10000];
    bool flag = false;
    for (int i = 0; i < num; i++)
    {
        cin >> arr[i];
        if (arr[i] >= 0)
            flag = true;
    }
    for (int i = 0; i < num; i++)
    {
        sum += arr[i];
        if (sum < 0)
            sum = 0;
        if (sum > max)
        {
            max = sum;
            latter = i;
        }
    }
    int temp = 0;
    for (int i = latter; i >= 0; i--)
    {
        temp += arr[i];
        if (temp == max && i < former)
            former = i;
    }
    if (flag)
    cout << max << " " << former << " " << latter;
    else cout<<"0 "<<arr[0]<<" "<<arr[num-1];
}