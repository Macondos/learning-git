#include <iostream>
#include <cstring>
using namespace std;

int main()
{
    int i = 0, j = 0;
    char mother[100];
    char child[100];
    int next[100];
    cin >> mother >> child;
    int lm=strlen(mother);
    int lc=strlen(child);
    //设置回溯数组
    int k = -1;
    next[0] = -1;
    while (j < lc)
    {
        if (k == -1 || child[j] == child[k])
        {
            j++;
            k++;
            next[j] = k;
        }
        else
            k = next[k];
    }
    //kmp
    j = 0;
    
    while (i < lm && j < lc)
    {
        if (j == -1 || mother[i] == child[j])
        {
            i++;
            j++;
        }
        else
            j = next[j];
    }

    if (j == lc)
        cout << i - j;
    else
        cout << -1;
}