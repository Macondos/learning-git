#include<iostream>
using namespace std;
int len;
void mfill(int(*)[100],int n=0,int start=1);

int main()
{
    cin>>len;
    int matrix[100][100];
    mfill(matrix);
    for (int i=0;i<len;i++)
    {
        for (int j=0;j<len;j++)
            cout<<matrix[i][j]<<" ";
        cout<<endl;
    }
}

void mfill(int(*matrix)[100],int n,int start)
{
    if (n>=len/2.0)
        return;
    else
    {
        int new_lenth=len-2*n;
        int l_start,u_start;
        l_start=u_start=start+3*(new_lenth-1);
        int d_start=start+2*(new_lenth-1);
        for (int i=n;i<new_lenth+n;i++)
        {
            matrix[i][len-n-1]=start++;
            matrix[i][n]=l_start--;
            matrix[len-n-1][i]=d_start--;
            if (i==new_lenth+n-1)
                break;
            matrix[n][i]=u_start++;
        }
        return mfill(matrix,n+1,u_start);
    }
}
