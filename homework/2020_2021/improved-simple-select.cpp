#include<iostream>
#include<iomanip>
#include<cstdlib>
using namespace std;

void simple();
void print();

int a[100];


int main()
{
    for (int i=0;i<100;i++)
        a[i]=rand()%10000;
    print();
    simple();
    cout<<"-------------------------------------------------------------------"<<endl;
    print();
}

void simple()
{
    for (int i=0;i<99;i++)
    {
        int max=99-i;
        int min=i;
        for (int j=i+1;j<100-i;j++)
        {
            if (a[j]>a[max])
                max=j;
            if (a[j]<a[min])
                min=j;
        }
        int temp=a[i];
        a[i]=a[min];
        a[min]=temp;
        temp=a[99-i];
        a[99-i]=a[max];
        a[max]=temp;
    }
}

void print()
{
    for (int i=0;i<100;i++)
    {
        if (i%10==0&&i!=0)
            cout<<endl;
        cout<<setw(5)<<a[i];
    }
    cout<<endl;
}
