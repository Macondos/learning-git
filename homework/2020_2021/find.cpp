#include <iostream>
#include <string>
using namespace std;

int tag[7] = {0}; //这个变量应该是符合要求的记录位置把
int len = 0;
int former, latter, flag, classs;
int max_position, min_position, max_class, min_class;
double summary, max_sum, min_sum;
string ch;
string **pt;
string inf[7][6] = {"10001", "11", "Zhang", "99.5", "88.5", "89.5",
                    "10002", "12", "Yang", "77.9", "56.5", "87.5",
                    "10003", "11", "Liang", "92.5", "99.0", "60.5",
                    "10004", "11", "Cai", "89.6", "56.9", "90.5",
                    "10005", "14", "Fu", "55.6", "67.9", "98.9",
                    "10006", "12", "Mao", "22.1", "45.9", "99.2",
                    "10007", "13", "Zhan", "35.6", "67.9", "88.0"};

void input();
string **find();
void rearrange();
void reset(int, bool, double *);
void exchange(int, int);
void print();

int main()
{
    input();
    pt = find();
    rearrange();
    print();
    delete[] pt;
    return 0;
}

void input()
{
    cin >> flag;
    int i = 0;
    switch (flag)
    {
    case 1:
        i = 1;
    case 2:
        cin >> former;
        cin.clear();
        cin.get();
        cin >> latter;
        for (int j = 0; j < 7; j++)
            if (stoi(inf[j][i]) >= former && stoi(inf[j][i]) <= latter)
                tag[j] = 1;
        break;
    case 5:
        cin >> classs;
        cin.clear();
        cin.get();
        cin >> former;
        cin.clear();
        cin.get();
        cin >> latter;
        for (int j = 0; j < 7; j++)
            if (stoi(inf[j][i]) >= former && stoi(inf[j][i]) <= latter && stoi(inf[j][1]) == classs)
                tag[j] = 1;
        break;
    case 3:
        cin >> ch;
        for (int j = 0; j < 7; j++)
            if (!ch.compare(0, 1, inf[j][2]) || !ch.compare(0, 2, inf[j][2]))
                tag[j] = 1;
        break;
    case 4:
        cin >> summary;

        double sum[7] = {0};
        for (int k = 0; k < 7; k++)
            for (int j = 3; j < 6; j++)
                sum[k] += stod(pt[k][j]);
        //get sum cmp next;
        for (int j = 0; j < 7; j++)
            if (sum[j] >= summary)
                tag[j] = 1;
        break;
    }
}

string **find()
{
    for (int i = 0; i < 7; i++)
        if (tag[i])
            len++;
    string **pt = new string *[len];
    int count = 0;

    for (int i = 0; i < 7; i++)
    {
        if (tag[i])
        {
            pt[count] = inf[i];
            count++;
        }
    }
    return pt; //point pt to the needed inf;
}

void rearrange()
{
    double sum[len] = {0};
    for (int i = 0; i < len; i++)
        for (int j = 3; j < 6; j++)
            sum[i] += stod(pt[i][j]);
    //get the summary of the new string;

    for (int i = 0; i < len - i; i++)
    {
        reset(i, false, sum);
        reset(len - i - 1, 1, sum);
        for (int j = i + 1; j < len - i; j++)
        {
            if (stoi(pt[j][1]) > max_class)
                reset(j, 1, sum);
            else if (stoi(pt[j][1]) == max_class && sum[j] < max_sum)
                reset(j, 1, sum);
            if (stoi(pt[j][1]) < min_class)
                reset(j, 0, sum);
            else if (stoi(pt[j][1]) == min_class && sum[j] > min_sum)
                reset(j, 0, sum);
        }
        exchange(i, min_position);
        exchange(len - i - 1, max_position);
    }
    //find the max and the min at the same time;using select-order
}

void reset(int i, bool j, double *sum)
{
    if (j)
    {
        max_position = i;
        max_class = stoi(pt[i][1]);
        max_sum = sum[i];
    }
    else
    {
        min_position = i;
        min_sum = sum[i];
        min_class = stoi(pt[i][1]);
    }
}

void exchange(int i, int j)
{
    for (int k = 0; k < 6; k++)
    {
        string temp = pt[i][k];
        pt[i][k] = pt[j][k];
        pt[j][k] = temp;
    }
}

void print()
{
    for (int i = 0; i < len; i++)
    {
        for (int j = 0; j < 6; j++)
            cout << pt[i][j] << " ";
        cout << endl;
    }
}
