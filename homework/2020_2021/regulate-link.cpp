#include<string>
#include<iostream>
#include<iomanip>
using namespace std;
typedef struct stu t;
typedef struct stu* p;
void input(p&);
void cdelete(p&);
void cselect(p&);
void arrange(p&);
void output(p&);
void output_column();
void delete_pointers(p);
void if_next(p&,p,p);
struct stu
{
    int num;
    int clss;
    string name;
    double grades[3];
    double sum;
    struct stu* next;
};

int main()
{
    int choice;
    p head=NULL;
    int n=0;
    while (1)
    {
        output_column();
        cin>>choice;
        if (choice==6)
            break;
        switch (choice)
        {
            case 1:
                input(head);
                break;
            case 2:
                cdelete(head);
                break;
            case 3:
                cselect(head);
                break;
            case 4:
                arrange(head);
                break;
            case 5:
                output(head);
                break;
        }
        n++;
    }
    if (head!=NULL)
        delete_pointers(head);
}



void output_column()
{
    cout<<"1.input"<<endl<<"2.delete"<<endl<<"3.select"<<endl<<"4.order"<<endl<<"5.output"<<endl<<"6.quit"
        <<endl<<"please input your option"<<endl;
}

void input(p& head)
{
    p p1,p2;
    string flag;
    int n=0;
    do
    {
        p student=new t;
        cout<<"Id ";
        cin>>student->num;
        cout<<"class ";
        cin>>student->clss;
        cout<<"name ";
        cin>>student->name;
        for (int i=0;i<3;i++)
        {
            cout<<"score"<<i+1<<" ";
            cin>>student->grades[i];
        }
        student->sum=0;
        for (int i=0;i<3;i++)
            student->sum+=student->grades[i];
        if (n)
            p2=p1;
        p1=student;
        if (!n)
           p2=head=p1;
        else p2->next=p1;
        n++;
        cout<<"continue?"<<endl;
        cin>>flag;
    }while (!flag.compare("yes"));
    p1->next=NULL;
}
        
void cdelete(p& head)
{
    string flag;
    int n=0;
    do
    {
        p p1,p2;
        p1=p2=head;
        string choice;
        cin>>choice;
        while (p1!=NULL)
        {
            if ((to_string(p1->num)==choice||p1->name==choice)&&n)
            {
                p2->next=p1->next;
                delete p1;
                break;
            }
            else if ((to_string(p1->num)==choice||p1->name==choice)&&!n)
            {
                head=p1->next;
                delete p1;
                break;
            }
            p2=p1;
            p1=p1->next;
            n++;
        }
        output(head);
        cout<<"continue?"<<endl;
        cin>>flag;
    }while (!flag.compare("yes"));
}

void cselect(p& head)
{
        int test_if=0;
        int choice;
        string flag;
        do
        {
            p p1,p2;
            p1=p2=head;
            cin>>choice;
            while (p1!=NULL)
            {
                if (p1->num==choice||p1->clss==choice)
                {
                    cout<<p1->num<<" "<<p1->clss<<" "<<p1->name<<" ";
                    for (int i=0;i<3;i++)
                        cout<<p1->grades[i]<<" ";
                    cout<<p1->sum<<endl;
                    test_if=1;
                }
                p2=p1;
                p1=p1->next;
            }
            if (p1==NULL&&!test_if)
            {
                cout<<"there is no eligible student"<<endl;
                output(head);
            }
            cout<<"continue?"<<endl;
            cin>>flag;
        }while (!flag.compare("yes"));
}

void arrange(p& head)
{
    p p1,p2,p0,pf0,mark;
    p2=mark=p0=pf0=head;
    p1=p0->next;
    int flag=0;
    
    while (p1!=NULL)
    {
        while (p1!=NULL)
        {
            if (p1->clss<mark->clss||(p1->clss==mark->clss&&p1->sum>mark->sum))
                mark=p1;
            p2=p1;
            p1=p1->next;
        }// find the one that should bo the first;
        p1=p0;
        p2=pf0;
        while (p1!=mark)
        {
            p2=p1;
            p1=p1->next;
        }//set p1 point to mark;
        if (!flag&&p0!=mark)
        {
            if_next(head,p1,p0);
            head=p1;
            p temp=p1->next;
            p1->next=p0->next;
            p2->next=p0;
            p0->next=temp;
        }
        else if (p0!=mark)
        {
            if_next(pf0->next,p1,p0);
            pf0->next=p1;
            p temp=p1->next;
            p1->next=p0->next;
            p2->next=p0;
            p0->next=temp;
        }
        //above:change the chain;
        pf0=p0;
        mark=p2=p0=p0->next;
        p1=p0->next;//go to next link;
        flag++;
    }
    output(head);
}

void output(p& head)
{
    p p1,p2;
    p1=p2=head;
    while (p1!=NULL)
    {
        cout<<p1->num<<" "<<p1->clss<<" "<<p1->name<<" ";
        cout<<fixed<<setprecision(1);
        for (int i=0;i<3;i++)
            cout<<p1->grades[i]<<" ";
        cout<<p1->sum<<endl;
        p2=p1;
        p1=p1->next;
    }
}

void delete_pointers(p head)
{
    p p1,p2;
    p2=head;
    p1=head->next;
    while (p1!=NULL)
    {
        delete p2;
        p2=p1;
        p1=p1->next;
    }
    delete p2;
}

void if_next(p& point,p p1,p p0)
{
    if (p1=p0->next)
    {
        point=p1;
        p temp=p1->next;
        p1->next=p0;
        p0->next=temp;
    }
}
