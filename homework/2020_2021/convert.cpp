#include<iostream>
using namespace std;

void convert(int);

int main()
{
    int num;
    cin>>num;
    convert(num);
}

void convert(int num)
{
    static bool flag=false;
    if (!flag)
    {
        while (!(num%10))
            num/=10;
        flag=true;
    }
    cout<<num%10;
    num/=10;
    if (num)
        return convert(num);
    else return;
}

