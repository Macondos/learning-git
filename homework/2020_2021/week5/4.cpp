#include <iostream>
#include <numeric>
using namespace std;

int find_gcd(int a, int b)
{
    if (b == 0)
        return a;
    return find_gcd(b, a % b);
} // 开始的时候要求传入的a比b大

int find_lcm(int a, int b)
{
    int temp = find_gcd(a, b);
    return a * b / temp;
}
int main()
{
    int num;
    cin >> num;
    int numerator[101] = {0};
    int denominator[101] = {0};
    for (int i = 0; i < num; i++)
    {
        cin >> numerator[i];
        cin.get();
        cin.clear();
        cin >> denominator[i];
    }
    //找到分母的最小公倍数
    int lcm = denominator[0];
    for (int i = 1; i < num; i++)
        lcm = find_lcm(lcm, denominator[i]);
    for (int i = 0; i < num; i++)
        numerator[i] *= lcm / denominator[i];
    int sum = accumulate(numerator, numerator + num, 0);
    //调整输出格式，转化为真分数
    int integer = sum / lcm;
    sum -= integer * lcm;
    //约分一下
    int gcd = find_gcd(sum, lcm);
    sum /= gcd;
    lcm /= gcd;
    if (sum == 0)
        cout << integer;
    else if (integer)
        cout << integer << " " << sum << "/" << lcm;
    else
        cout << sum << "/" << lcm;

    return 0;
}