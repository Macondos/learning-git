#include <iostream>
#include <cstring>
using namespace std;

int main()
{
    char arr[100];
    cin >> arr;
    int num = strlen(arr);
    int n1 = (num + 2) / 3;
    int space = num - 2 * n1;
    for (int i = 0; i < n1; i++)
    {
        if (i != n1 - 1)
        {
            cout << arr[i];
            for (int j = 0; j < space; j++)
                cout << " ";
            cout << arr[num - 1 - i];
            cout << endl;
        }
        else
            for (int j = i; j < num - n1 + 1; j++)
                cout << arr[j];
    }
}