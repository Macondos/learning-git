#include<iostream>
#include<queue>
#include<set>
using namespace std;
struct ship
{
    int time;
    int people;
    int * nation;
};

int main()
{
    int nation_arr[100001]={0};
    int number=0;
    int front_time,back_time;
    front_time=back_time=0;
    queue<ship> qship;
    
    cin>>number;
    int count=number;
    int current_nation=0;
    
    while(count!=0)
    {
        //get in the data
        ship temp;
        cin>>temp.time;
        cin>>temp.people;
        temp.nation=new int[temp.people];
        for (int i=0;i<temp.people;i++)
            cin>>temp.nation[i];
        //push into the queue;
        qship.push(temp);
        //renew the nation_arr
        for(int i=0;i<temp.people;i++)
        {
            if (nation_arr[temp.nation[i]]==0)
                current_nation++;
            nation_arr[temp.nation[i]]++;
        }
        //renew the time;
        back_time=temp.time;
        if(count==number)
            front_time=temp.time;
        //check if need to pop
        while ((back_time-front_time)>=86400)
        {
            //renew the nation_arr
            ship tship=qship.front();
            for(int i=0;i<tship.people;i++)
            {
                nation_arr[tship.nation[i]]--;
                if (nation_arr[tship.nation[i]]==0)
                    current_nation--;
            }
            //move the front
            delete []tship.nation;
            qship.pop();
            //renew the front time
            front_time=qship.front().time;
        }
        // print the data
        cout<<current_nation<<endl;
        count--;
    }
}