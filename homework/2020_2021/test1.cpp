#include<iostream>
using namespace std;

class one 
{
		int hi;
	public:
		one(int o=0):hi(o){}
		virtual void read(){cout<<hi<<"call one"<<endl;};
};

class two:public one
{
		int hi;
	public:
		two(int o=0,int h=1):one(o),hi(h){}
		void read(){cout<<hi<<"call two"<<endl;}
};

int main()
{
	one* pt;
	two apple(1,2);
	pt=&apple;
	(*pt).read();
}
