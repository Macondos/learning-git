#include <iostream>
#include <deque>
using namespace std;

int main()
{
    int n;
    cin >> n;
    deque<int> q;
    q.push_front(1);
    cout << "1" << endl;
    int temp = 0;
    for (int i = 2; i <= n; i++)
    {
        q.push_front(0);

        for (int j = 1; j <= i; j++)
        {
            int t = q.back();
            q.pop_back();
            temp += t;
            cout << temp << " ";
            q.push_front(temp);
            temp = t;
        }
        cout << endl;
    }
}