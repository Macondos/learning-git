#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;

struct knot
{
    char name;
    knot *left;
    knot *right;
};
knot *build(char *, char *);
void lsearch(knot *root);
char fsearch[28];
char msearch[28];
int cur = 0;
int main()
{
    int num;

    cin >> msearch >> fsearch;
    num = strlen(msearch);
    knot *root = build(msearch, msearch + num - 1);
    lsearch(root);
    return 0;
}
//cur标记对于先序序列访问到了第几个元素
//[former,latter]是搜索的范围
knot *build(char *former, char *latter)
{
    knot *root = new knot;
    char temp = fsearch[cur++];
    char *position = find(former, latter + 1, temp);
    root->name = temp;
    if (former == latter)
    {
        root->right = nullptr;
        root->left = nullptr;
        return root;
    }
    if (position - 1 >= former)
        root->left = build(former, position - 1);
    else
        root->left = nullptr;
    if (position + 1 <= latter)
        root->right = build(position + 1, latter);
    else
        root->right = nullptr;
    return root;
}
//后序遍历
void lsearch(knot *root)
{
    if (root->left != nullptr)
        lsearch(root->left);
    if (root->right != nullptr)
        lsearch(root->right);
    cout << root->name;
    return;
}
