#include<iostream>
#include<cmath>
const int len=100;
using namespace std;
    
int main()
{
    bool number[len+1]={false};
    int prime[len+1];
    int count=0;
    int k;

    for (int i=2;i<=sqrt(len);i++)
    {
        if (!number[i])
        {
            prime[count++]=i;
            for (int j=i*i;j<=len;j+=i)
            {
                for (k=0;k<count-1;k++)
                {
                    if (!(number[j]%prime[k]))
                        break;
                }
                if (k!=count&&k)
                    continue;
                number[j]=true;
            }
        }
    }

    for (int i=2;i<=len;i++)
        if (!number[i])
            cout<<i<<" ";
}


