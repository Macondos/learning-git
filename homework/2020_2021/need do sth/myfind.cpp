#include <iostream>
#include <iomanip>
using namespace std;

//struct Table define student's information
struct Table
{
    int student_id;
    int class_id;
    string name;
    float achievement1;        //achievement of course 1
    float achievement2;        //achievement of course 1
    float achievement3;        //achievement of course 1
    float achievement_sum = 0; //the sum of all achievements
    //define the = > < operator between struct Table;
    bool operator>=(const Table &t2)
    {
        if (class_id > t2.class_id) //如果student班级号大于t2的班级号，则student大于t2,排序的时候在后面
        {
            return class_id > t2.class_id;
        }
        else if (class_id == t2.class_id) ////如果同一个班级，则比较总分，总分大的，认为是小的student，排序可以排在前面
        {
            return achievement_sum <= t2.achievement_sum;
        }
        else
        {
            return false;
        }
    }
    bool operator<=(const Table &t2)
    {
        if (class_id < t2.class_id) //参考上面的注释
        {
            return class_id < t2.class_id;
        }
        else if (class_id == t2.class_id) //参考上面的注释
        {
            return achievement_sum >= t2.achievement_sum;
        }
        else
        {
            return false;
        }
    }
    bool operator==(const Table &t2)
    {
        return class_id == t2.class_id && achievement_sum == t2.achievement_sum;
    }
    friend ostream &operator<<(ostream &cout, const Table &t)
    {
        cout << setw(10) << t.student_id;
        cout << setw(10) << t.class_id;
        cout << setw(10) << t.name;
        cout << setw(10) << t.achievement1;
        cout << setw(10) << t.achievement2;
        cout << setw(10) << t.achievement3;
        cout << setw(10) << t.achievement_sum;
        cout << endl;
        return cout;
    }
};

const int lines = 7; //define table rows
Table students[] = {
    {10001, 11, "Zhang", 99.5, 88.5, 89.5},
    {10002, 12, "Yang", 77.9, 56.5, 87.5},
    {10003, 11, "Liang", 92.5, 99.0, 60.5},
    {10004, 11, "Cai", 89.6, 56.9, 90.5},
    {10005, 14, "Fu", 55.6, 67.9, 98.9},
    {10006, 12, "Mao", 22.1, 45.9, 99.2},
    {10007, 13, "Zhan", 35.6, 67.9, 88.0}};
int tags[lines] = {0}; //the value of the tags[i] is 1 if students[i] is find and selected//
//create index on student id and achievement_sum field
struct IndexItem
{
    float value;
    int index;
    bool operator>=(const IndexItem &item)
    {
        return value >= item.value;
    }

    bool operator<=(const IndexItem &item)
    {
        return value <= item.value;
    }
};
IndexItem stid[lines];    //index on student id
IndexItem achisum[lines]; //index on achievement_sum
/*save formmer digit and latter digit
the minimal and maxmal digit you wanna search
*/
struct former_latter
{
    float former;
    float latter;
    int class_id;
};

/*save search arg,if search by name,U should not input former and latter
union类型内部变量不支持string，所以用C字符串
@name:student name
*/
union Arg
{
    char name[20];
    former_latter fm;
} arg;

//some functions here
template <typename T>
void indexMainData(T data[], int left, int right); //index data on class(ascent) and achievement_sum(desc)
void inputSearchKeyword();                         //
void findStudent(int index, Arg arg, int begin, int end);
void printResull(); //show search result
int binarysearch(IndexItem data[],int key, int begin, int end, bool direct);
void InitDataBase();

int main()
{ //init and sum achievements
    for (int i = 0; i < lines; i++)
        students[i].achievement_sum += (students[i].achievement1 + students[i].achievement2 + students[i].achievement3);

    InitDataBase();
    inputSearchKeyword();
    return 0;
}

template <typename T>
void indexMainData(T data[], int left, int right)
{
    if (left >= right)
    {
        return;
    }

    int i = left;
    int j = right;
    T base = data[left];
    while (i < j)
    {
        while (data[j] >= base && i < j)
        {
            j--;
        }

        while (data[i] <= base && i < j)
        {
            i++;
        }

        if (i < j)
        {
            T tmp = data[i];
            data[i] = data[j];
            data[j] = tmp;
        }
    }

    data[left] = data[i];
    data[i] = base;

    indexMainData(data, left, i - 1);
    indexMainData(data, i + 1, right);
}

void findStudent(int index, Arg arg, int begin, int end)
{
    //this search use binary search method
    if (index == 1) //if search by name
    {
        //because the search result is not only one,so I do not use binary search here
        for (int i = 0; i < lines; i++)
        {
            if (students[i].name.find(arg.name) != -1) //find方法找到就返回位置，找不到就返回-1
                tags[i] = 1;
        }
    }
    else
    {
        switch (index) //根据字段的索引决定Table的哪一个变量是查找的对象
        {
        case 0:
        {
            //根据学号查找，因为无序，所以从头到尾对比查找
            int b=0;
            int e=lines-1;
            int begin=binarysearch(stid,arg.fm.former,b,e,true);
            b=0;
            e=lines-1;
            int end=binarysearch(stid,arg.fm.latter,b,e,false);
            for (int i = begin; i <= end; i++)
            {
                int idx=stid[i].index;
                if(students[idx].class_id==arg.fm.class_id)
                    tags[idx]=1;
            }
            break;
        }
        case 2:
        {
            //根据成绩查找
            int b=0;
            int e=lines-1;
            int begin=binarysearch(achisum,arg.fm.former,b,e,true);
            b=0;
            e=lines-1;
            int end=binarysearch(achisum,arg.fm.latter,b,e,false);
            for (int i = begin; i <=end; i++)
            {
                tags[achisum[i].index] = 1;
            }
        }
        }
    }
}

void printResull()
{
    cout << endl;
    for (int i = 0; i < lines; i++)
    {
        if (tags[i] == 1)
        {
            //下面的代码可是试着模仿重载<<运算符，写在struct Table里面，这样子就不必以后每次显示都写下面的一大堆东西了，自己百度
            cout << students[i];
        }
    }
}
/*
@key待检索的值,函数可以查找确切的数值，对于有重复数据的，找到其最小的下边位置
也可以查找区间值，比如查找比key大最小值200，如果只找到相邻的两个书数是191,205则函数返回205
类似的查找比key小的最大值。
*/
int binarysearch(IndexItem data[],int key, int begin, int end, bool direct)
{
    int mid = (begin + end) / 2;
    if (data[mid].value > key)
    {
        end = mid-1;
    };
    if (data[mid].value < key)
    {
        begin = mid + 1;
    };
    if (data[mid].value == key)
    {
        if (direct == true)
        {
            while (data[mid - 1].value == key)
                mid--;
        }
        else
        {
            while (data[mid + 1].value == key)
                mid++;
        }
        return mid;
    };
    if(direct==true)
    {
        //查找比key大的最小值，适合查找成绩
        if(data[0].value>key)
            return 0;
        if(data[mid].value>key && data[mid-1].value<key)
            return mid;
        if(data[mid].value<key && data[mid+1].value>key)
            return mid+1;
    }
    else
    {
        //查找比key小的最大值，适合查找成绩
        if(data[lines-1].value<key)
            return lines-1;
        if(data[mid].value>key && data[mid-1].value<key)
            return mid-1;
        if(data[mid].value<key && data[mid+1].value>key)
            return mid;
    }
    
    
    
    if (begin == end && data[begin].value == key)
    {
        if (direct == true)
        {
            while (data[begin - 1].value == key)
                begin--;
        }
        else
        {
            while (data[begin + 1].value == key)
                begin++;
        }
        return begin;
    }
    else if (begin >= end)
        return -1;
    else if (begin < end)
        return binarysearch(data,key, begin, end, direct);
}

void InitDataBase()
{
    //先排序主表，然后根据主表抽取学号和总成绩的索引
    indexMainData(students, 0, lines - 1);
    for (int i = 0; i < lines; i++)
    {
        stid[i].index = i;
        stid[i].value = students[i].student_id;

        achisum[i].index = i;
        achisum[i].value = students[i].achievement_sum;
    }
    indexMainData(stid, 0, lines - 1);
    indexMainData(achisum, 0, lines - 1);
}

void inputSearchKeyword()
{
    int index;
    do
    {
        cout << "Please input field you search:\n"
             << "0.by student number and class number\n"
             << "1.by name\n"
             << "2.by sum of achievement\n"
             << "3.Quit!:\n";
        cin >> index;
        if (index >= 4 && index <= 0)
        {
            cout << "You input wrong selection!\n";
            continue;
        }
        switch (index)
        {
        case 1:
        {
            cout << "Input student\'s name:";
            cin >> arg.name;
            break;
        }
        case 0:
        {
            cout << "Input student id and class id\n"
                 << "e.g. 11.10001-10004:";
            string tmp;
            cin>>tmp;
            //下面解析查询关键词，提取班级以及学号起始值
            int line_position=tmp.find("-");
            string leftchar,rightchar;
            if(line_position!=-1){
                leftchar=tmp.substr(0,line_position);
                rightchar=tmp.substr(line_position+1);
            }
            else
            {
                leftchar=tmp;
                int dot_position=leftchar.find(".");
                rightchar=leftchar.substr(dot_position+1);
            }
            int dot_position;
            dot_position=leftchar.find(".");
            string class_id=leftchar.substr(0,dot_position);
            string former=leftchar.substr(dot_position+1);
            string latter=rightchar;
            arg.fm={0,0,0};
            arg.fm.class_id=stoi(class_id);
            arg.fm.former=stoi(former);
            arg.fm.latter=stoi(latter);
            break;
        }
        case 2:
        {
            arg.fm={0,0,0};
            cout << "Input the score of you wanna search:"
                 << "e.g. 200 300,blank between digits:";
            cin >> arg.fm.former;
            cin >> arg.fm.latter;
            break;
        }
        case 3:
        {
            cout<<"Bye!"<<endl;
            exit(0);
        }
        }
        findStudent(index, arg, 0, lines - 1);
        printResull();
        for (int i = 0; i < lines; i++)
            tags[i] = 0;
    } while (cin);
}
