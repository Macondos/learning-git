#include <iomanip>
#include <iostream>
using namespace std;
const int len = 3;
double a[len][len];
double b[len][len];

int main()
{
    for (int i = 0; i < len; i++)
        for (int j = 0; j < len; j++)
            cin >> a[i][j];

    for (int i = 0; i < len; i++)
        for (int j = 0; j < len; j++)
            if (i == j)
                b[i][j] = 1;
            else
                b[i][j] = 0;

    for (int j = 0; j < len; j++)
    {
        if (a[j][j] == 0)
        {
            int i;
            for (i = j + 1; i < len; i++)
                if (a[i][j] != 0)
                {
                    for (int k = j; k < len; k++)
                    {
                        double temp = a[i][k];
                        a[i][k] = a[j][k];
                        a[j][k] = temp;
                    } //change a not including 0 num;
                    for (int k=0;k<len;k++)
                    {
                        double temp = b[i][k];
                        b[i][k] = b[j][k];
                        b[j][k] = temp;
                    }
                    break;
                }
            if (len == i)
            {
                cout << "no inverse matirx" << endl;
                return 0;
            }
        } //test if have a inverse matrix before change;

        for (int i = j + 1; i < len; i++)
        {
            double plus = -a[i][j] / a[j][j];
            for (int k = j; k < len; k++)
                a[i][k] = a[i][k] + plus * a[j][k];
            for (int k = 0; k < len; k++)
                b[i][k] = b[i][k] + plus * b[j][k];
        }
    } //change to line-step-matrix;

    for (int i=0;i<len-1;i++)
        for (int j=i+1;j<len;j++) //原先是for (int j=i+1;j<len-1;j++),每一行最后一列没修改
            a[i][j]=a[i][j]/a[i][i];
    for (int i=0;i<len;i++)
         for (int j=0;j<len;j++)
             b[i][j]=b[i][j]/a[i][i];//change to standard line-step-matrix;

    

    for (int j=len-1;j>0;j--)
    {
        for (int i=j-1;i>=0;i--)
            for (int k=0;k<len;k++)
                 b[i][k]=b[i][k]-b[j][k]*a[i][j];
    }//change b according to a
    
    /*for(int i=len-1;i>=0;i--)
    {
        double tmp1=a[i][i];
        for(int j=0;j<len;j++)
        {
            a[i][j]=a[i][j]/tmp1;
            b[i][j]=b[i][j]/tmp1;
        }
        if(i==0)
            break;
        for(int j=i-1;j>=0;j--)
        {
            double tmp2=a[j][i];
            for(int k=0;k<len;k++)
            {
                b[j][k]=b[j][k]-b[i][k]*tmp2;
                a[j][k]=a[j][k]-a[i][k]*tmp2;
            }
        }
    }*/
    cout << fixed << setprecision(2);
    for (int i = 0; i < len; i++)
    {
        for (int j = 0; j < len; j++)
            cout << setw(6) << b[i][j];
        cout << endl;
    } //print b;
}