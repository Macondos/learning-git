#include <iostream>
#include <stack>
using namespace std;

int main()
{
    stack<char> s;
    while (cin.peek() != '\n')
    {
        char temp;
        cin >> temp;
        while (temp != '(' && temp != ')' && cin.peek() != '\n')
            cin >> temp;
        if (temp == '(')
            s.push(temp);
        else if (temp == ')')
        {
            if (!s.empty() && s.top() == '(')
                s.pop();
            else
                s.push(temp);
        }
    }
    if (s.empty())
        cout << "括号匹配！";
    else
        cout << "括号不匹配！";
}