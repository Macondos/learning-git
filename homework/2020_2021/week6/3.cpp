#include <iostream>
using namespace std;

class q
{
private:
    int arr[100];
    int front;
    int rear;

public:
    q() : front(0), rear(0){};
    bool in(int t)
    {
        if ((rear + 1) % 100 == front)
            return false;
        arr[rear] = t;
        rear = (rear + 1) % 100;
        return true;
    }
    bool out()
    {
        if (front == rear)
            return false;
        front = (front + 1) % 100;
        return true;
    }
    void print()
    {
        cout << arr[front];
        for (int i = (front + 1) % 100; i != rear; i = (++i) % 100)
            cout << " " << arr[i];
    }
};

int main()
{
    int temp;
    int num;
    q hi;
    cin>>num;
    for(int i=0;i<num;i++)
    {
        cin >> temp;
        hi.in(temp);
    }
    hi.out();
    hi.out();
    hi.in(11);
    hi.in(12);
    hi.print();
}