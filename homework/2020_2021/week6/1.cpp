#include <iostream>
#include <cstring>
using namespace std;

class link
{
private:
    char arr[100];
    int len;

public:
    link(){};
    bool insert(int p, char c)
    {
        if (p >= len)
            return false;
        for (int i = len; i > p; i--)
            arr[i] = arr[i - 1];
        arr[p] = c;
        len++;
        return true;
    }
    void insert(char c)
    {
        arr[len] = c;
        len++;
    }
    void del(char c)
    {
        for (int i = 0; i < len; i++)
            if (arr[i] == c)
            {
                for (int j = i; j < len; j++)
                    arr[j] = arr[j + 1];
                len--;
                i--;
            }
    }
    void print()
    {
        for (int i = 0; i < len; i++)
            cout << arr[i];
        cout << endl
             << len;
    }
};

int main()
{
    char arr[100];
    cin.getline(arr, 99);
    int num = strlen(arr);

    link hi;
    for (int i = 0; i < num; i++)
        hi.insert(arr[i]);
    hi.print();

    char temp;
    cin >> temp;
    hi.del(temp);
    hi.print();
}