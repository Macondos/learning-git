# 第四章笔记
简单的一维数组创建：int iar[20];创建了一个20个元素的数组，每一个数组的内容是int类型。类似可以创建 char car[12];double dar[5]等等，很简单。

还接着可以创建二维数组，如果一个数组看做一行数据，二维数组就是包含多行的一个表格，比如：int iar[3][4]创建一个3行4列的二维数组，每一行iar[i]就是一个一维数组。比如下面的iar[3][4]二维数组：

|下标| 0 | 1 | 2 | 3 |
| :-: | :-: | :-: | :-: | :-: | 
| 0 | 0 | 1 | 2 | 3 | 
| 1 | 4 | 5 | 6 | 7 | 
| 2 | 8 | 9 | 10| 11| 
```
iar[0]={0,1,2,3}
iar[1]={4,5,6,7}
iar[2]={8,9,10,11}
```
数组采用下标访问，行和列的下标从0开始，上面的数据中，iar[0][0]=0,iar[2][3]=11;
通常采用for循环语句访问数组的每一个元素
* 可以用sizeof运算符获取数组的字节数（这里的大小指数组的定义的字节数，不是实际元素的字节数），比如 int a[5]={2,3,1};sizeof(a)为20。
* sizeof运算符对于以参数形式传递过来的数组名无效，因为传递过来的时候已经丢失了定义信息，只能传递过来数组的头部地址。比如下面的代码，显示8.因为地址数据是8个字节：
```cpp
int getSize(int a[]){
	return sizeof(a);
}
int main() {
	int a[5]={3,3,1};
	cout<<getSize(a)<<endl;
	return 0;
}
```
有时候我们需要知道数组到底有多少个元素，怎么办呢？如果数组是满的，用sizeof(a)/sizeof(int)就可以，如果数组不满，就真的没办法了，程序必须时刻注意数组元素个数的变化。 

* cin>>不能整体读取缓冲区数据到数组，因此cin>>a是错误的。
## 特殊的数组，字符数组。
定义很简单char a[n]，但是这个数组有其特殊性。这个数组的有效元素个数只有n-1个，否则数组在显示的时候就会出错。最后一个元素会被cin设置为'\0'字符，cout在输出的时候，会遇到'\0'就停止显示数组的内容，否则会认为字符串没有结束，一致显示下去，直道遇到'\0'。

可以用字符串整体给字符数组赋值，会自动加入'\0'字符。

比如Lchar a[5]="abc";则a[0]='a',a[1]='b',a[2]='\0'.

带'\0'的字符数组通常被书上称为**C风格的字符串_**

可以用TBN来拼接字符串常量，比如下面的代码是正确的：
```cpp
cout<<"Hello"
	<<"Word!"<<endl;
char alongStr[]="This is a " "very long string "
				"provided by me";
cout<<alongStr<<endl;
```
二维字符串数组有些不一样的地方：比如下面的字符串：
```cpp
char carr[][19]={
	"line one",
	"line 2",
	"this is line three",
}
char carr2[]="this a tring";
//二维数组定义的时候，最后一列的值必须指定。大概是对于一维数组，每一个元素，处理的时候看做一行，对于二维数组，也是这么对待，只不过一行数据，由原先的一个字符变成了一行数据。
//这个要求延续到了二维数组的指针定义上。
```
[关于cin输入的总结,看这里](https://gitee.com/Macondos/learning-git/blob/master/Cin%E7%9A%84%E9%82%A3%E4%BA%9B%E4%BA%8B.txt),正常情况下按照要求输入数据不会出现莫名其妙的问题，主要就是上面分析的几种情况.

string类，用起来很方便的类，比c字符串方便好多有木有，大多数编程语言都内置了这个类，话说还有多少人执着的用C字符串或者数组？

C++可以用两个string类的头文件,cstring或者string,第一个从c语言延伸过来，不但包括了string，还包括对c风格字符串的处理函数，比如strcpy，strlen,strcmp,strncmp(比较前n个字符)等

关于结构体没啥难处，需要注意的一点，结构体内部的匿名结构体如果有对应的变量，则不可以直接把它内部的变量当做外部结构体的成员。

可能拗口。看例子更清楚：
```cpp
struct test
{
	struct
	{
		int a;
	};
	int b;
};

test c;
c.a=1;
c.b=2;
//以上用法正确
struct test
{
	struct
	{
		int a;
	} t;
	int b;
};

test c;
c.a=1; //错误，必须用c.t.a=1
c.b=2;

```
* 结构数组和普通数组没什么区别。

结构中的为字段，只是为了有意的限制一个类型变量占多少位，比如一个整形默认32位，就是4字节，我们可以定义这个整形变量只占4位即可，便于节省内存或者做位运算而已，这种限制目前知道只能在结构体中定义。

* 共用体：union,内部所有成员变量公用一个存储空间，这说明共用体的存储空间大小为数据类型最大的那个。比如
```cpp
union tets{
	char a;
	int b;
	long c;
}
```
这个共用体的存储空间大小就是sizeof(long),由于公用一个空间，那么在某一时刻，这么多成员变量只有一个有效，其他的无效。

* 枚举类型

枚举类型只能赋值，不能运算。但是由于枚举类型的每一个元素都有默认的整数值，因此可以默认转换为其他类型参与运算，比如把枚举类型当做整数，参与整数的运算。

枚举类型默认的值从0，开始，以后依次加一，但是可以修改起始值，或者某一个中间元素的值，比如enum test{a=1,b,c,d=6,e,f,g}.

枚举值可以重复，比如:enum test{a,b=0,c=0,d,e,f}

以下代码是合法的：
```cpp
enum test{a=-6,b=2,c=4,d=8};
test myenum;
myenum=test(6);
```
枚举变量的可取值范围这么定义：

上限值：找到大于类型定义最大值的、最小的2的幂，然后减一
下限值：找到定义的最小值，如果>=0，则取值下限就是0，否则就“找到小于类型定义最小值的、最大的2的幂，然后加一”

上面的例子中，定义的最大值是8，大于8的最小的2的幂值是16，16-1=15，上限值就是15，小于-6的最大的2的幂的值是$-2^{3}$=-8,-8+1=-7,因此下限是-7.

这个取值有啥意义暂时不知道。

## 指针
指针存在堆heap里面，指针不像局部变量，连续分配的指针，地址不一定连续。

指针是由new分配空间，delete删除空间，记住，是删除指针指向的空间，而不是指针本身，指针变量依然存在，还可以指向其他地址，或者重新再分配内存空间。

最好不要随意在被调用的函数内部创建指针，以免忘记删除指针引起不可预料的错误。

所有new创建的指针用过后都要delete。那些指向普通变量地址的指针由于不是new创建的，所以不必delete。

指针可以指向变量（包括对象），还可以指向函数代码所在的地址，甚至还可以指向另外一个指针。

指针处理比较困难的是指针和数组名之间的关系，下面的代码演示这些关系：

```cpp
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int int_ar[5]={1,2,3,4,5}; //创建一个整数数组
    int *pint=int_ar; //创建一个指向数组的指针，这个指针加1，步长就是一个int长度
    int (*pint_array)[5]=&int_ar; //创建一个指向五个元素的数组的指针，这个指针加1.步长就是一个int数组的长度
    cout<<"int_ar is:"<<int_ar<<endl;
    cout<<"pint is:"<<pint<<endl;
    cout<<"&int_ar is:"<<&int_ar<<endl;
    cout<<"(*pint_array)[5],pint_arrar is:"<<pint_array<<endl;

    int i1=1,i2=2,i3=3,i4=4,i5=5;
    int *ppa[5]={&i1,&i2,&i3,&i4,&i5}; //创建一个长度是5，每一个元素是int指针的数组
    int **ppb=ppa;  //创建指针的指针，ppb指向的地址为数组首地址，数组每一个元素是指针
    **ppb=5; //*(*ppb)=5;
    **(ppb+1)=4; // *(*(ppb+1))=4;

    for(int i=0;i<5;i++)
        cout<<*ppa[i]<<"\t";

    char *pchar[5]={  //创建一个有5个元素的数组，每隔元素是一个字符串指针
        "平顶山",
        "洛阳",
        "焦作",
        "南阳",
        "商丘"
    };
    cout<<pchar[3]<<endl;
    //下面创建的是二维数组的指针
    int intArray[4][3]={
        {0,1,2},
        {3,4,5},
        {6,7,8},
        {9,10,11}
    };
    cout<<endl;
    int (*p)[3]=intArray;
    for(int i=0;i<4;i++){
        for (int j=0;j<3;j++)
            cout<<setw(5)<<*(*(p+i)+j); //等效于cout<<setw(5)<<p[i][j];
        cout<<endl;
    }
    cout<<endl;
    return 0;
}
`#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int int_ar[5]={1,2,3,4,5}; //创建一个整数数组
    int *pint=int_ar; //创建一个指向数组的指针，这个指针加1，步长就是一个int长度
    int (*pint_array)[5]=&int_ar; //创建一个指向五个元素的数组的指针，这个指针加1.步长就是一个int数组的长度
    cout<<"int_ar is:"<<int_ar<<endl;
    cout<<"pint is:"<<pint<<endl;
    cout<<"&int_ar is:"<<&int_ar<<endl;
    cout<<"(*pint_array)[5],pint_arrar is:"<<pint_array<<endl;

    int i1=1,i2=2,i3=3,i4=4,i5=5;
    int *ppa[5]={&i1,&i2,&i3,&i4,&i5}; //创建一个长度是5，每一个元素是int指针的数组
    int **ppb=ppa;  //创建指针的指针，ppb指向的地址为数组首地址，数组每一个元素是指针
    **ppb=5; //*(*ppb)=5;
    **(ppb+1)=4; // *(*(ppb+1))=4;

    for(int i=0;i<5;i++)
        cout<<*ppa[i]<<"\t";

    char *pchar[5]={  //创建一个有5个元素的数组，每隔元素是一个字符串指针
        "平顶山",
        "洛阳",
        "焦作",
        "南阳",
        "商丘"
    };
    cout<<pchar[3]<<endl;
    //下面创建的是二维数组的指针
    int intArray[4][3]={
        {0,1,2},
        {3,4,5},
        {6,7,8},
        {9,10,11}
    };
    cout<<endl;
    int (*p)[3]=intArray;
    for(int i=0;i<4;i++){
        for (int j=0;j<3;j++)
            cout<<setw(5)<<*(*(p+i)+j); //等效于cout<<setw(5)<<p[i][j];
        cout<<endl;
    }
    cout<<endl;
    return 0;
}
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int int_ar[5]={1,2,3,4,5}; //创建一个整数数组
    int *pint=int_ar; //创建一个指向数组的指针，这个指针加1，步长就是一个int长度
    int (*pint_array)[5]=&int_ar; //创建一个指向五个元素的数组的指针，这个指针加1.步长就是一个int数组的长度
    cout<<"int_ar is:"<<int_ar<<endl;
    cout<<"pint is:"<<pint<<endl;
    cout<<"&int_ar is:"<<&int_ar<<endl;
    cout<<"(*pint_array)[5],pint_arrar is:"<<pint_array<<endl;

    int i1=1,i2=2,i3=3,i4=4,i5=5;
    int *ppa[5]={&i1,&i2,&i3,&i4,&i5}; //创建一个长度是5，每一个元素是int指针的数组
    int **ppb=ppa;  //创建指针的指针，ppb指向的地址为数组首地址，数组每一个元素是指针
    **ppb=5; //*(*ppb)=5;
    **(ppb+1)=4; // *(*(ppb+1))=4;

    for(int i=0;i<5;i++)
        cout<<*ppa[i]<<"\t";

    char *pchar[5]={  //创建一个有5个元素的数组，每隔元素是一个字符串指针
        "平顶山",
        "洛阳",
        "焦作",
        "南阳",
        "商丘"
    };
    cout<<pchar[3]<<endl;
    //下面创建的是二维数组的指针
    int intArray[4][3]={
        {0,1,2},
        {3,4,5},
        {6,7,8},
        {9,10,11}
    };
    cout<<endl;
    int (*p)[3]=intArray;
    for(int i=0;i<4;i++){
        for (int j=0;j<3;j++)
            cout<<setw(5)<<*(*(p+i)+j); //等效于cout<<setw(5)<<p[i][j];
        cout<<endl;
    }
    cout<<endl;
    return 0;
}
```
当字面常量字符串赋值给字符类型指针的时候，其实赋值的是字符串的首地址，这时候的地址并不是new产生的，所以不用删除，其实相当于编译器产生一个临时的字符数组常量，然后将数组地址赋值给这个指针，正因为如此，不可以利用指针修改字符串的内容。

虽然书上说C++不保证每一次使用字符串字面值在内存中都具有相同的地址，但是就实际验证来看，好像是一样的，下面的代码验证如下：
```cpp
char *p1="string1";
char *p2="string1";
char *p3="string1";
char *p4="string1";
cout<<(int *)p1<<endl;
cout<<(int *)p2<<endl;
cout<<(int *)p3<<endl;
cout<<(int *)p4<<endl;
//上面的四个输出显示相同的地址
```
注意，如果要输入一个字符串，赋值给一个字符串指针p，那么这个p必须提前用new分配空间，这是因为我们输入的数据是在系统缓冲区中，和代码内使用的字面常量字符串不一样，没有new的指针p指向不可预知的地方，容易引起程序错误。

地址相互赋值，并不会将一个指针指向的数据赋值给另一个指针指向的空间，只会改变指针的指向地址。这个规则同样适用于数组，因为数组变量其实就是数组的首地址。

为此，数组或者指向数组的指针的内容赋值，通常采用循环，一个个的赋值（用解除符号*）,对于字符串数组，通常采用函数strcpy或者strncpy。

## 自动存储、静态存储和动态存储