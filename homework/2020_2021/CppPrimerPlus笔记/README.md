# C++ Primer plus第六版书中源码

* 真的只是把书上的代码原封不动的再敲了一遍，放在这里方便看
* 好吧，上面是人家的话，辛辛苦苦输入代码，不好意思删除

内容在各章节目录

为了避免反复输入相同的文字，约定如下的简写或者简称

 1. TBN     表示空格、tab、或者换行符\n
 1. CD      表示文件结束符Ctrl+D

 
  ## 目录

* [第二章笔记](https://gitee.com/Macondos/learning-git/blob/master/CppPrimerPlus%E7%AC%94%E8%AE%B0/Chapter%2002/README.md)
* [第三章笔记](https://gitee.com/Macondos/learning-git/blob/master/CppPrimerPlus%E7%AC%94%E8%AE%B0/Chapter%2003/README.md)
* [第四章笔记](https://gitee.com/Macondos/learning-git/blob/master/CppPrimerPlus%E7%AC%94%E8%AE%B0/Chapter%2004/README.md)