#include <iostream>
#include <string>
#include <cstring>
using namespace std;
//classdeclare
class Cow {
	private:
		char name[20];
		char * hobby;
		int lh;
		double weight;
	public:
		Cow();
		Cow(const char * nm, const char * ho, double wt);
		Cow(const Cow & c);
		~Cow();
		Cow & operator=(const Cow & c);
		void ShowCow() const; // display all cow data
};
Cow::Cow()
{
	name[20]={0};
	lh=0;
	hobby=new char[lh+1];
	hobby[0]='\0';
	weight=0;
}
Cow::Cow(const char * nm, const char * ho, double wt)
{
	strcpy(name,nm);
	lh=strlen(ho);
	hobby=new char[lh+1];
	strcpy(hobby,ho);	
	weight=wt;
}
Cow::Cow(const Cow & c)
{
	strcpy(name, c.name );
	lh=c.lh;
	hobby=new char[lh+1];
	strcpy(hobby, c.hobby );
	weight=c.weight;
}
Cow & Cow::operator=(const Cow & c)
{
	if(this== &c)
		return *this;
	delete [] hobby;
	strcpy(name,c.name);
	lh=c.lh;
	hobby=new char [lh+1];
	strcpy(hobby,c.hobby);
	weight=c.weight;
}
Cow:: ~Cow()
{
	delete [] hobby;
}
void Cow::ShowCow() const
{
	cout<<name<<" "<<hobby<<" "<<weight<<endl;
}
//finished
int main()
{
	string c1n,c2n;
	string c1h,c2h;
	double m1,m2;
	cin>>c1n;
	cin.get();
	getline(cin,c1h);
	cin>>m1;

	cin>>c2n;
	cin.get();
	getline(cin,c2h);
	cin>>m2;
	char *pn1,*pn2,*ph1,*ph2;
	pn1=&c1n[0];
        ph1=&c1h[0];
	pn2=&c2n[0];
	ph2=&c2h[0];
	Cow c1=Cow(pn1,ph1,m1);
	Cow c2=Cow(pn2,ph2,m2);
	Cow c3=Cow(c1);
	Cow c4;
	c4=c2;
	c3.ShowCow();
	c4.ShowCow();
	return 0;
}
