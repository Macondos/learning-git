#include<iostream>
using namespace std;

int search(int,int*,int start=0,int end=9);

int main()
{
    int number[10];
    int num_to;
    for (int i=0;i<10;i++)
        cin>>number[i];
    cin>>num_to;
    cout<<search(num_to,number);
}

int search(int num_to,int*number,int start,int end)
{
    if (start<=end)
    {
        int mid=(start+end)/2;
        if (num_to>number[mid])
            return search(num_to,number,mid+1,end);
        else if (num_to<number[mid])
            return search(num_to,number,start,mid-1);
        else return mid;
    }
    else return -1;
}

