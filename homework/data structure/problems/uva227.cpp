#include <iostream>
using namespace std;

bool swap(char &a, char &b)
{
	if (b==0)
		return false;
	char t;
	t = a;
	a = b;
	b = t;
	return true;
}

int main()
{
	//creat arry
	char arr[7][7] = {0};
	//cin arry;it's a loop
	int l, c, times = 1;
	do
	{
		if (times != 1)
			cout << endl;
		//input the array //and mark the position of void
		for (int i = 1; i <= 5; i++)
		{
			for (int j = 1; j <= 5; j++)
			{	arr[i][j] = cin.get();
				if (arr[i][j] == ' ')
				{
					l = i;
					c = j;
				}
			}
			cin.get();
		}
		//now the kyboard buffer is clear
		//read in the order ; change the blocks ;revise the position
		char order;
		bool flag;
		do
		{
			while(cin.peek()=='\n')
				cin.get();
			order = cin.get();
			switch (order)
			{
			case 'A':
				flag=swap(arr[l][c], arr[l - 1][c]);
				l--;
				break;
			case 'R':
				flag=swap(arr[l][c], arr[l][c + 1]);
				c++;
				break;
			case 'L':
				flag=swap(arr[l][c], arr[l][c - 1]);
				c--;
				break;
			case 'B':
				flag=swap(arr[l][c], arr[l + 1][c]);
				l++;
				break;
			}
			if (!flag)
				break;
		} while (cin.peek() != '0');
		//discard what's left
		while (cin.get() != '\n')
			cin.get();
		//print title
		cout << "Puzzle #" << times << ":" << endl;
		//judge
		if (!flag)
			cout << "This puzzle has no final configuration." << endl;
		//print the configuration
		else
		{
			for (int i = 1; i <= 5; i++)
			{
				for (int j = 1; j <= 5; j++)
					if (j == 1)
						cout << arr[i][j];
					else
						cout << " " << arr[i][j];
				cout << endl;
			}
		}
		//times and new line
		times++;
	} while (cin.peek() != 'Z');
	return 0;
}