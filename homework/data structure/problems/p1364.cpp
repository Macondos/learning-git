#include <iostream>
using namespace std;

int knot[101] = {0};
int relation[101][101] = {0};
//size[i]表示以结点i为根的子树的大小
int size[101] = {0};
//f[i]表示以i为根树的带权路径长度
int f[101] = {0};
int num;

int t_size(int p)
{
    int count = knot[p];
    for (int i = 1; i <= num; i++)
        if (relation[p][i])
            count += t_size(i);
    return count;
}

void build(int p)
{
    if (p == 0)
        return;
    int a[2] = {0};
    int j = 0;
    for (int i = 1; i <= num; i++)
        if (relation[p][i])
        {
            a[j] = i;
            j++;
        }
    f[a[1]] = f[p] - 2 * size[a[1]] + size[1];
    f[a[0]] = f[p] - 2 * size[a[0]] + size[1];
    build(a[1]);
    build(a[0]);
}
//计算某棵树所有孩子的权值；
int f1(int p)
{
    if (p == 0)
        return 0;
    int a[2] = {0};
    int j = 0;
    for (int i = 1; i <= num; i++)
        if (relation[p][i])
        {
            a[j] = i;
            j++;
        }
    return f1(a[0]) + f1(a[1]) + size[a[1]] + size[a[0]];
}
int main()
{
    cin >> num;
    for (int i = 1; i <= num; i++)
    {
        cin >> knot[i];
        int left, right;
        cin >> left >> right;
        if (left)
            relation[i][left] = 1;
        if (right)
            relation[i][right] = 1;
    }
    for (int i = 1; i <= num; i++)
        size[i] = t_size(i);

    f[1] = f1(1);
    build(1);

    int min = f[1];
    for (int i = 2; i <= num; i++)
        if (f[i] < min)
            min = f[i];
    cout << min;
}