//以课程为结点，课程之间的关系为边得到一aov网络G
//aov网络所有可能的序列实际上构成一aov遍历树，样例附图1
//对于解决该题的思路，首先是找出该图的所有aov遍历路径，然后对各路径之间进行比较，找出最符合要求的路径
//对于找出所有aov遍历路径，采用利用图构造aov遍历树的方法，该遍历树从根到叶子节点的一条路径即为该图的一条aov路径
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <numeric>
using namespace std;
#define max_class 99    //最多的课程门数
#define max_semester 12 //最多的学期数
#define allow_burden 0
//allow_burden是反映学生各学期负担是否平均的一个值。该值可根据实际对负担的要求进行调整
//allow_burden=求和(各学期修的学分-平均每学期应修学分)^2/总学期数
//当allow_burden等于0时，学生各学期负担完全平均。随着其值的增大，学生各学期的负担逐渐不平均
#define concentrated 81
//concentrated反映学生课程在前几个学期中的集中程度
//为了反映在前几个学期，需要对每个学期求出的数据加权值。这里权值=总学期数-当前学期数
//concentrated=求和（各学期修的学分*权值^2）/总学期数
//concentrated越大，表明该学生的课程在前几个学期越集中
//这里concentrated取，实际中可以根据所想要的数据调整concentrad
int concentrate_number = 0; //记录平均负担排课有多少种可能的解
int burden_number = 0;      //记录前几个学期集中排课有多少个可能的解
vector<int> before;
struct item
{
    int num;
    int score;
};
struct node
{
    int num;
    int score;
    node *left_child;
    node *right_brother;
    node(int n, int s)
    {
        num = n;
        score = s;
        left_child = right_brother = nullptr;
    }
};

void search_aov(int, node *, vector<int>, int[][max_class + 1], int *); //对课程构成的aov网络进行遍历，找出所有可能的序列，并使用左孩子右兄弟的办法构建一aov遍历树
void input(int &, int &, int &, double &, int[][max_class + 1], int *); //输入函数
void walk(node *, node *, string, double, int, int, int, int);          //遍历生成的aov树，并利用用户选择的策略求出合适的解
void burden(item *, int, double, string, node *, int, int);             //策略1对于函数：使负担尽可能平均
void concentrate(item *, int, string, node *, int, int);                //策略2对应函数：使课程尽可能集中在前几个学期
void clean(node *);                                                     //清除在堆中申请的内存
bool is_similar(vector<int>, item *, int *);                            //判断本学期排课课程是否和之前的方案重复

int main()
{
    while (true)
    {
        int semester = 0, max_score = 0, num = 0;         //学期数，每学期最多学分，总共课程门数
        int relation[max_class + 1][max_class + 1] = {0}; //relation为邻接矩阵，表示课程之间的依赖关系
        int score[max_class + 1] = {0};                   //score记录每门课程的学分
        double average = 0;                               //average记录平均一个学期应该修的学分
        vector<int> parents;                              //初始下父母为空
        node *start = new node(-1, 0);                    //aov树起始根节点
        int order = 0;                                    //order记录用户策略选择
        concentrate_number = 0;                           //重置平均负担排课有多少种可能的解
        burden_number = 0;                                //重置前几个学期集中排课有多少个可能的解
        try
        {
            input(semester, max_score, num, average, relation, score);
            search_aov(num, start, parents, relation, score);
        }
        catch (const char *s)
        {
            cout << "s" << endl
                 << "the program will be restart" << endl;
            continue;
        }

        //以下对aov生成树进行处理
        cout << "共有两种编排策略：" << endl
             << "1.使学生在各学期中的学习负担尽量均匀;2.使课程尽可能地集中在前几个学期中";
        cout << "请选取编排策略，并输入对应策略的序号" << endl;
        cin >> order;
        while (order != 1 && order != 2) //检测恶意输入
        {
            cout << "输入错误,请重新输入";
            cin >> order;
        }
        walk(start, start, "", average, num, semester, max_score, order); //遍历生成的aov树，并利用用户选择的策略求出合适的解

        //检测用户所选取的参数是否求出来解
        if (concentrate_number == 0 && order == 2)
            cout << "选取的concentrated参考因数过大，请修改" << endl;
        if (burden_number == 0 && order == 1)
            cout << "选取的allow_burden参考因数过小，请重新选取" << endl;

        //删除所生成的aov搜索树，并判断用户是否还要继续排课
        clean(start); //清除在堆中申请的内存
        cout << "是否继续排课(N退出，按其他任意键继续)" << endl;
        char command;
        cin >> command;
        if (command == 'N')
            break;
    }
}

void search_aov(int num, node *cur, vector<int> parents, int relation[][max_class + 1], int *score) //num总节点个数，cur为当前结点，parents存储父母
{
    int temp = cur->num;
    if (temp != -1) //初始start结点不用输入到父母里面
        parents.push_back(temp);
    //下面寻找入度为0的结点并且将他们串起来
    node *head = nullptr, *p = nullptr; //p指向当前链表的最后一个节点
    for (int i = 1; i <= num; i++)
    {
        int j = 1;
        for (; j <= num; j++)
            if (relation[j][i] && find(parents.begin(), parents.end(), j) == parents.end() || find(parents.begin(), parents.end(), i) != parents.end())
                break;
        if (j == num + 1) //说明该点i入度为0
        {
            if (head == nullptr)
            {
                head = new node(i, score[i]);
                p = head;
            }
            else
            {
                p->right_brother = new node(i, score[i]);
                p = p->right_brother;
            }
        }
    }
    if (head == nullptr) //此时意味着没有新的可以访问的结点
    {
        if (parents.size() != num) //若此时已访问结点数少于总结点数，则问题无解
            throw "The problem has no solution!";
        else
            return; //此时已经走出一条完整的aov路径
    }
    //cur的左孩子指向更改
    cur->left_child = head;
    //上链后访问所有孩子结点
    do
    {
        search_aov(num, head, parents, relation, score);
        head = head->right_brother;
    } while (head != nullptr);
}
void input(int &semester, int &max_score, int &num, double &average, int relation[][max_class + 1], int *score)
{
    cout << "输入学期总数:";
    cin >> semester;
    if (semester > max_semester)
        throw "The semester is out of range!";
    cout << "输入一学期学分上限：";
    cin >> max_score;
    cout << "输入共计开设课程数目：";
    cin >> num;
    for (int i = 1; i <= num; i++)
    {
        int temp, k;
        cout << "课程号：";
        cin.get();
        cin.get();
        cin >> temp;
        while (temp > num) //识别输入的课程是否在开设的课程中
        {
            cout << "输入的课程未开设，请重新输入";
            cout << "课程号：";
            cin.get();
            cin.get();
            cin >> temp;
        }
        cout << "学分：";
        cin >> score[temp];
        cout << "直接先修课程课程编号，输入序列中间用空格隔开" << endl;
        cin.get();
        while (cin.peek() != '\n')
        {
            cin >> k;
            relation[k][temp] = 1;
        }
    }
    //计算每学期平均应修学分数，如果该值大于每学期最大所修学分数，则该问题一定无解
    double total = accumulate(score, score + num + 1, 0.0);
    average = total / semester;
    if (average > max_score)
        throw "一个学期的最大允许学分过少，无法完成学业";
}
void walk(node *start, node *sstart, string rec, double average, int num, int semester, int max_score, int order)
{
    node *child = start->left_child;
    char ch = '0';
    if (!child) //此时已经从aov树的根节点搜索到aov树的叶子节点，rec记录了搜索路径
    {
        //以下按照rec路径读出路径上经过节点。
        item arr[num] = {0};
        node *p = sstart; //sstart是记录根节点的参量，因为start在遍历过程中会发生改变
        for (int i = 0; !rec.empty(); i++)
        {
            p = p->left_child;
            rec.erase(0, 1);
            while (rec[0] == '1')
            {
                p = p->right_brother;
                rec.erase(0, 1);
            }
            arr[i].score = p->score;
            arr[i].num = p->num;
        }
        if (order == 1)
            burden(arr, num, average, rec, sstart, semester, max_score);
        else
            concentrate(arr, num, rec, sstart, semester, max_score);
    }
    while (child != nullptr)
    {
        rec.push_back(ch);
        walk(child, sstart, rec, average, num, semester, max_score, order);
        ch = '1';
        child = child->right_brother;
    }
}
void burden(item *arr, int num, double average, string rec, node *start, int semester, int max_score)
{
    //接下来进行一学期应有课程的划分
    //划分的原则是使学分上限不超过一学期最高学分，并且使负担尽可能均匀。
    //采用的算法是贪心算法，即先求出当前情况下每学期的平均学分，在计算过程中，使每学期的学分都接近平均学分且不超过最大学分。
    int count_semester = 0, j = 0;
    double calculate_burden = 0; //该数据记录在贪心算法下完成需要的学期数，如果学期数大于最大学期数，则无解
    int ss[semester + 1] = {0};  //该数据记录每学期所修的课程
    while (count_semester <= semester && j < num)
    {
        double sum = 0; //记录当前学分
        for (; j < num; j++)
        {
            if (sum < average &&
                ((sum + arr[j].score <= average) ||
                 (sum + arr[j].score > average && sum + arr[j].score < max_score && sum + arr[j].score - average < average - sum)))
                sum += arr[j].score;
            else
                break;
        }
        count_semester++;
        ss[count_semester] = j;
        calculate_burden += (sum - average) * (sum - average) / semester;
    }
    if (j != num || count_semester > semester)
        return;
    if (calculate_burden <= allow_burden && !is_similar(before, arr, ss))
    {
        before.clear();
        burden_number++;
        cout << "符合要求的修读顺序(" << burden_number << ")为：" << endl;
        int rol = 1; //计数器
        while (rol <= count_semester)
        {
            cout << "第" << rol << "学期修读课程：";
            for (int i = ss[rol - 1]; i < ss[rol]; i++)
            {
                cout << "课程:" << arr[i].num << ",";
                before.push_back(arr[i].num);
            }
            cout << endl;
            before.push_back(-1);
            rol++;
        }
    }
}
void concentrate(item *arr, int num, string rec, node *start, int semester, int max_score)
{
    //接下来进行一学期应有课程的划分
    //划分的原则是使学分上限不超过一学期最高学分，并且使负担尽可能集中在第一学期。
    int count_semester = 0, j = 0; //count_semester记录完成需要的学期数，如果学期数大于最大学期数，则无解
    double calculate_burden = 0;
    int ss[semester + 1] = {0}; //该数据记录每学期所修的课程
    while (count_semester <= semester && j < num)
    {
        double sum = 0; //记录本学期学分
        for (; j < num; j++)
        {
            if (sum + arr[j].score < max_score)
                sum += arr[j].score;
            else
                break;
        }
        count_semester++;
        ss[count_semester] = j;
        calculate_burden += sum * (semester - count_semester) * (semester - count_semester) / semester;
    }
    if (calculate_burden > concentrated && !is_similar(before, arr, ss))
    {
        before.clear();
        concentrate_number++;
        cout << "符合要求的修读顺序(" << concentrate_number << ")为：" << endl;
        int rol = 1; //计数器
        while (rol <= count_semester)
        {
            cout << "第" << rol << "学期修读课程：";
            for (int i = ss[rol - 1]; i < ss[rol]; i++)
            {
                cout << "课程:" << arr[i].num << ",";
                before.push_back(arr[i].num);
            }
            cout << endl;
            before.push_back(-1);
            rol++;
        }
    }
}
void clean(node *start)
{
    if (start->left_child == nullptr && start->right_brother == nullptr)
        delete start;
    else
    {
        if (start->left_child)
            clean(start->left_child);
        if (start->right_brother)
            clean(start->right_brother);
        delete start;
    }
}
bool is_similar(vector<int> before, item *arr, int *ss)
{
    if (before.empty())
        return false;
    int i = 0, k = 0, begin = 0, end = 0, temp = 0, pos_f = 0, pos_b = -1;
    for (; ss[i + 1]; i++) //该学期课程对应于arr中，从ss[i]开始，到ss[i+1]-1结束
    {
        begin = ss[i];
        end = ss[i + 1];
        pos_b = pos_f = pos_b + 1;
        while (before[pos_b] != -1)
            pos_b++;                  //在before中，-1为学期之间的分界线
        for (k = begin; k < end; k++) //对本学期内的每一门课程进行查找
        {
            temp = arr[k].num; //某一门课的学号
            //下面寻找在before记录的上一学期内，有没有对应的学号
            //j所记录的是上一个before[j]=-1的位置，即学期之间的分界线。
            //pos_f和pos_b标记before中记录学期的起始位置，并且在该区间内查找
            if (find(before.begin() + pos_f, before.begin() + pos_b, temp) == before.begin() + pos_b)
                return false;
        }
    }
    return true;
}
