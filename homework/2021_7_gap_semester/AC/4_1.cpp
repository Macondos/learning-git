#include <bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;

ll arr[25] = {0};
ll n, count_plan = 0, count_prime = 0;

#define max_total 40000001
int prime_list[max_total + 1] = {0}; //从1到可能的最大和.prime值为0；后续有一个就减一

void set_prime()
{
    int max = sqrt(max_total);
    prime_list[1] = prime_list[0] = 1;
    ll pre = 2;
    while (pre < max_total)
    {
        for (int i = pre, temp; pre <= max && (temp = pre * i) <= max_total; i++)
            prime_list[temp] = 1;
        for (pre = pre + 1; prime_list[pre]; pre++)
            ;
    }
}

void binary(ll now_sum, int now_decide)
{
    if (now_decide == n + 1)
    {
        if (prime_list[now_sum] <= 0)//0是质数，-1是已经出现过的质数
        {
            count_plan++;
            if (prime_list[now_sum] == 0)
            {
                count_prime++;
                prime_list[now_sum] = -1;
            }
        }
    }
    else
    {
        binary(now_sum, now_decide + 1);//不取这个
        binary(now_sum + arr[now_decide], now_decide + 1);//取这个
    }
}

int main()
{
    cin >> n;
    for (int i = 1; i <= n; i++)
        cin >> arr[i];
    set_prime();
    binary(0, 1);
    cout << count_plan << endl;
    cout << count_prime;
}