#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
struct weapon
{
    int pos;
    int num;
    friend bool operator<(const weapon &a, const weapon &b)
    {
        if (a.num == b.num)
            return a.pos > b.pos;
        else
            return a.num > b.num;
    }
};
bool cmp(weapon &a, weapon &b)
{
    return a.pos < b.pos;
}
int main()
{
    priority_queue<weapon, vector<weapon>> q;
    vector<weapon> qq;
    weapon temp;
    int n;
    cin >> n;
    for (int i = 1; i <= n; i++)
    {
        cin >> temp.num;
        temp.pos = i;
        q.push(temp);
    }
    weapon pre, cur;
    pre = q.top();
    q.pop();
    while (!q.empty())
    {
        cur = q.top();
        q.pop();
        if (cur.num == pre.num)
        {
            temp.num = 2 * cur.num;
            temp.pos = pre.pos > cur.pos ? pre.pos : cur.pos;
            q.push(temp);
            if (!q.empty())
            {
                pre = q.top();
                q.pop();
            }
        }
        else
        {
            qq.push_back(pre);
            pre = cur;
        }
    }
    qq.push_back(pre);
    cout << qq.size() << endl;
    sort(qq.begin(), qq.end(), cmp);
    for (auto &x : qq)
        cout << x.num << " ";
}