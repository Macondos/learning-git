#include <bits/stdc++.h>
using namespace std;
#define max_r 10000005
typedef long long ll;
bool prime[10000005] = {0};
int main()
{
    ll l, r, num;
    cin >> num;

    prime[0] = 1;
    ll pre = 2;
    ll sq = sqrt(max_r);
    while (pre <= max_r)
    {
        for (ll i = pre; pre <= sq && pre * i <= max_r; i++)
            prime[pre * i] = 1;
        for (pre = pre + 1; prime[pre]; pre++)
            ;
    }
    vector<ll> v;
    for (ll i = 0; i < num; i++)
    {
        cin >> l >> r;
        ll count = 0;
        for (ll j = l; j <= r; j++)
            if (prime[j] == 0)
                count++;
        v.push_back(count);
    }
    for (ll i = 0; i < num; i++)
        cout << v[i] << endl;
}
