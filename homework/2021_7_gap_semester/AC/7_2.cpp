#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int pre[1000005]; //记录每个元素的前项
struct edge
{
    int l;
    int r;
    int dis;
    friend bool operator<(const edge &a, const edge &b)
    {
        return a.dis > b.dis;
    }
    edge(int a, int b, int c)
    {
        l = a;
        r = b;
        dis = c;
    }
};

int get_pre(int pos)
{
    return pre[pos] == pos ? pos : pre[pos] = get_pre(pre[pos]);
}
void merge(int a, int b)
{
    int p1 = get_pre(a);
    int p2 = get_pre(b);
    pre[p1] = p2;
}
int main()
{
    int n, m, dis, l, r;
    priority_queue<edge, vector<edge>> q;
    pre[0] = 0;
    cin >> n >> m;
    for (int i = 1; i <= n; i++)
    {
        cin >> dis;
        q.push(edge(0, i, dis));
        pre[i] = i;
    }
    for (int i = 1; i <= m; i++)
    {
        cin >> l >> r >> dis;
        q.push(edge(l, r, dis));
    }
    ll sum = 0;
    ll count = 0;
    while (count != n)
    {
        while (get_pre(q.top().l) == get_pre(q.top().r))
            q.pop();
        edge hi = q.top();
        q.pop();
        sum += hi.dis;
        count++;
        merge(hi.l, hi.r);
    }
    cout << sum;
}