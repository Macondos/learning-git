#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
struct node
{
    vector<node> child;
    char ch;
};
int main()
{
    string arr;
    cin >> arr;
    node root;
    node *cur;
    ll count = 0;
    int pos; //pos记录比较到了第几位
    for (int i = 0; i < arr.size(); i++)
    {
        pos = i;
        cur = &root;
        while (pos < arr.size()) //和trie树比较，找到第一个不同的点
        {
            int k = 0;
            for (; k < cur->child.size(); k++)
                if (arr[pos] == cur->child[k].ch)
                    break;
            if (k == cur->child.size()) //没有找到，pos位也不相同
                break;
            cur = &cur->child[k];
            pos++;
        }
        if (pos != arr.size()) //说明pos位及以后都不相同
        {
            node temp;
            for (int u = pos; u < arr.size(); u++)
            {
                temp.ch = arr[u];
                cur->child.push_back(temp);
                cur = &cur->child[cur->child.size() - 1];
                count++;
            }
        }
    }
    cout << count;
}