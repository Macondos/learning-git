#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int main()
{
    ll n;
    cin >> n;
    ll h[n + 1];
    for (int i = 1; i <= n; i++)
        cin >> h[i];
    stack<ll> s;
    ll sum = 0;
    for (int i = n; i >= 1; i--)
    {
        while (!s.empty() && h[i] >= s.top())
            s.pop();
        sum += s.size();
        s.push(h[i]);
    }
    cout << sum;
}
