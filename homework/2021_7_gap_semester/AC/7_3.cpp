#include <bits/stdc++.h>
using namespace std;
#define mod 998244353
typedef long long ll;
vector<int> arr[100002];
vector<int> brr[100002];
bool visited[100002] = {0};
ll len[100002] = {0};
int n, m;
ll binary(int start)
{
    if (len[start])
        return len[start];
    if (arr[start].empty() && !brr[start].empty())
        return 1;
    if (arr[start].empty() && brr[start].empty())
        return 0;
    ll sum = 0;
    for (int i = 0; i < arr[start].size(); i++)
    {
        if (!visited[arr[start][i]])
        {
            visited[arr[start][i]] = 1;
            sum = (sum + (binary(arr[start][i])) % mod) % mod;
            visited[arr[start][i]] = 0;
        }
    }
    len[start] = sum;
    return sum;
}
int main()
{
    cin >> n >> m;
    for (int i = 0; i < m; i++)
    {
        int l, r;
        cin >> l >> r;
        arr[l].push_back(r);
        brr[r].push_back(l);
    }
    ll sum = 0;
    for (int i = 1; i <= n; i++)
    {
        if (brr[i].empty())
            sum = (sum + (binary(i)) % mod) % mod;
    }
    cout << sum;
}