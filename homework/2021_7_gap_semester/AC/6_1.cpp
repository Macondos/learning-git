#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
struct hole
{
    ll num;
    ll x;
    ll y;
    ll z;
    ll r;
    ll pre;
};
hole arr[5001];
ll prepre[5001] = {0};
ll get_pre(hole &a)
{
    return a.pre == a.num ? a.pre : a.pre = get_pre(arr[a.pre]);
}
bool is_connected(hole &a, hole &b)
{
    if (((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y) + (a.z - b.z) * (a.z - b.z)) < (a.r + b.r) * (a.r + b.r))
        return true;
    else
        return false;
}
void merge(hole &a, hole &b)
{
    ll p1 = get_pre(a);
    ll p2 = get_pre(b);
    arr[p1].pre = p2;
}
int main()
{
    ll n;
    cin >> n;
    for (ll i = 1; i <= n; i++)
    {
        arr[i].num = i;
        arr[i].pre = i;
        cin >> arr[i].x >> arr[i].y >> arr[i].z >> arr[i].r;
    }
    for (ll i = 1; i <= n; i++)
        for (ll j = i + 1; j <= n; j++)
            if (is_connected(arr[i], arr[j]))
                merge(arr[i], arr[j]);
    for (ll i = 1; i <= n; i++)
        prepre[i] = get_pre(arr[i]);
    sort(prepre + 1, prepre + n + 1);
    cout << (unique(prepre + 1, prepre + n + 1) - prepre - 2);
}