#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<pair<ll, ll>, ll> node;
vector<node> arr[10004][21]; //white-black+10
//[编号][white-black+10].first(连接点的[编号][white-black+10]).second(距离)
short color[10004]; //节点编号记
ll dis[10004][21];
ll n, m, w, b, u, v, d, s, t; //-b-w
struct cmp
{
    bool operator()(const node &a, const node &b)
    {
        return a.second > b.second;
    }
};
int main()
{

    cin >> n >> m >> w >> b;
    for (ll i = 1; i <= n; i++)
        cin >> color[i];
    for (ll i = 1; i <= m; i++)
    {
        cin >> u >> v >> d;
        if (color[v] == 1)
            for (ll i = -b; i <= w - 1; i++)
                arr[u][i + 10].push_back(make_pair(make_pair(v, i + 11), d));
        if (color[v] == 2)
            for (ll i = -b + 1; i <= w; i++)
                arr[u][i + 10].push_back(make_pair(make_pair(v, i + 9), d));
        if (color[u] == 1)
            for (ll i = -b; i <= w - 1; i++)
                arr[v][i + 10].push_back(make_pair(make_pair(u, i + 11), d));
        if (color[u] == 2)
            for (ll i = -b + 1; i <= w; i++)
                arr[v][i + 10].push_back(make_pair(make_pair(u, i + 9), d));
    }
    cin >> s >> t;
    /////////////////////////////////////////////////////
    priority_queue<node, vector<node>, cmp> q;
    ll cur_x = s, cur_y, x, y;
    if (color[s] == 1)
        cur_y = 11;
    else
        cur_y = 9;
    node temp;
    //由于优先队列无法删除更改，故在访问堆顶的时候，和dis中做一比较
    //初始化工作
    memset(dis, -1, sizeof(dis));
    for (ll i = -b; i <= w; i++)
        dis[s][i + 10] = 0;
    q.push(make_pair(make_pair(cur_x, cur_y), 0));
    /////////////////////////////////////////////////////
    while (cur_x != t)
    {
        while (!q.empty() && q.top().second > dis[q.top().first.first][q.top().first.second])
            q.pop();
        if (q.empty())
            break;
        temp = q.top();
        q.pop();
        cur_x = temp.first.first;
        cur_y = temp.first.second;
        //循环更改当前结点所有邻接点,并推入队列
        for (ll j = 0; j < arr[cur_x][cur_y].size(); j++)
        {
            x = arr[cur_x][cur_y][j].first.first;
            y = arr[cur_x][cur_y][j].first.second;
            if (dis[x][y] == -1 || dis[x][y] > dis[cur_x][cur_y] + arr[cur_x][cur_y][j].second)
            {
                dis[x][y] = dis[cur_x][cur_y] + arr[cur_x][cur_y][j].second;
                q.push(make_pair(make_pair(x, y), dis[x][y]));
            }
        }
    }
    if (cur_x == t)
        cout << dis[cur_x][cur_y];
    else
        cout << -1;
}
