#include <bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;

int cnt = 0;          //记录满足条件的解数
bool lie[14] = {0};   //记录列有没有放皇后
bool k1[27] = {0};    //主对角线
bool k_1[27] = {0};   //整体+13，行减列
int answer[14] = {0}; //a[i]表示第i行的皇后被放在一列
int n;
void binary(int decide)
{
    if (decide == n + 1)
    {
        if (cnt < 3)
        {
            for (int i = 1; i < n; i++)
                cout << answer[i] << " ";
            cout << answer[n];
            cout << endl;
        }
        cnt++;
        return;
    }
    for (int i = 1; i <= n; i++) //decide行，i列
    {
        if (!lie[i] && !k1[decide + i] && !k_1[decide - i + 13])
        {
            lie[i] = 1;
            k1[decide + i] = 1;
            k_1[decide - i + 13] = 1;
            answer[decide] = i;
            binary(decide + 1);
            lie[i] = 0;
            k1[decide + i] = 0;
            k_1[decide - i + 13] = 0;
        }
    }
    return;
}

int main()
{
    cin >> n;
    binary(1);
    cout << cnt;
}