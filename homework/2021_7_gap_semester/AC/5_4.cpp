#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int upper_bound(int *a, int *min, int temp, int begin, int end)
{
    if (begin == end)
        return begin;
    int mid = (begin + end + 1) / 2;
    if (temp >= a[min[mid]])
        return upper_bound(a, min, temp, mid, end);
    else
        return upper_bound(a, min, temp, begin, mid - 1);
}
int main()
{
    int n;
    cin >> n;
    int a[n + 1] = {0};
    int cur = 0;               //cur记录目前最长的子序列有多长
    int min[n + 1] = {0};      //min[i]记录长度为i的子序列的最小的最高位下标
    int transfer[n + 1] = {0}; //tansfer[i]记录1-i最长的子序列是从哪一位转移过来的
    int position;
    for (int i = 1; i <= n; i++)
        cin >> a[i];
    for (int i = 1; i <= n; i++)
    {
        if (a[i] >= a[min[cur]])
        {
            transfer[i] = min[cur];
            min[++cur] = i;
        }
        else
        {
           position = upper_bound(a, min, a[i], 0, cur);
            transfer[i] =min[position];
            min[++position] = i;
        }
    }
    cout << cur << endl;
    int num = min[cur];
    stack<int> s;
    while (num)
    {
        s.push(num);
        num = transfer[num];
    }
    while (!s.empty())
    {
        cout << s.top() << " ";
        s.pop();
    }
}