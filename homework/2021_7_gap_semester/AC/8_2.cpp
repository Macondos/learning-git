#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
ll gcd(ll a, ll b)
{
    return b ? gcd(b, a % b) : a;
}
int main()
{
    ll x, y;
    cin >> x >> y;
    ll div = y / x;
    if (div * x != y)
    {
        cout << 0;
        return 0;
    }
    ll count = 0;
    vector<pair<ll, ll>> v;
    for (ll i = 1; i <= sqrt(div); i++)
        if (div % i == 0)
        {
            if (gcd(i, div / i) == 1)
            {
                v.push_back(make_pair(i * x, div / i * x));
                count++;
                if (i * i != div)
                {
                    count++;
                    v.push_back(make_pair(div / i * x, i * x));
                }
            }
        }
    cout << count << endl;
    for (ll i = 0; i < v.size(); i++)
        cout << v[i].first << " " << v[i].second << endl;
}