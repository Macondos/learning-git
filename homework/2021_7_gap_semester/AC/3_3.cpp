#include <bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;
struct matrix
{
    ll a11;
    ll a12;
    ll a21;
    ll a22;
    void plus(matrix &m2, ll mod)
    {
        ll temp11, temp12, temp21, temp22;
        temp11 = ((a11 % mod) * (m2.a11 % mod) + (a12 % mod) * (m2.a21 % mod)) % mod;
        temp12 = ((a11 % mod) * (m2.a12 % mod) + (a12 % mod) * (m2.a22 % mod)) % mod;
        temp21 = ((a21 % mod) * (m2.a11 % mod) + (a22 % mod) * (m2.a21 % mod)) % mod;
        temp22 = ((a21 % mod) * (m2.a12 % mod) + (a22 % mod) * (m2.a22 % mod)) % mod;
        a11 = temp11;
        a12 = temp12;
        a21 = temp21;
        a22 = temp22;
    }
    matrix()
    {
        a11 = 1;
        a12 = 1;
        a21 = 1;
        a22 = 0;
    }
};
matrix &total_plus(matrix &m, ll jieshu, ll mod)
{
    if (jieshu == 1)
        return m;
    else
    {
        if (jieshu & 1)
        {
            matrix temp;
            total_plus(temp, jieshu / 2, mod);
            temp.plus(temp, mod);
            temp.plus(m, mod);
            m = temp;
        }
        else
        {
            matrix temp;
            total_plus(temp, jieshu / 2, mod);
            temp.plus(temp, mod);
            m = temp;
        }
        return m;
    }
}
int main()
{
    matrix m;
    ll month, k, num;
    cin >> month >> k;
    if (month >= 3)
    {
        m = total_plus(m, month - 2, k);
        num = (m.a11 + m.a12) % k * 2 % k;
    }
    else
        num = 2;
    ll left = num % k;
    cout << left;
}