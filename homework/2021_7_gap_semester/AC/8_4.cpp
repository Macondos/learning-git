#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
ll gcd(ll a, ll b)
{
    return b ? gcd(b, a % b) : a;
}
ll euler(ll temp)
{
    ll re = temp;
    ll pre = 0;
    for (ll i = 2; i * i <= temp; i++)
        if (temp % i == 0)
        {
            temp /= i;
            if (i != pre)
            {
                re = re / i * (i - 1);
                pre = i;
            }
            --i;
        }
    if (temp != 1)
        if (temp != pre)
            re = re / temp * (temp - 1);
    return re;
}
int main()
{
    ll t;
    cin >> t;
    ll a, m;
    for (int i = 1; i <= t; i++)
    {
        cin >> a >> m;
        m /= gcd(a, m);
        cout << euler(m) << endl;
    }
}