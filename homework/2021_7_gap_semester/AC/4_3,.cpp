#include <bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;
int movx[] = {-1, -1, 1, 1, 2, 2, -2, -2};
int movy[] = {-2, 2, 2, -2, 1, -1, -1, 1};
ll sum_now = 0;
ll sum_max = 0;
ll max_[2001][2001] = {0}; //表示每一个点的最优解
ll a[2002][2002];
ll n, m;

ll binary(ll x, ll y)
{
    if (max_[x][y])
        return max_[x][y];
    ll x_, y_, temp;
    max_[x][y] = a[x][y];
    bool flag = false;
    for (ll i = 0; i < 8; i++)
    {
        x_ = x + movx[i];
        y_ = y + movy[i];
        if (x_ >= 1 && x_ <= n && y_ >= 1 && y_ <= m)
        {
            if (a[x][y] > a[x_][y_])
            {
                temp = a[x][y] + binary(x_, y_);
                if (temp > max_[x][y])
                    max_[x][y] = temp;
                flag = true;
            }
        }
    }
    if (max_[x][y] > sum_max)
        sum_max = max_[x][y];
    return max_[x][y];
}
int main()
{
    std::ios::sync_with_stdio(false);
    cin >> n >> m;
    for (ll i = 1; i <= n; i++)
        for (ll j = 1; j <= m; j++)
            cin >> a[i][j];
    for (ll i = 1; i <= n; i++)
        for (ll j = 1; j <= m; j++)
            binary(i, j);
    cout << sum_max;
}
