#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int main()
{
    int n, div;
    char temp;
    cin >> n;
    string arr[n + 1];
    for (int i = 1; i <= n; i++)
    {
        cin >> arr[i];
        div = arr[i][0] - 'a';
        if (div != 0)
        {
            for (int j = 0; j < arr[i].size(); j++)
            {
                arr[i][j] -= div;
                if (arr[i][j] < 'a')
                    arr[i][j] += 26;
            }
        }
    }
    string min = arr[1];
    for (int i = 2; i <= n; i++)
        if (arr[i] < min)
            min = arr[i];
    cout << min;
}