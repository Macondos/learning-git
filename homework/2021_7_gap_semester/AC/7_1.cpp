#include <bits/stdc++.h>
using namespace std;
int main()
{
    int n, s;
    cin >> n >> s;
    set<int> arr[n + 1];
    bool visited[n + 1] = {0};
    int l, r;
    for (int i = 1; i < n; i++)
    {
        cin >> l >> r;
        arr[l].insert(r);
        arr[r].insert(l);
    }
    while (!arr[s].empty())
    {
        int temp = *arr[s].begin();
        arr[temp].erase(s);
        s = temp;
    }
    cout << s;
}