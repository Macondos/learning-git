#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int main()
{
    ll sum = 0, max_sum = 0;
    int n;
    cin >> n;
    int a[n];
    for (int &x : a)
        cin >> x;
    int start = 0, i;
    while (start < n)
    {
        sum = 0;
        for (i = start; i < n; i++)
        {
            sum += a[i];
            if (sum > max_sum)
                max_sum = sum;
            if (sum <= 0)
                break;
        }
        start = i + 1;
    }
    cout << max_sum;
}