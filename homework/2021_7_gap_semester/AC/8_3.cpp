#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int main()
{
    ll len;
    cin >> len;
    ll temp = sqrt(len);
    ll a[temp + 2] = {0};
    //预处理len/i;i<=temp
    for (ll div = 1; div <= temp + 1; div++)
        a[div] = len / div;

    //判断个数
    ll num;
    if (a[temp] == temp)
    {
        num = temp - 1;
        printf("%lld\n", 2 * temp - 1);
    }
    else
    {
        num = temp;
        printf("%lld\n", 2 * temp);
    }

    //先处理可能间隔数数都是一个的，遍历除数
    for (ll div = 1; div <= temp; div++)
    {
        printf("%lld ", a[div] + 1);
        printf("%lld\n", 1);
    }
    //处理可能间隔数可能为许多个的，但是间隔对应的棵数不会超过temp，遍历棵数
    for (; num >= 1; num--)
    {
        printf("%lld ", num + 1);
        printf("%lld\n", a[num] - a[num + 1]);
    }
}