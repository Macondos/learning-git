#include <bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;
#define inf 1 << 30
//f[y]表示考虑第i件游戏，花费恰好为y时的最小折扣
//f[i][y]=min{f[i-1][y],f[i-1][y-v[i]]+ai-bi};
//不存在时为inf
int f[10004] = {0};
int a[10004] = {0};
int b[10004] = {0};
int main()
{
    int n, l, r;
    cin >> n >> l >> r;
    for (int i = 1; i <= n; i++)
        cin >> a[i] >> b[i];
    //i=0初始化f
    for (int i = 1; i <= r; i++)
        f[i] = inf;
    for (int i = 1; i <= n; i++)
        for (int j = r; j >= 0; j--)
            if ((j - b[i] >= 0))
                f[j] = min(f[j], f[j - b[i]] + a[i] - b[i]);
    int cost = inf;
    for (int k = l; k <= r; k++)
    {
        if (f[k] < cost)
            cost = f[k];
    }
    if (cost == inf)
        cout << -1;
    else
        cout << cost;
}
