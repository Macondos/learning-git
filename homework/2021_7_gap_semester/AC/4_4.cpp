#include <bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;
bool mp[2005][2005] = {0};
bool visited[2005][2005] = {0};  //visited到过一次，再到某个到过的点时（不一定非要是起点），只要不在同一个地图中就好啦
int x_visited[2005][2005] = {0}; //记录visited是在哪个地图第一次到的
int y_visited[2005][2005] = {0};

int x0, y00, m, n;
char temp;
int direction_x[] = {1, -1, 0, 0};
int direction_y[] = {0, 0, 1, -1};

bool binary(int x, int y, int dx, int dy) //在dx,dy地图中的xy点
{
    if (visited[x][y] && (dx != x_visited[x][y] || dy != y_visited[x][y]))
        return true;
    else if (visited[x][y])
        return false;

    visited[x][y] = 1;
    x_visited[x][y] = dx;
    y_visited[x][y] = dy;
    int tx,tdx=dx,ty,tdy=dy;
    for (int i = 0; i < 4; i++)
    {
        tdx=dx;tdy=dy;
        tx = x + direction_x[i];
        ty = y + direction_y[i];
        //处理边界情况
        if (tx==0){tx+=n;tdx=dx-1;}
        if(tx>n){tx-=n;tdx=dx+1;}
        if(ty==0){ty+=m;tdy=dy-1;}
        if (ty>m){ty-=m;tdy=dy+1;}
        //边界情况处理完毕
        if (mp[tx][ty])
            if(binary(tx,ty,tdx,tdy))
                return true;
    }
    return false;
}
int main()
{

    cin >> n >> m;
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++)
        {
            cin >> temp;
            if (temp != '#')
                mp[i][j] = 1;
            if (temp == 'S')
            {
                x0 = i;
                y00 = j;
            }
        }
    if (binary(x0, y00,0,0))
        cout << "Yes";
    else
        cout << "No";
}