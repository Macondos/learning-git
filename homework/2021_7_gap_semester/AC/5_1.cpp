#include <bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;
struct matrix
{
    ll a[3][3];
    void plus(matrix &m2, ll mod)
    {
        ll temp[3][3] = {0};
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                for (int k = 0; k < 3; k++)
                    temp[i][j] += (a[i][k] % mod * m2.a[k][j] % mod) % mod;
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                a[i][j] = temp[i][j];
    }
    matrix()
    {
        a[0][0] = a[1][1] = a[1][2] = a[2][0] = a[2][2] = 0;
        a[0][1] = a[0][2] = a[1][0] = a[2][1] = 1;
    }
};
matrix &total_plus(matrix &m, ll jieshu, ll mod)
{
    if (jieshu == 1)
        return m;
    else
    {
        if (jieshu & 1)
        {
            matrix temp;
            total_plus(temp, jieshu / 2, mod);
            temp.plus(temp, mod);
            temp.plus(m, mod);
            m = temp;
        }
        else
        {
            matrix temp;
            total_plus(temp, jieshu / 2, mod);
            temp.plus(temp, mod);
            m = temp;
        }
        return m;
    }
}
int main()
{
    int n;
    cin >> n;
    matrix m;
    ll sum = 0;
    if (n > 3)
    {
        m = total_plus(m, n - 3, 998244353);
        sum = (m.a[0][0] + m.a[0][1]) % 998244353;
    }
    else if (n = 1)
        sum = 0;
    else
        sum = 1;
    cout << sum;
}