#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int main()
{
    ll n, m;
    cin >> n >> m;
    ll a[n];
    ll b[m];
    for (long long &x : a)
        cin >> x;
    for (long long &x : b)
        cin >> x;
    sort(a, a + n, greater<ll>());
    sort(b, b + m, greater<ll>());
    if ((a[0] >= 0 && b[0] >= 0) || (a[n - 1] <= 0 && b[m - 1] <= 0))
    {
        if (a[0] * b[0] >= a[n - 1] * b[m - 1]) //a[0]被盖住了
        {
            cout << a[0] << " ";
            if ((a[1] >= 0 && b[0] >= 0) || (a[n - 1] <= 0 && b[m - 1] <= 0))
                cout << max(a[1] * b[0], a[n - 1] * b[m - 1]);
            else
            {
                if (a[1] < 0)
                    cout << a[2] * b[m - 1];
                else
                    cout << a[n - 1] * b[0];
            }
        }
        else
        {
            cout << a[n - 1] << " ";
            if ((a[0] >= 0 && b[0] >= 0) || (a[n - 2] <= 0 && b[m - 1] <= 0))
                cout << max(a[0] * b[0], a[n - 2] * b[m - 1]);
            else
            {
                if (a[0] < 0)
                    cout << a[0] * b[m - 1];
                else
                    cout << a[n - 2] * b[0];
            }
        }
    }
    else //两边都是异号
    {
        if (a[0] < 0)
            cout << a[0] << " " << a[1] * b[m - 1];
        else
            cout << a[n - 1] << " " << a[n - 2] * b[0];
    }
}