#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
bool board[68][68] = {0};
int matrix[200][131] = {0};
int n, m;
int main()
{
    cin >> n >> m;
    char temp;
    for (int i = 1; i <= 2 * n; i++)
        for (int j = 1; j <= 2 * m; j++)
        {
            while (cin.peek() != '1' && cin.peek() != '0')
                cin.get();
            temp = cin.get();
            if (temp == '1')
                board[i][j] = 1;
        }
    int t;
    //共有(n+1)*(m+1)个子区域，每个子区域可以生成一个m*n维的向量
    //以区域的左上角坐标遍历
    for (int i = 1; i <= n + 1; i++)
        for (int j = 1; j <= m + 1; j++)
            for (int x = i; x < n + i; x++)
                for (int y = j; y < m + j; y++)
                {
                    t = (x - i) * m + y - j + 1;
                    if (board[x][y])
                        matrix[(i - 1) * (m + 1) + j][(x - i) * m + y - j + 1] = 1;
                }
    for (int i = 1; i <= (n + 1) * (m + 1); i++)
    {
        for (int j = 1; j <= m * n - 1; j++)
            cout << matrix[i][j] << ",";
        cout << matrix[i][m * n];
        cout << endl;
    }
}