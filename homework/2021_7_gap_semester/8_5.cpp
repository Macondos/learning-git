#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
bool board[68][68] = {0};
int matrix[200][131] = {0};
int n, m;
int main()
{
    cin >> n >> m;
    char temp;
    for (int i = 1; i <= 2 * n; i++)
        for (int j = 1; j <= 2 * m; j++)
        {
            while (cin.peek() != '1' && cin.peek() != '0')
                cin.get();
            temp = cin.get();
            if (temp == '1')
                board[i][j] = 1;
        }
    //共有(n+1)*(m+1)个子区域，每个子区域可以生成一个m*n维的向量
    //以区域的左上角坐标遍历
    for (int i = 1; i <= n + 1; i++)
        for (int j = 1; j <= m + 1; j++)
            for (int x = i; x < n + i; x++)
                for (int y = j; y < m + j; y++)
                    if (board[x][y])
                        matrix[(i - 1) * (m + 1) + j][(x - i) * m + y - j + 1] = 1;

    int pianyi = 0;
    int i = 1;
    for (; (i + pianyi) <= n * m && i <= (n + 1) * (m + 1); i++) //列约简，游标是行标
    {
        if (!matrix[i][i + pianyi]) //首先找到一个首元不是0的列，并且和最左面的一列交换
        {
            int j = i + pianyi + 1;
            for (; j <= m * n && !matrix[i][j]; j++)
                ;
            if (j == n * m + 1) //说明该行剩下的列全为0，那么矩阵的秩就要减一
            {
                --pianyi;
                continue;
            }
            else //那么就交换
                for (int k = 1; k <= (n + 1) * (m + 1); k++)
                    swap(matrix[k][j], matrix[k][i + pianyi]);
        }
        for (int k = i + pianyi + 1; k <= m * n; k++)
        {
            if (matrix[i][k])
            {
                long double plus = -matrix[i][k] / double(matrix[i][i + pianyi]);
                for (int w = i; w <= (n + 1) * (m + 1); w++)
                    matrix[w][k] += plus * matrix[w][i + pianyi];
            }
        }
    }
    if ((i + pianyi) > n * m) //从右侧出来的,从而应该是满秩
        cout << n * m;
    else //非满秩的只能从下面出来
        cout << (n + 1) * (m + 1) + pianyi;
}
