#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

ll cost = 0;                                         //当前的花费
ll best_cost = LLONG_MAX;                            //最优的调度花费时间
priority_queue<int, vector<int>, greater<int>> heap; //排列机器的加工时间
ll remain = 0;                                       //剩余未排序机器的加工时间和
ll n = 0;                                            //加工元素的个数
ll m = 0;                                            //加工机器数
ll estimate = 0;

void search(int layer, int *x, int *best_x, int *t)
{
    if (layer > n && cost < best_cost)
    {
        best_cost = cost;
        for (int i = 1; i <= n; i++)
            best_x[i] = x[i];
    }
    else
    {
        for (int i = layer; i <= n; i++)
        {
            swap(x[layer], x[i]);
            //更新新的一层的时间
            int temp = heap.top();
            int temp_cost = cost;
            if ((temp + t[x[layer]]) > cost)
                cost = temp + t[x[layer]];
            //预估完成时间的下界
            estimate = temp + remain / (n - layer + 1);
            if (estimate < best_cost) //进入下一层
            {
                //传参时备份最小堆，以便回退栈时恢复最小堆
                priority_queue<int, vector<int>, greater<int>> heap_copy = heap;
                //更新remain。cost已经被更新了
                remain -= t[x[layer]];
                //并更新m台机器所构成的最小堆
                heap.pop();
                heap.push(temp + t[x[layer]]);
                //////////////////////////////////
                search(layer + 1, x, best_x, t);
                //复原所做的更新
                remain += t[x[layer]];
                heap = heap_copy;
            }
            swap(x[layer], x[i]);
            cost = temp_cost;
        }
    }
}

int main()
{
    cout << "输入待调度作业的个数n和加工机器数m，中间用空格隔开：";
    cin >> n >> m;
    cout << "输入加工n个作业分别所需要的时间，中间用空格隔开:";
    int t[n + 1] = {0};      //记录加工作业所需的时间
    int x[n + 1] = {0};      //当前的解向量
    int best_x[n + 1] = {0}; //最优解向量

    for (int i = 1; i <= n; i++)
    {
        cin >> t[i]; //输入时间并初始化x
        x[i] = i;
        remain += t[i];
    }
    for (int i = 1; i <= m; i++)
        heap.push(0); //初始化最小堆
    search(1, x, best_x, t);
    cout << "作业完成最早的时间为：" << best_cost << endl;
    //下面输出作业的加工方式
    priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> q; //first为加工时间，second为机器编号
    for (int i = 1; i <= m; i++)
        q.push(make_pair(0, i));
    pair<int, int> temp;
    vector<int> v[m + 1];
    for (int i = 1; i <= n; i++)
    {
        temp = q.top();
        q.pop();
        v[temp.second].push_back(best_x[i]);
        q.push(make_pair(temp.first + t[best_x[i]], temp.second));
    }
    cout << "作业的加工方式为：" << endl;
    for (int i = 1; i <= m; i++)
    {
        cout << "第" << i << "个机器上的加工作业依次为：";
        for (auto x : v[i])
            cout << "作业" << x << "(时间 " << t[x] << "),";
        cout << endl;
    }
}
