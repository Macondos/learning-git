#include <bits/stdc++.h>
using namespace std;
int i, j, num;
void search(int *a, int *b, int i_b, int i_e, int j_b, int j_e)
{
    if (i_b == i_e)
    {
        i = i_b;
        j = num - 1 - i;
        return;
    }
    if (j_b == j_e)
    {
        j = j_b;
        i = num - 1 - j;
        return;
    }
    int mid_i = (i_b + i_e) / 2;
    int mid_j = (j_b + j_e) / 2;
    if (a[mid_i] == b[mid_j]) //为了保证相等的时候0~i-1+0~j-1一共有n-1个数，故作出了一部分细节调整
    {
        if (a[i_e] > b[i_e])
        {
            i_b = i_e = mid_i;
            j_b = j_e;
        }
        else
        {
            i_b = i_e;
            j_b = j_e = mid_j;
        }
    }
    else if (a[mid_i] > b[mid_j])
    {
        i_e = mid_i;
        j_b = mid_j;
    }
    else
    {
        i_b = mid_i;
        j_e = mid_j;
    }
    return search(a, b, i_b, i_e, j_b, j_e); //每次折半查找，故时间复杂度为O(logn)
}

int main()
{
    cin >> num;
    int a[num], b[num];
    for (int i = 0; i < num; i++)
        cin >> a[i];
    for (int i = 0; i < num; i++)
        cin >> b[i];
    search(a, b, 0, num - 1, 0, num - 1);
    //输出i，j后，0-i-1和0-j-1一共有n-1个数，下面判断哪个数是第n和第n+1个，并求平均，便可以得到中位数。
    //在j=0和i=0的边界条件下求中位数略有不同，单独列出
    if (j == 0)
    {
        if (b[0] <= a[num - 2])
            cout << "中位数为：" << (a[num - 2] + min(b[1], a[num - 1])) / 2.0;
        else
            cout << "中位数为：" << (b[0] + b[1] + a[num - 1] - max(b[0], max(b[1], a[num - 1]))) / 2.0;
    }
    else if (i == 0)
    {
        if (a[0] < b[num - 2])
            cout << "中位数为：" << (b[num - 2] + min(b[num - 1], a[1])) / 2.0;
        else
            cout << "中位数为：" << (a[0] + a[1] + b[num - 1] - max(a[0], max(a[1], b[num - 1]))) / 2.0;
    }
    else
    {
        int temp[4] = {a[i], a[i + 1], b[j], b[j + 1]};
        sort(temp, temp + 4); //找到第n大和第n+1大的数字
        cout << "中位数为：" << (temp[0] + temp[1]) / 2.0;
    }
}
