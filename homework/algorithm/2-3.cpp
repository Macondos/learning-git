#include <bits/stdc++.h>
using namespace std;
int low_bound(int *a, int begin, int end, int search)
{
    if (begin == end)
        return begin;
    int mid = (begin + end + 1) / 2;
    if (a[mid] <= search)
        return low_bound(a, mid, end, search);
    else
        return low_bound(a, begin, mid - 1, search);
}

int up_bound(int *a, int begin, int end, int search)
{
    if (begin == end)
        return begin;
    int mid = (begin + end) / 2;
    if (a[mid] < search)
        return up_bound(a, mid + 1, end, search);
    else if (a[mid] > search)
        return up_bound(a, begin, mid, search);
    else//////这里为了使得当数组中有多个相同的元素等于search时，和low_bound一样返回最右边的元素
    {
        if (a[mid + 1] == search)
            return up_bound(a, mid + 1, end, search);
        else
            return mid;
    }
}

int main()
{
    int num, search;
    cin >> num;
    int a[100] = {0};
    for (int i = 0; i < num; i++)
        cin >> a[i];
    cin >> search;

    if (search < a[0])
        cout << "没有小于其的元素";
    else
        cout << "小于其的最大元素下标：" << low_bound(a, 0, num - 1, search) << endl;

    if (search > a[num - 1])
        cout << "没有大于其的元素";
    else
        cout << "大于其的最小元素下标：" << up_bound(a, 0, num - 1, search);
}