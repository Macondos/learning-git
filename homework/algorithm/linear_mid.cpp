#include <bits/stdc++.h>
using namespace std;

int partition(int *a, int begin, int end, int pivot)
{
    int temp = a[pivot];
    swap(a[begin], a[pivot]); //把枢轴放在开始的位置
    int i = begin, j = end;
    while (i < j)
    {
        while (a[j] > temp && i < j)
            j--;
        a[i] = a[j];
        while (a[i] <= temp && i < j)
            i++;
        a[j] = a[i];
    }
    a[i] = temp;
    return i;
}

int find_k(int *a, int begin, int end, int k)
{
    static default_random_engine e;
    static uniform_int_distribution<unsigned> u(begin, end);
    int temp = u(e);                          //temp为随机选出来的下标，介于begin，end之间
    int pos = partition(a, begin, end, temp); //pos为划分过后a[temp]的下标的位置
    if (pos - begin + 1 == k)
        return a[pos];
    else if (pos - begin + 1 < k)
        return find_k(a, pos + 1, end, k - (pos + 1 - begin));
    else
        return find_k(a, begin, pos - 1, k);
}

int main()
{
    int n = 0;
    cout << "输入字符串长度:";
    cin >> n;

    int a[n + 1] = {0};
    cout << "输入字符串序列，中间以空格分隔：";
    for (int i = 1; i <= n; i++)
        cin >> a[i];

    if (n % 2 == 0) //是偶数
    {
        int c1 = find_k(a, 1, n, n / 2);
        int c2 = find_k(a, n / 2 + 1, n, 1);
        cout << "中位数为：" << (c1 + c2) / 2.0;
    }
    else
    {
        int c = find_k(a, 1, n, (n + 1) / 2);
        cout << "中位数为: " << c;
    }
}
