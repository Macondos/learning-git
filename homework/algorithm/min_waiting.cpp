#include <bits/stdc++.h>
using namespace std;

int main()
{
    int n = 0, m = 0;
    cout << "请输入待加工作业数目n: ";
    cin >> n;
    cout << "请输入加工设备数m: ";
    cin >> m;
    if (m >= n)
    { cout << "n个作业的等待时间和为" << 0;return 0;}
    pair<int, int> time[n + 1]; //first为作业所需的加工时间，second为该作业编号
    cout << "请输入待加工作业所需要的加工时间，中间以空格分开：";
    for (int i = 1; i <= n; i++)
    {
        cin >> time[i].first;
        time[i].second = i;
    }

    sort(time + 1, time + n + 1);
    long long sum_time = 0;

    priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> q; //first为加工时间，second为机器编号
    for (int i = 1; i <= m; i++)
        q.push(make_pair(0, i));
    pair<int, int> temp;
    vector<int> v[m + 1];
    for (int i = 1; i <= n; i++)
    {
        temp = q.top();
        sum_time += temp.first;
        q.pop();
        v[temp.second].push_back(i);
        q.push(make_pair(temp.first + time[i].first, temp.second));
    }
    cout << "作业的加工方式为：" << endl;
    for (int i = 1; i <= m; i++)
    {
        cout << "第" << i << "个机器上的加工作业依次为：";
        for (auto x : v[i])
            cout << "作业" << time[x].second << "(时间 " << time[x].first << "),";
        cout << endl;
    }
    cout << "n个作业的等待时间和为" << sum_time;
}