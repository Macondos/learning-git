#include <bits/stdc++.h>
using namespace std;

int partition(int *a, int begin, int end, int pivot)
{
    int temp = a[pivot];
    swap(a[begin], a[pivot]); //把枢轴放在开始的位置
    int i = begin, j = end;
    while (i < j)
    {
        while (a[j] > temp && i < j)
            j--;
        a[i] = a[j];
        while (a[i] <= temp && i < j)
            i++;
        a[j] = a[i];
    }
    a[i] = temp;
    return i;
}

int find_k(int *a, int begin, int end, int k)
{
    //判断要不要递归
    int num = end - begin + 1;
    int shuzhou = 0;
    if (num <= 5) //小于等于5个元素则不用分组处理，直接排序
    {
        sort(a + begin, a + end + 1);
        return a[begin + k - 1];
    }
    else //多于5个元素要分组处理
    {
        for (int i = 0;; i += 5)
            if ((begin + i + 5) <= end + 1)
                sort(a + begin + i, a + begin + i + 5);
            else
            {
                sort(a + begin + i, a + begin + num);
                break;
            }
        int count = 0;               //计数一共要递归排序多少个数
        int temp[num / 5 + 5] = {0}; //temp数组存储num/5个中位数
        for (int i = 2;; i += 5)
        {
            if (i + begin <= end)
            {
                temp[count] = a[i + begin];
                count++;
            }
            else
                break;
        }
        shuzhou = find_k(temp, 0, count - 1, (count + 1) / 2); //现在这个找到的是temp里面元素
        for (int i = begin; i <= end; i++)                     //转化为a里面的坐标
            if (a[i] == shuzhou)
            {
                shuzhou = i; //转化为在a里面的坐标
                break;
            }
        int pos = partition(a, begin, end, shuzhou); //pos为划分过后的下标的位置,shuzhou也为数组下标
        if (pos - begin + 1 == k)
            return a[pos];
        else if (pos - begin + 1 < k)
            return find_k(a, pos + 1, end, k - (pos + 1 - begin));
        else
            return find_k(a, begin, pos - 1, k);
    }
}

int main()
{
    int n = 0;
    cout << "输入字符串长度:";
    cin >> n;

    int a[n + 1] = {0};
    cout << "输入字符串序列，中间以空格分隔：";
    for (int i = 1; i <= n; i++)
        cin >> a[i];

    if (n % 2 == 0) //是偶数
    {
        int c1 = find_k(a, 1, n, n / 2);
        int c2 = find_k(a, n / 2 + 1, n, 1);
        cout << "中位数为：" << (c1 + c2) / 2.0;
    }
    else
    {
        int c = find_k(a, 1, n, (n + 1) / 2);
        cout << "中位数为: " << c;
    }
}
