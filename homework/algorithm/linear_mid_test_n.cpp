#include <bits/stdc++.h>
#include <iomanip>
#include <Windows.h>
using namespace std;

long long partition(long long *a, long long begin, long long end, long long pivot)
{
    long long temp = a[pivot];
    swap(a[begin], a[pivot]); //把枢轴放在开始的位置
    long long i = begin, j = end;
    while (i < j)
    {
        while (a[j] > temp && i < j)
            j--;
        a[i] = a[j];
        while (a[i] <= temp && i < j)
            i++;
        a[j] = a[i];
    }
    a[i] = temp;
    return i;
}

long long find_k(long long *a, long long begin, long long end, long long k)
{
    //判断要不要递归
    long long num = end - begin + 1;
    long long shuzhou = 0;
    if (num <= 5) //小于等于5个元素则不用分组处理，直接排序
    {
        sort(a + begin, a + end + 1);
        return a[begin + k - 1];
    }
    else //多于5个元素要分组处理
    {
        for (long long i = 0;; i += 5)
            if ((begin + i + 5) <= end + 1)
                sort(a + begin + i, a + begin + i + 5);
            else
            {
                sort(a + begin + i, a + begin + num);
                break;
            }
        long long count = 0;               //计数一共要递归排序多少个数
        long long temp[num / 5 + 5] = {0}; //temp数组存储num/5个中位数
        for (long long i = 2;; i += 5)
        {
            if (i + begin <= end)
            {
                temp[count] = a[i + begin];
                count++;
            }
            else
                break;
        }
        shuzhou = find_k(temp, 0, count - 1, (count + 1) / 2); //现在这个找到的是temp里面元素
        for (long long i = begin; i <= end; i++)                     //转化为a里面的坐标
            if (a[i] == shuzhou)
            {
                shuzhou = i; //转化为在a里面的坐标
                break;
            }
        long long pos = partition(a, begin, end, shuzhou); //pos为划分过后的下标的位置,shuzhou也为数组下标
        if (pos - begin + 1 == k)
            return a[pos];
        else if (pos - begin + 1 < k)
            return find_k(a, pos + 1, end, k - (pos + 1 - begin));
        else
            return find_k(a, begin, pos - 1, k);
    }
}
double linear_mid(long long *a, long long n)
{
    if (n % 2 == 0) //是偶数
    {
        long long c1 = find_k(a, 1, n, n / 2);
        long long c2 = find_k(a, n / 2 + 1, n, 1);
        return (c1 + c2) / 2.0;
    }
    else
    {
        long long c = find_k(a, 1, n, (n + 1) / 2);
        return c;
    }
}
int main()
{
    long long n[6] = {1, 10, 100, 1000, 10000, 100000};
    for (long long test = 0; test < 6; test++)
    {
        long long a[n[test]+1] = {0};
        long long round = 1; //总共测试1000轮，并对所用时间取平均
        double sum_time = 0; //记录总运行时间；

        while (round <= 1000)
        {
            static default_random_engine e;
            for (long long i = 1; i <= n[test]; i++)
                a[i] = e();

            LARGE_INTEGER StartingTime, EndingTime, ElapsedMicroseconds;
            LARGE_INTEGER Frequency;
            QueryPerformanceFrequency(&Frequency);
            QueryPerformanceCounter(&StartingTime);

            linear_mid(a, n[test]); // 计时的活动

            QueryPerformanceCounter(&EndingTime);
            ElapsedMicroseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;
            double timeSpend = (ElapsedMicroseconds.QuadPart * 1000.0) / Frequency.QuadPart;
            //得到的结果是毫秒

            sum_time += timeSpend;
            round++;
        }

        cout << "对于n为O(" << n[test] << ")数量级所花费的平均时间(ms)为：";
        double ans = sum_time / (round - 1);
        cout << fixed << setprecision(8) << ans << endl;
    }
    return 0;
}