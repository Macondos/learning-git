#include <bits/stdc++.h>
using namespace std;
int main()
{
    int a_len, b_len;
    cout << "请输入a的长度：";
    cin >> a_len;
    cout << "请输入b的长度：";
    cin >> b_len;
    cin.get();

    char a[a_len + 1] = {0};
    char b[b_len + 1] = {0};
    cout << "请输入字符串a：";
    for (int i = 1; i <= a_len; i++)
        cin >> a[i];
    cin.get();
    cout << "请输入字符串b：";
    for (int i = 1; i <= b_len; i++)
        cin >> b[i];

    int d[2][b_len + 1] = {0}; //d为记录字符串编辑距离 d[0][]为第一行，d[1]为接下来的一行，下一个循环交换第一行
    //初始化d
    for (int i = 0; i <= b_len + 1; i++)
        d[0][i] = i;
    for (int i = 1; i <= a_len; i++) //对于矩阵表的计算要一直计算到a_len行
    {
        if (i % 2 == 1) //奇数行，则更新d[1][]
        {
            d[1][0] = i; //初始化这一行的第一个值
            for (int j = 1; j <= b_len; j++)
            {
                if (a[i] == b[j])
                    d[1][j] = d[0][j - 1];
                else
                    d[1][j] = min(d[1][j - 1], min(d[0][j], d[0][j - 1])) + 1;
            }
        }
        else //偶数行，则更新d[0][]
        {
            d[0][0] = i; //初始化这一行的第一个值
            for (int j = 1; j <= b_len; j++)
            {
                if (a[i] == b[j])
                    d[0][j] = d[1][j - 1];
                else
                    d[0][j] = min(d[0][j - 1], min(d[1][j], d[1][j - 1])) + 1;
            }
        }
    }
    cout << "a,b的最小编辑距离为：" << d[a_len % 2][b_len];
}