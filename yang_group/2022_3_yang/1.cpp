#include <bits/stdc++.h>
using namespace std;
int main()
{
    long double Cx = 191076.5149724329;
    long double Cy = 244372.4850275671;
    long double Bx = 189649.5149724329;
    long double By = 242945.4850275671;
    long double Ax = 190643; //圆心坐标
    long double Ay = 241952; //圆心坐标
    long double circle_begin_x = 189649.5149724329;
    long double circle_begin_y = 242945.4850275671;
    long double circle_end_x = 189238;
    long double circle_end_y = 241952;
    long double r_2, b, a, c;
    swap(Cx, Bx);
    swap(Cy, By);
    r_2 = pow(circle_end_x - Ax, 2) + pow(circle_end_y - Ay, 2);
    // r_2 = sqrt(r_2);
    // b = 2 * (Cx - Bx) * (Bx - Ax) + 2 * (Cy - By) * (By - Ay);
    // c = pow((Bx - Ax), 2) + pow((By - Ay), 2) - pow(r_2, 2);
    // a = pow(Cx - Bx, 2) + pow(Cy - By, 2);
    // long double bb = pow(b, 2);
    // long double delta = pow(b, 2) - 4 * a * c;
    long double delta = r_2 * (pow(Cx - Bx, 2) + pow(Cy - By, 2)) - pow((Cx - Bx) * (By - Ay),2) - pow((Cy - By) * (Bx - Ax), 2)+2*((Cx - Bx) * (By - Ay)*(Cy - By) * (Bx - Ax));
    long double error = (2 * r_2 * (Cx + Cy - Bx - By) + pow(Cx - Bx, 2) + pow(Cy - By, 2) - 2 * ((Cx - Bx) * (By - Ay) - (Cy - By) * (Bx - Ax)) * (Cx - Cy + Ax - Ay - 2 * Bx + 2 * By))*1e-10;
    cout << delta;
}