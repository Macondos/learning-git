#include <bits/stdc++.h>
using namespace std;
int main()
{
    double Cx = 299697.9184719829;
    double Cy = 230398.91847198288;
    double Bx = 299756.9184719829;
    double By = 230339.91847198288;
    double Ax = 300959;
    double Ay = 231542;
    double circle_begin_x = 299756.9184719829;
    double circle_begin_y = 230399.91847198288;
    double circle_end_x = 302161.0815280171;
    double circle_end_y = 232744.08152801712;
    double r_2, b, a, c;
    swap(Cx, Bx);
    swap(Cy, By);
    r_2 = pow(circle_end_x - Ax, 2) + pow(circle_end_y - Ay, 2);
    r_2 = sqrt(r_2);
    // r_2 = 1405;
    b = 2 * (Cx - Bx) * (Bx - Ax) + 2 * (Cy - By) * (By - Ay);
    c = pow((Bx - Ax), 2) + pow((By - Ay), 2) - pow(r_2, 2);
    a = pow(Cx - Bx, 2) + pow(Cy - By, 2);
    double bb = pow(b, 2);
    double temp = pow(b, 2) - 4 * a * c;    
    long double error=(2*pow(r_2,2)*(Cx+Cy-Bx-By)+pow(Cx-Bx,2)+pow(Cy-By,2)-2*((Cx-Bx)*(By-Ay)-(Cy-By)*(Bx-Ax))*(Cx-Cy+Ax-Ay-2*Bx+2*By))*1e-10;
    cout << temp;
}