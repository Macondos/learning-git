#include <bits/stdc++.h>
#define BOUND 10
using namespace std;

float *solve(float a1, float b1, float c1, float a2, float b2, float c2)
{
    float *res = new float[2];
    float x, y;
    x = (b2 * c1 - b1 * c2) / (a2 * b1 - a1 * b2);
    y = (a1 * c2 - a2 * c1) / (a2 * b1 - a1 * b2);
    res[0] = x;
    res[1] = y;
    return res;
}

int main()
{
    // float* solve(float a, float b, float c, float  d, float  e, float  f);
    // two equations
    // a * x + b * y + c  = 0;
    // d * x + e * y + f  = 0;

    float a, b, c, d, e, f;
    // another equation line by interpolation
    float m, n, p;
    float alpha, dis1, dis2, dis3;
    bool flag;
    float arr2[205 * BOUND][2] = {0};
    float arr3[205 * BOUND][2] = {0};

    // start of line 1
    float x0 = 412055.128475;
    float y0 = 690585.999985;
    // end of line 1
    float x1 = 407774.128475;
    float y1 = 690586.999985;
    //(y1 - y0) * x - (x1 - x0) * y - (y1 - y0) * x0 + (x1 - x0) * y0 = 0
    a = y1 - y0;
    b = -(x1 - x0);
    c = -(y1 - y0) * x0 + (x1 - x0) * y0;

    // start of line 2
    float x2 = 512285;
    float y2 = 690586;
    // end of line 2
    float x3 = 412055.128475;
    float y3 = 690585.999985;
    d = y3 - y2;
    e = -(x3 - x2);
    f = -(y3 - y2) * x2 + (x3 - x2) * y2;
    float *result1 = solve(a, b, c, d, e, f);
    dis1 = sqrt((result1[0] - x3) * (result1[0] - x3) + (result1[1] - y3) * (result1[1] - y3));

    // a = 1; b = -1; c = 2;
    // d = 1.00000006; e = -1; f = 1;
    double mindis = 1e10;
    double minalpha;
    int count = 0;
    for (alpha = -BOUND; alpha < BOUND; alpha += 0.01)
    {
        // alpha = (a * a + b * b) / (a * a - a * d + b * b - b * e);
        // alpha = 0.1;
        // alpha = 8.8;
        // alpha = -80;
        m = (1 - alpha) * a + alpha * d;
        n = (1 - alpha) * b + alpha * e;
        p = (1 - alpha) * c + alpha * f;
        arr2[count][0] = arr3[count][0] = alpha;
        // float x_ = 600000;
        // float y_ = (-p - m * x_) / n;
        // am + bn = 0;
        //(1 - alpha) * a*a + alpha * d *a + (1 - alpha) * b * b + alpha * e * b = 0
        // a*a + b * b + alpha(-a*a + d *a - b * b + e * b ) = 0

        float *result2 = solve(a, b, c, m, n, p);
        dis2 = sqrt((result2[0] - x3) * (result2[0] - x3) + (result2[1] - y3) * (result2[1] - y3));
        arr2[count][1] = dis2;

        float *result3 = solve(m, n, p, d, e, f);
        dis3 = sqrt((result3[0] - x3) * (result3[0] - x3) + (result3[1] - y3) * (result3[1] - y3));
        arr3[count][1] = dis3;

        count++;
        if (dis2 < mindis)
        {
            mindis = dis2;
            minalpha = alpha;
            flag = 0;
        }
        if (dis3 < mindis)
        {
            mindis = dis3;
            minalpha = alpha;
            flag = 1;
        }
    }
    ofstream oFile;
    oFile.open("arr2.txt");
    for (int i = 0; i < count; i++)
        oFile << arr2[i][0] << " " << arr2[i][1] << endl;
    oFile.close();

    oFile.open("arr3.txt");
    for (int i = 0; i < count; i++)
        oFile << arr3[i][0] << " " << arr3[i][1] << endl;
    
}
