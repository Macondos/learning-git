import numpy as np  # 加载数学库用于函数描述
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import style

# with open("test.txt", "r") as f:
#     data = f.readlines()


def draw(filename):
    data = np.loadtxt(filename+'.txt')
    data_x = data[:, 0]
    data_y = data[:, 1]
    plt.scatter(data_x, data_y, s=1)
    plt.ylim(-10, 100)
    plt.show()

draw('arr2')
draw('arr3')
