#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int main()
{
    ll n, k;
    cin >> n >> k;
    ll a[n + 1] = {0};
    for (int i = 1; i <= n; i++)
        cin >> a[i];
    ll control = 0; //记录前一个数
    ll regis = 0;
    ll length = 1;
    ll length_max;
    if (k < n)
        length_max = 1 + k;
    else if(k==n)
        length_max = n;
    int chance = 0;

    for (int i = 1; i <= n-1;)
    { //从1开始扫描,比较下一个数和这个数，lenth包括到i
        if (a[i + 1] < a[i] && chance == 0)
        {
            if (i + k >= n) //超出范围
            {
                length += n - i;
                length_max = max(length_max, length);
                break;
            }
            chance = 1;
            control = a[i];
            regis = i; //下一个数开始换
            length += k;
            //接下来k个数都变得比contr大
            i = i + k + 1; //认为i是已经被处理过的,下面的那个数还未处理
            if (a[i] >= control)
                length++;
            else
            {
                length_max = max(length_max, length);
                i = regis + 1;
                chance = 0;
                length = 1;
            }
        }
        else if (a[i + 1] < a[i] && chance == 1)
        {
            length_max = max(length_max, length);
            i = regis + 1;
            chance = 0;
            length = 1;
        }
        else if(a[i+1]>=a[i])
        {
            length++;
            i++;
        }
    }
    length_max = max(length, length_max);
    cout << length_max;
    return 0;
}