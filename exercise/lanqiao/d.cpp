#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int main()
{
    ll n, m, x;
    cin >> n >> m >> x;
    int arr[n + 1] = {0};
    int copy[n + 1] = {0};
    for (int i = 1; i <= n; i++)
    {
        cin >> arr[i];
        copy[i] = x ^ arr[i];
    }
    ll l, r;
    for (int i = 1; i <= m; i++)
    {
        cin >> l >> r;
        bool flag = false;
        for (int i = l; i <= r; i++)
        {
            for (int j = l; j <= r; j++)
                if (copy[j] == arr[i])
                {
                    flag = true;
                    break;
                }
            if (flag)
                break;
        }
        if (flag)
            cout << "yes" << endl;
        else
            cout << "no" << endl;
    }
    return 0;
}