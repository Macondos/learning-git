#include <bits/stdc++.h>
using namespace std;
typedef long double ld;
struct item
{
    ld x;
    ld y;
    ld z;
    ld r;
    ld angle;
    int name;
    int rank;
};
bool compare1(item a, item b)
{
    if (a.angle < b.angle)
        return true;
    else if (a.angle = b.angle)
    {
        if (a.r <= b.r)
            return true;
        else
            return false;
    }
    else
        return false;
}
bool compare2(item a, item b)
{
    if (a.name < b.name)
        return true;
    else
        return false;
}
int main()
{
    int n, l;
    cin >> n >> l;
    item arr[n + 1] = {0};
    for (int i = 1; i <= n; i++)
    {
        cin >> arr[i].x >> arr[i].y >> arr[i].z;
        arr[i].r = sqrt(arr[i].x * arr[i].x + arr[i].y * arr[i].y);
        arr[i].angle = atan2l(arr[i].y, arr[i].x);
        //调整角度
        if (arr[i].angle < 0)
        {
            arr[i].angle = -arr[i].angle;
            arr[i].angle += atan2l(1, 0);
        }
        else if (arr[i].angle <= atan2l(1, 0))
            arr[i].angle = atan2l(1, 0) - arr[i].angle;
        else
            arr[i].angle = 5 * atan2l(1, 0) - arr[i].angle;
        //////////////////////
        arr[i].name = i;
        arr[i].rank = -1;
    }
    sort(arr + 1, arr + n + 1, compare1);

    vector<item> vec;
    vector<item>::iterator it, temp;
    vec.assign(arr, arr + n + 1);
    it = vec.begin() + 1;
    ld cur_angle;
    int cur_rank = 1;
    int i = 1;
    bool flag = false;
    while (!vec.empty())
    {
        while (l < (*it).r && i <= n)
        {
            i++;
            it++;
        }
        if (it >= vec.end())
        {
            i = 1;
            it = vec.begin() + 1;
            if (flag)
            {
                continue;
                flag = false;
            }
            else
                break;
        }
        cur_angle = (*it).angle;
        int count = 0;
        do
        {
            flag = true;
            arr[i].rank = cur_rank;
            l += (*it).z;
            vec.erase(it);
            i++;
            count++;
        } while ((*it).r <= l && (*it).angle == cur_angle);
        cur_rank += count;
    }

    sort(arr + 1, arr + n + 1, compare2);
    for (int i = 1; i <= n - 1; i++)
        cout << arr[i].rank << " ";
    cout << arr[n].rank;

    return 0;
}