#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int main()
{
    ll n;
    cin >> n;
    ll a[n + 1] = {0};
    ll b[n + 1] = {0}; // b用来保存后缀和
    for (int i = 1; i <= n; i++)
        cin >> a[i];
    b[n] = a[n];
    for (int i = n - 1; i >= 1; i--)
        b[i] = a[i] + b[i + 1];
    ll sum = 0;
    for (int i = 1; i <= n - 1; i++)
        sum += a[i] * b[i + 1];
    cout << sum;
    return 0;
}