#include<bits/stdc++.h>
using namespace std;
int w,s;
string input;
vector<int> vec;
vector<int> ans;
int mod = 929;
int mazi(int a,int b)
{
	return 30*a+b;
}
int main()
{
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);
	cin>>w>>s;
	cin>>input;
	int flag = 0;
	for(int i=0;i<input.length();i++)
	{
		if(input[i]<='Z'&&input[i]>='A')
		{
			if(flag == 1)
			{
				vec.push_back(28);
				vec.push_back(28);
			}
			else if(flag == 2)
			{
				vec.push_back(28);
			}
			vec.push_back(input[i]-'A');
			flag = 0;
		}
		else if(input[i] >='a'&&input[i]<='z')
		{
			if(flag != 1)
				vec.push_back(27);
			vec.push_back(input[i]-'a');
			flag = 1;
		}
		else if(input[i]>='0'&&input[i]<='9')
		{
			if(flag != 2)
				vec.push_back(28);
			vec.push_back(input[i]-'0');
			flag = 2;
		}
	}
	if(vec.size() % 2!=0)
	{
		vec.push_back(29);
	}
	for(int i=0;i<vec.size();i+=2)
	{
		ans.push_back(mazi(vec[i],vec[i+1]));
	}
	int k = pow(2, s+1);
	if(s!=-1)
	{
		while((ans.size()+k+1)%w!=0)
		{
			ans.push_back(900);
		}
	}
	else
	{
		while((ans.size()+1)%w!=0)
		{
			ans.push_back(900);
		}
	}
	cout<<ans.size()+1<<"\n";
	
	if(s > -1)
	{
		vector<int> dx(ans.size()+1+k);
		for(int i=0;i<dx.size();i++)
		{
			dx[i] = 0;
		}
		vector<int> gx(k+1);
		for(int i=0;i<k+1;i++)
			gx[i] = 0;
		gx[k] = -3;
		gx[k-1] = 1;
		int temp = -9;
		for(int i=0;i<k-1;i++)
		{
			vector<int> tt(k+1);
			for(int j = k-i-1;j<=k;j++)
				tt[j] = gx[j];
			for(int j=k-i-1;j<=k;j++)
			{
				gx[j] = ((gx[j] % mod) * (temp % mod))%mod;
			}
			for(int j=k-i-1;j<k;j++)
			{
				gx[j] = (gx[j] %mod + tt[j+1]%mod)%mod;
			}
			gx[k-i-2] = 1;
			temp *= 3;
			temp %= mod;
		}

		dx[0] = ans.size()+1;
		for(int i=1;i<ans.size()+1;i++)
		{
			dx[i] = ans[i-1];
			dx[i] %= mod;
		}
		for(int i=0;i<dx.size()-k;i++)
		{
			int t = dx[i];
			for(int j=0;j<gx.size();j++)
			{
				dx[i+j] = (dx[i+j] -  (gx[j]*t)%mod) % mod;
			}
		}
		for(int i=dx.size() - gx.size()+1;i<dx.size();i++)
		{
			if(-dx[i] < 0)
				ans.push_back(mod + (-dx[i] % mod));
			else
				ans.push_back(-dx[i]%mod);
		}
	}
	for(int i=0;i<ans.size();i++)
	{
		cout<<ans[i]<<"\n";
	}
	return 0;
 } 
