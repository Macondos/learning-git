#include <bits/stdc++.h>
using namespace std;
int main()
{
    int n, N;
    cin >> n >> N;
    int a[n + 2] = {0};
    for (int i = 1; i <= n; i++)
        cin >> a[i];
    a[n + 1] = N;

    int cur = 0, last = 0;
    int last_pos = 0;
    int sum = 0;
    for (int i = 0; i <= n + 1; i++) //对数组从0-n进行扫描
    {
        cur = a[i]; // 2
        sum += (cur - last) * last_pos;
        last_pos = i;
        last = a[i];
    }
    cout << sum;
}