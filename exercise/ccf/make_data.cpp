#include <bits/stdc++.h>
using namespace std;

string strRand(int length)
{                  // length: 产生字符串的长度
    char tmp;      // tmp: 暂存一个随机数
    string buffer; // buffer: 保存返回值

    // 下面这两行比较重要:
    random_device rd;                   // 产生一个 std::random_device 对象 rd
    default_random_engine random(rd()); // 用 rd 初始化一个随机数发生器 random

    for (int i = 0; i < length; i++)
    {
        tmp = random() % 62; // 随机一个小于 36 的整数，0-9、A-Z 共 36 种字符
        if (tmp < 10)
        { // 如果随机数小于 10，变换成一个阿拉伯数字的 ASCII
            tmp += '0';
        }
        else if (tmp >= 10 && tmp < 36)
        { // 否则，变换成一个大写字母的 ASCII
            tmp -= 10;
            tmp += 'A';
        }
        else
        {
            tmp -= 36;
            tmp += 'a';
        }
        buffer += tmp;
    }
    return buffer;
}
int main()
{
    srand(time(0));
    cout << rand() % 10 << " " << rand() % 9 << endl;
    cout << strRand(rand() % 100);
    return 0;
}
