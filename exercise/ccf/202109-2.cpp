/* CCF202109-2 非零段划分 */

#include <bits/stdc++.h>

using namespace std;

int main()
{
    int n;
    cin >> n;
    int a[n + 2] = {0};
    for (int i = 1; i <= n; i++)
        cin >> a[i];
    n = unique(a , a + n + 2) - a - 1;
    int merge[10003] = {0};
    for (int i = 1; i <= n; i++)
    {
        if (a[i] > a[i - 1] && a[i] > a[i + 1])
            merge[a[i]]++;
        else if (a[i] < a[i - 1] && a[i] < a[i + 1])
            merge[a[i]]--;
    }
    int m = 0;
    int sum = 0;
    for (int i = 10002; i >= 0; i--)
    {
        sum += merge[i];
        m = max(sum, m);
    }
    cout << m;
    return 0;
}
