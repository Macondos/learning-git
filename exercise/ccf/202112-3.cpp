#include <bits/stdc++.h>
using namespace std;
int main()
{
    long long word, s; // word表示每行码字数目，s表示校验级别
    cin >> word >> s;
    long long jiaoyan;
    if (s != -1)
        jiaoyan = 1 << (s + 1); //校验码字的数目
    else
        jiaoyan = 0;
    string code; //输入的待编码的串
    cin >> code;
    //首先把string里面的数据编码
    vector<long long> valid_data; //记录编码后的有效数据码字
    //编码前有效码字数目还不确定
    long long status = 1; //记录当前编码器所处的模式，大写为1，小写为2，数字为3
    for (long long i = 0; i < code.length(); i++)
    {
        long long temp = code[i];
        if (temp >= 48 && temp <= 57) //是数字,要求模式3
        {
            if (status != 3)
            {
                valid_data.push_back(28);
                status = 3;
            }
            valid_data.push_back(temp - 48);
        }
        else if (temp >= 65 && temp <= 90) //是大写字母，要求模式1
        {
            if (status == 2)
            {
                valid_data.push_back(28);
                valid_data.push_back(28);
                status = 1;
            }
            else if (status == 3)
            {
                valid_data.push_back(28);
                status = 1;
            }
            valid_data.push_back(temp - 65);
        }
        else //是小写数字，要求模式2
        {
            if (status != 2)
            {
                valid_data.push_back(27);
                status = 2;
            }
            valid_data.push_back(temp - 97);
        }
    }

    if (valid_data.size() % 2 == 1)
        valid_data.push_back(29);
    ////////////////////////////////////////////////////////
    //计算实际有效码字
    vector<long long> valid_code;
    for (long long i = 0; i < valid_data.size(); i += 2)
        valid_code.push_back(valid_data[i] * 30 + valid_data[i + 1]);
    //计算填充码字的数目
    long long blank;
    if ((valid_code.size() + jiaoyan + 1) % word)
        blank = word - (valid_code.size() + jiaoyan + 1) % word;
    else
        blank = 0;
    //全部数据码字的个数
    long long n = blank + valid_code.size() + 1;
    long long k = jiaoyan;
    /////////////////////////////////////////////////////////
    //创建一个存储多项式的数组
    long long xd[n + k] = {0}; //该多项式有n+k位，最高位是n+k-1
    long long gx[k + 1] = {0}; // gx的最高位是k

    if (jiaoyan)
    {
        //表示dx
        xd[n + k - 1] = n;
        for (long long i = n + k - 2; i > blank - 1 + k; i--)
            xd[i] = valid_code[n + k - 2 - i];
        for (long long i = blank - 1 + k; i >= k; i--)
            xd[i] = 900;

        //把gx表示出来
        gx[0] = -3;
        gx[1] = 1;               //先表示gx=x-3;
        vector<int> needed(513); //记录3^n%929的数值
        needed[0] = 1;
        for (int i = 1; i <= 512; i++)
            needed[i] = (needed[i - 1] * 3) % 929;
        for (long long i = 2; i <= k; i++)
        {
            int temp = needed[i];
            for (long long j = k; j >= 1; j--)
                gx[j] = (gx[j - 1] - temp * gx[j]) % 929;
            gx[0] *= -temp;
            gx[0] %= 929;
        }

        //用多项式xd除以多项式gx
        for (long long i = n + k - 1; i >= k; i--)
        {
            long long mul = xd[i];
            for (long long j = k; j >= 0; j--)
            {
                xd[i - (k - j)] -= mul * gx[j];
                xd[i - (k - j)] %= 929;
            }
        }
    }
    //输出
    cout << n << endl;
    for (long long x : valid_code)
        cout << x << endl;
    for (long long i = 1; i <= blank; i++)
        cout << 900 << endl;
    if (jiaoyan)
    {
        for (long long i = k - 1; i >= 1; i--)
            if (((-xd[i]) % 929) < 0)
                cout << (929 + (-xd[i]) % 929) << endl;
            else
                cout << (-xd[i]) % 929 << endl;
        //最后一个换行处理
        if (((-xd[0]) % 929) < 0)
            cout << (929 + (-xd[0]) % 929);
        else
            cout << (-xd[0]) % 929;
    }
}