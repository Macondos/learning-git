#include <bits/stdc++.h>
using namespace std;
int main()
{
    long n;
    cin >> n;
    long a[n + 2] = {0}; // i=0和i=n+1都设置为0的边界条件
    vector<long> index[10050];
    for (long i = 1; i <= n; i++)
    {
        cin >> a[i];
        index[a[i]].push_back(i);
    }
    // index记录了对于某一值在数组中的下标位置
    long max_longerval = 0;
    long longerval = 0;
    //初始化longerval
    for (long i = 1; i <= n + 1; i++)
    {
        if (!a[i] && a[i - 1])
            longerval++;
    }
    max_longerval = longerval;
    long max_e = *max_element(a + 1, a + n + 1);
    for (long p = 1; p <= max_e; p++) //做操作小于等于p的数据都变成0
    {
        for (long i = 0; i < index[p].size(); i++)
        {
            long pos = index[p][i];
            a[pos] = 0;
            if (a[pos - 1] && a[pos + 1])
                longerval++;
            else if (!a[pos - 1] && !a[pos + 1])
                longerval--;
            if (longerval > max_longerval)
                max_longerval = longerval;
        }
    }
    cout << max_longerval;
    return 0;
}