#include <bits/stdc++.h>
using namespace std;
int main()
{
    long long n, N;
    cin >> n >> N;
    long long a[n + 2] = {0}; //下表为0-n+1
    for (long long i = 1; i <= n; i++)
        cin >> a[i];
    a[n + 1] = N;
    long long r = N / (n + 1);
    long long A, B;
    long long cur = 0, last = 0; //指的是f(i)中的i的值
    long long last_pos = 0;
    long long sum = 0;

    for (long long i = 1; i <= n + 1; i++) //对数组从0-n进行扫描
    {                                      //每次计算的值是f(last)-f(a[i]-1)
        cur = a[i];
        A = last / r;
        B = (cur - 1) / r;

        if (B > A)
        {
            sum += abs(A - last_pos) * (r - last % r);
            for (long long i = 1; i <= B - A - 1; i++)
                sum += abs(A + i - last_pos) * r;
            sum += abs(B - last_pos) * ((cur - 1) % r + 1);
        }
        else 
            sum += abs(A - last_pos) * (cur - last);
    

        last_pos = i;
        last = a[i];
    }
    cout << sum;
}